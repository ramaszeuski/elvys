package ECData;
use Moose;
use namespace::autoclean;

use Catalyst::Runtime 5.80;

# Set flags and add plugins for the application
#
#         -Debug: activates the debug mode for very useful log messages
#   ConfigLoader: will load the configuration from a Config::General file in the
#                 application's home directory
# Static::Simple: will serve static files from the application's root
#                 directory

use Catalyst qw/
    ConfigLoader
    Static::Simple
    -Debug

    Authentication
    Authorization::Roles

    Session
    Session::Store::FastMmap
    Session::State::Cookie

    Unicode::Encoding
/;


extends 'Catalyst';

our $VERSION = '1.05';
$VERSION = eval $VERSION;

# Configure the application.
#
# Note that settings in ecdata.conf (or other external
# configuration file that you set up manually) take precedence
# over this when using ConfigLoader. Thus configuration
# details given here can function as a default configuration,
# with an external configuration file acting as an override for
# local deployment.

__PACKAGE__->config(
   name => 'ECData',

   roles_for_results      => 'lab customer dealer root',
   roles_for_results_list => 'lab customer dealer root',

   authentication => {
      default_realm => 'users',
      realms        => {
         users => {
            credential => {
               class          => 'Password',
               password_field => 'password',
               password_type  => 'clear'
            },
            store => {
               class         => 'DBIx::Class',
               user_model    => 'DB::Login_view',
               role_relation => 'login_roles',
               role_field    => 'role',
            }
         },
      },
   },

    'Controller::HTML::FormFu' => {
        constructor => {
            render_method => 'tt',
            tt_args       => {
                ENCODING => 'utf-8',
            },
        },
        model_stash => {
            schema => 'DB',
        },
    },

   'Plugin::Static::Simple' => {
        include_path => [
            'root/static',
        ],
    },

    'Plugin::Session' => {
        expires           => 3600,
        verify_user_agent => 1,
        verify_address    => 1,
#       cookie_secure  => 1,
    },

#    'View::PDF' => {
#       INCLUDE_PATH => __PACKAGE__->path_to('root2','pdf')
#     }

);

# Start the application
__PACKAGE__->setup();


=head1 NAME

ECData - Catalyst based application

=head1 SYNOPSIS

    script/ecdata_server.pl

=head1 DESCRIPTION

[enter your description here]

=head1 SEE ALSO

L<ECData::Controller::Root>, L<Catalyst>

=head1 AUTHOR

Andrej Ramašeuski,,,,

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
