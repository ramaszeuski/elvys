package ECData::FormFu::Validator::ICZFree;

use strict;
use utf8;

use base 'HTML::FormFu::Validator';

sub validate_value {
    my $self = shift;
    my ( $value, $params) = @_;
    my $c = $self->form->stash->{context};

    return 1 if $c->model('DB::Subject')->search( { id => $value } )->count == 0;

    die HTML::FormFu::Exception::Validator->new({
        message => 'Zadané ICZ je již použito',
    });
}

1;

