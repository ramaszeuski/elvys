package ECData::FormFu::Validator::SpecializationCode;

use strict;
use utf8;

use base 'HTML::FormFu::Validator';

sub validate_value {
    my $self = shift;
    my ( $value, $params) = @_;
    my $c = $self->form->stash->{context};

    return 1 if ! $value;

    return 1 if $c->model('DB::DastaList')->search( 
        { 
            id   => $value, 
            list => 'VZP_ODB',
        } 
    )->count;

    die HTML::FormFu::Exception::Validator->new({
        message => 'Neplatný kód spacializace',
    });
}

1;

