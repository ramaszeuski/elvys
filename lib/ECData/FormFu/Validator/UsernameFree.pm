package ECData::FormFu::Validator::UsernameFree;

use strict;
use utf8;

use base 'HTML::FormFu::Validator';
use YAML;

sub validate_value {
    my $self = shift;
    my ( $value, $params ) = @_;
    my $c = $self->form->stash->{context};

    return 1 if $c->stash->{login} && $c->stash->{login} eq $value;

    return 1 if $c->model('DB::Login_view')->search( { username => { ilike => $value} } )->count == 0;

    die HTML::FormFu::Exception::Validator->new({
        message => 'Zadané uživatelské jméno je již použito',
    });
}

1;


