package ECData::Controller::Commissions;
use Moose;
use namespace::autoclean;

use utf8;

use constant BASE_URL => '/commissions/';

BEGIN {extends 'Catalyst::Controller'; }

=head1 NAME

ECData::Controller::Commissions - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut

sub base :Chained('/') :PathPart('commissions') :CaptureArgs(0) {
    my ($self, $c) = @_;
}

sub period :Chained('base') :PathPart('') :CaptureArgs(1) {
    my ($self, $c, $id) = @_;

    my $period = $c->model('DB::CommissionPeriod')->find($id);

    die "Period '$id' not found!" if ! $period;

    $c->stash(
        period => $period,
    ); 
}  

=head2 index

=cut

sub index :Path :Args(0) {
    my ( $self, $c ) = @_;

    $c->assert_user_roles( 'root', 'dealers_manager' );

    my $periods = $c->model('DB::CommissionPeriod')->search(
        {},
        {
            order_by => [
                { -asc => 'year' },
                { -asc => 'quarter' },
            ]           
        }
    );

    my @periods          = ();
    my $allow_calculate = 0;

    while (my $period = $periods->next) {

        if ( $period->is_ready_to_calculate ) {
            $allow_calculate++;            
        } 

        push @periods, {
            $period->get_columns,
            allow_calculate => ( $allow_calculate == 1 ) ? 1 : 0,
        };            
    }

    $c->stash->{periods} = [ reverse @periods ];
}

sub save_parameter :Chained('period') :PathPart('save_parameter') {
    my ( $self, $c ) = @_;
    $c->assert_user_roles( 'root', 'dealers_manager' );

    my $period = $c->stash->{period};
    my $args  = $c->request->params;

    my $value = $args->{value};
    my $field = $args->{field};

    $value = undef if ! length $value;

    $c->stash->{output} = 'json';

    # formalni validace
    if ( defined $value ) {
        if ( ($field eq 'coefficient') && $value !~ /^\d+(\.\d+)*$/ ) {
            $c->stash->{json}{error} = "Neplatný koefficient '$value'";
            return;
        }
        if ( ($field eq 'consultation_price') && $value !~ /^\d+$/ ) {
            $c->stash->{json}{error} = "Neplatný počet bodů '$value'";
            return;
        }
    }

    $period->update( { $field => $value } );

    $c->stash->{json} = { rc => 'OK' };

}    

sub calculate :Chained('period') :PathPart('calculate') {
    my ( $self, $c ) = @_;
    $c->assert_user_roles( 'root', 'dealers_manager' );

    my $period = $c->stash->{period};
    my $args   = $c->request->params;

    my $scope_guard  = $c->model('DB')->schema->txn_scope_guard;

    my $customer_credits = $c->model('DB::Credit_View')->search(
        {
            year              => $period->year, 
	        quarter           => $period->quarter,
	        allow_commissions => 1,
        },
        {
            select => [
                'customer_id', 
                'bonusprogramm_percents', 
                { sum => 'credits' } 
            ],
            as => [
                'customer_id', 
                'bonusprogramm_percents', 
                'credits' 
            ],
	        group_by => [
                'customer_id', 
                'bonusprogramm_percents',
            ],
        }
    );

    $period->commissions->delete();

    CUSTOMER:
    while ( my $credits = $customer_credits->next ) {

        my $customer = $credits->customer;

        # predesle obdobi
        my $last = $customer->commissions(
            {
                '-or' => [				
                    { 'period.year' => {'<', $period->year } },
	                -and => [
                        { 'period.year' => $period->year },
                        { 'period.quarter' => { '<', $period->quarter} },
	                ]
	            ]
	        }, 
            { 
                join => 'period',
	 	        rows => 1, 
	 	        order_by => [
                    { '-desc' => 'year' },
                    { '-desc' => 'quarter' },
                ], 
	        }
        )->first;

        my %commissions = (
            customer_id => $credits->customer_id 
        );

# zakladni vypocet
        $commissions{calculated} = (
            $period->coefficient 
            * $credits->credits 
            * $credits->bonusprogramm_percents
        ) / 100;

# prevod z minuleho obdobi
        if ( $last ) { 
            $commissions{transfered} 
                = $last->commissions 
                - $last->invoiced 
                + $last->transfer
            ;
        }
# pocet konzultaci
        $commissions{consultations} = int(
            ( $commissions{calculated} + $commissions{transfered} ) 
            / $period->consultation_price
        );

# realni provize
        $commissions{commissions} 
            = $commissions{consultations} 
            * $period->consultation_price
        ;

# hodnota k prevodu
        $commissions{transfer} 
            = $commissions{calculated} 
            + $commissions{transfered} 
            - $commissions{commissions}
        ; 

        $period->add_to_commissions ( \%commissions);
    }

    $period->update( { calculated => \'now()' });

    $c->model('DB::Log')->create(
        {
            event         => 'commissions_calculate',
            user_username => $c->user->username,
            address       => $c->req->address,
            data          => $period->id,
        }
    );

    $scope_guard->commit;

    $c->response->redirect($c->uri_for(BASE_URL));

}    
=head1 AUTHOR

Andrej Ramašeuski

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;
