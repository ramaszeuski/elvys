package ECData::Controller::Dealers;
use Moose;
use namespace::autoclean;
use utf8;

BEGIN {extends 'ECData::Controller::Users'; }

use constant REDIRECT_URL => '/dealers/';
use constant ADD_VALID    => { roles => ['dealer'] };  

sub auto :Private {
    my ($self, $c) = @_;
    $c->assert_user_roles( 'dealers_manager' );
}    
=head1 NAME

ECData::Controller::Dealers - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut

sub base :Chained('/') :PathPart('dealers') :CaptureArgs(0) { 
    my ($self, $c) = @_; 
} 

sub index :PathPart('') :Args(0)  {
    my ( $self, $c ) = @_;

    my $users = $c->model('DB::User')->search(
        {
            'user_roles.role_role' => 'dealer',
        },
        {
            order_by => ['lastname', 'firstname'],           
            join     => ['user_roles'],
        }
    );

    my @users = ();

    while (my $user = $users->next) {
        push @users, $user;
    }

    $c->stash->{users} = \@users;

}

sub add_FORM_RENDER {
    my ( $self, $c ) = @_;

    my $form = $c->stash->{form};
    $form->auto_constraint_class( 'constraint_%t' );

    $form->remove_element( $form->get_field( {name => 'roles'} ));
    $form->remove_element( $form->get_element( {id   => 'roles_label'} ));

}

sub edit_FORM_RENDER {
    my ( $self, $c ) = @_;

    my $form = $c->stash->{form};
    my $user = $c->stash->{user}; 

    $form->model->default_values($user);


    $form->remove_element( $form->get_field( {name => 'roles'} ));
    $form->remove_element( $form->get_element( {id   => 'roles_label'} ));
                
}

sub credits :Chained('user') :PathPart('credits') :Args(0) {
    my ( $self, $c ) = @_;

    $c->stash->{credits} = $c->model('DB::Credit_view')->summary_matrix(
        {
            dealer_account_login => $c->stash->{user}->username,
            dealer_contract      => 't',
        }
    );
}

sub credits_month :Chained('user') :PathPart('credits') :Args(2) {
    my ( $self, $c, $year, $month ) = @_;
    $c->assert_any_user_role( 'root', 'dealers_manager' );

    $c->stash->{conditions} = {
        dealer_account_login => $c->stash->{user}->username,
        dealer_contract      => 't',
    };            

    $c->stash->{title} .= " za $month.$year";

    $c->controller('Credits')->month($c, $year, $month);

}
=head1 AUTHOR

Andrej Ramašeuski

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;
