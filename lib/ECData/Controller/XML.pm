package ECData::Controller::XML;
use Moose;
use namespace::autoclean;

use YAML;
use ECData::Storage;
use Date::Manip;

Date_Init('DateFormat=non-US');

BEGIN {extends 'Catalyst::Controller::HTML::FormFu'; }

=head1 NAME

ECData::Controller::XML - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut

sub base :Chained('/') :PathPart('xml') :CaptureArgs(0) { 
    my ($self, $c) = @_; 
} 

sub xml :Chained('base') :PathPart('') :CaptureArgs(1) {
    my ($self, $c, $id) = @_;

    my $xml = $c->model('DB::XML')->find($id);
    die "XML '$id' not found!" if ! $xml;

    if ( $c->check_user_roles('customer')) {
        die 'Access denied' if $xml->customer_id != $c->user->subject->id;        
    }
    if ( $c->check_user_roles('lab')) {
        die 'Access denied' if $xml->lab_id != $c->user->subject->id;        
    }            

    $c->stash->{xml} = $xml;
}

=head2 index

=cut

sub index :PathPart('') :Args(0)  :FormConfig('filter_xml.yaml') {
    my ( $self, $c ) = @_;
    if ( $c->check_any_user_role('root')) {
        $c->stash->{show_customer} = 1;
        $c->stash->{show_lab}      = 1;
    }        
    if ( $c->check_user_roles('customer')) {
        $c->stash->{show_lab}      = 1;
    }
    if ( $c->check_user_roles('lab')) {
        $c->stash->{show_customer} = 1;
    }

    # defaultni filtr
    if ( ! defined $c->session->{filter_xml} ) {
        $c->session->{filter_xml}{show_downloaded} = 0;
    }
}

sub index_FORM_RENDER {
    my ( $self, $c ) = @_;

    my $form = $c->stash->{form} // return;

    if ( $c->check_any_user_role ('root', 'supervisor', 'customer') ) {
        $form->get_field( {name => 'lab_id'} )
            ->options(
                $c->controller('Labs')->options($c)
            )
            ->value($c->session->{filter_xml}{lab_id})
        ;
    }        
    else {
        $form->remove_element( $form->get_field( {name => 'lab_id'} ) );
    }
    
    if ( $c->check_any_user_role ('customer' )) {
        $form->remove_element( $form->get_field( {name => 'customer'} ) );
    }        

    FIELD:
    foreach my $field (qw(
        generated_begin 
        generated_end
        customer
        show_downloaded
    )) {
        $form->get_field( {name => $field} )
            ->default($c->session->{filter_xml}{$field})
        ;
    }

}

sub set_filter :Chained('base') :PathPart('set_filter') :Args(0) {
    my ( $self, $c ) = @_;

    my $args = $c->request->params;

    $c->session->{filter_xml} = {};                   

    FIELD:
    foreach my $field (qw(
        lab_id
        generated_begin 
        generated_end
        customer
        show_downloaded
    )) {
        if ( $args->{$field}) {
            $c->session->{filter_xml}{$field} = $args->{$field};        
        }
        else {
            delete $c->session->{filter_xml}{$field};                   
        }
    }

    $c->response->body('OK');
}

sub json :Chained('base') :PathPart('json') :Args(0) {
# http://richard.wallman.org.uk/2011/12/howto-use-jquerydatatables-with-perlcatalystdbic/
    my ( $self, $c ) = @_;

    my $args = $c->request->params;

    my $conditions;
    my @where = ();

    # nucene omezujici podminky dle opravneni
    if ( ! $c->check_any_user_role('root')) {
        if ( $c->check_user_roles('customer')) {
            my @customer_ids = $c->user->customer->ids;
            $conditions->{customer_id} = { in => \@customer_ids};        
        }
        if ( $c->check_user_roles('lab')) {
            $conditions->{lab_id} = $c->user->subject->id;        
        }            
    }        

    my $unfiltered_count = $c->model('DB::XML_view')->count(
        $conditions,
    );


#TODO: do samostatne tridy!!!
    my @columns = ();   
    COLUMN:
    for my $n ( 0 .. $args->{iColumns} - 1 ) {
        my $column = $args->{'mDataProp_' . $n};

        push @columns, $column;        

        my $search = $args->{'sSearch_' . $n};
        if ( defined $search && length( $search) ) {
            $conditions->{$column} //= {
                ilike => '%' . $search . '%',
            };            
        }
    }

    my @sorting;
    SORT_COLUMN:
    for my $n ( 0 .. $args->{iSortingCols} - 1 ) {
        next SORT_COLUMN if not exists $args->{"iSortCol_$n"};
        push @sorting, {'-'. $args->{"sSortDir_$n"} => $columns[$args->{"iSortCol_$n"}]};
    }       

    # TODO: samostatna funkce make filter conditions
    if ( $c->session->{filter_xml}{lab_id} && ! $c->check_user_roles('lab') ) {
        $conditions->{lab_id} = $c->session->{filter_xml}{lab_id};        
    }
    
    # datum zpracovani    
    if ( $c->session->{filter_xml}{generated_begin} ) {
        my $date = ParseDate($c->session->{filter_xml}{generated_begin});
        if ($date) {
            push @where, [ 
                'me.generated' => { '>=' => UnixDate($date, '%Y-%m-%d')} 
            ];
        }
    }

    if ( $c->session->{filter_xml}{generated_end} ) {
        my $date = ParseDate($c->session->{filter_xml}{generated_end});
        if ($date) {
            push @where, [ 
                'me.generated' => { '<=' => UnixDate($date, '%Y-%m-%d 23:59:59')} 
            ];
        }
    }

    if ( $c->session->{filter_xml}{customer} && ! $c->check_user_roles('customer') ) {
        my $search = '%' . $c->session->{filter_xml}{customer} . '%';
        push @where, [
            '-or' => [ 
                customer_name => { ilike => $search },
                customer_icz  => { like  => $search },
                customer_icp  => { like  => $search },
            ],
        ];
    }        

    if ( ! $c->session->{filter_xml}{show_downloaded} ) {
        push @where, [
            'me.downloaded' => undef,
        ]
    }            

    $conditions->{-and} = \@where if scalar @where;

    my $filtered_count = $c->model('DB::XML_view')->count(
        $conditions
    );

    my $xmls = $c->model('DB::XML_view')->search(
        $conditions,
        {
            order_by => \@sorting,
            rows     => $args->{iDisplayLength},
            offset   => $args->{iDisplayStart},
             
        }
    );

    my @xmls = ();

    while (my $xml = $xmls->next) {
        push @xmls, {
            id            => $xml->id,
            lab           => $xml->lab_icz . '/' . $xml->lab_icp,
            lab_name      => substr($xml->lab_name, 0, 25),
            customer_name => $xml->customer_name,
            generated     => $xml->timestamp('generated'),
            downloaded    => $xml->timestamp('downloaded'),
            checked       => $xml->downloaded ? 0 : 1,
        }, 
    }

    $c->stash(
        output => 'json',
        json    => {
            sEcho                => $args->{sEcho},
            aaData               => \@xmls,
            iTotalRecords        => $unfiltered_count,
            iTotalDisplayRecords => $filtered_count,
        },
    );

}

sub download :Chained('base') :PathPart('download') :Args(0) {
    my ( $self, $c ) = @_;
 
    my $args = $c->request->params;
    my $storage = ECData::Storage->new( $c->config->{workdir} );

    my @customer_ids = $c->user->customer->ids;
    my $conditions = { customer_id => { in => \@customer_ids} };

    if ( $args->{download_all} ) {
        # otazka, zda nebylo by lepsi pro oznaceni stazenych vyuzit seznam
        $conditions->{downloaded} = undef;
    } 
    else {
        $conditions->{id} = $args->{id};
    }

    my $scope_guard = $c->model('DB')->schema->txn_scope_guard;

    my $xmls = $c->model('DB::XML')->search( 
        $conditions,
        {
            columns => ['id'],
            rows    => 1000,
        } 
    );
    $xmls->update(
        {
            downloaded => \'now()'
        }
    );
    $c->model('DB::Log')->create(
        {
            event         => 'xml_download',
            user_username => $c->user->username,
            address       => $c->req->address,
        }
    );

    $scope_guard->commit;

    my @ids;

    XML:
    while ( my $xml = $xmls->next ) {
        push @ids, $xml->id;
    }

    my $zip = $storage->zip(\@ids); 

    $c->res->content_type('application/zip');
    $c->res->header('Content-Disposition', 'attachment; filename="' . $zip->{filename} . '"');
    $c->serve_static_file( $zip->{tempdir} . '/' . $zip->{filename} );

}

sub view :Chained('xml') :PathPart('view') :Args(0)  {
    my ( $self, $c ) = @_;
    my $storage = ECData::Storage->new( $c->config->{workdir} );

    $c->stash->{content} = $storage->get_as_string( 
        $c->stash->{xml}->id 
    );

}    

=head1 AUTHOR

Andrej Ramašeuski

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;
