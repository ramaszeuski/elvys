package ECData::Controller::Results;
use Moose;
use namespace::autoclean;

use ECData::Storage;
use YAML;
use Encode;
use utf8;
use Date::Manip;
use File::Slurp;
use File::Path qw(make_path remove_tree);

Date_Init('DateFormat=non-US');

BEGIN {extends 'Catalyst::Controller::HTML::FormFu'; }

=head1 NAME

ECData::Controller::Results - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut


=head2 index

=cut

sub base :Chained('/') :PathPart('results') :CaptureArgs(0) {
    my ($self, $c) = @_;
}

sub result :Chained('base') :PathPart('') :CaptureArgs(1) {
    my ($self, $c, $id) = @_;

    my $result = $c->model('DB::Result')->find($id);
    $result ||= $c->model('DB::ResultArchive')->find($id);
    die "Result '$id' not found!" if ! $result;

    if ( $c->check_user_roles('customer')) {
        my %customer_ids = map { $_ => 1 } $c->user->customer->ids;
        die 'Access denied' if not exists $customer_ids{ $result->xml->customer_id };
    }
    if ( $c->session->{icp} ) {
        my %icp = map { $_ => 1 } ( split /\D/, $c->session->{icp} );
        die 'Access denied' if not exists $icp{ $result->view->customer_icp };
    }
    if ( $c->check_user_roles('lab')) {
        die 'Access denied' if $result->xml->lab_id != $c->user->subject->id;
    }

    $c->stash->{result} = $result;
}

sub index :PathPart('') :Args(0)  :FormConfig('filter_result.yaml') {
    my ( $self, $c ) = @_;
    if ( $c->check_any_user_role('root', 'dealer') ) {
        $c->stash->{show_customer} = 1;
        $c->stash->{show_lab}      = 1;
#        if ( ! $c->check_any_user_role('root')) {
#            $c->stash->{hide_patient_name} = 1;
#        }
    }
    if ( $c->check_user_roles('customer') || $c->session->{icp}) {
        $c->stash->{show_lab}      = 1;
        $c->stash->{show_customer} = 1;
    }
    if ( $c->check_user_roles('lab')) {
        $c->stash->{show_customer} = 1;
    }
    if ( $c->check_user_roles('customer')) {
        $c->stash->{direct_results} =
            $c->user->customer->direct_results
            &&
            $c->config->{feature}{show_value_in_list}
        ;
    }
}

sub index_FORM_RENDER {
    my ( $self, $c ) = @_;

    my $form = $c->stash->{form};

    if ( $c->check_any_user_role ('root', 'supervisor', 'customer')  || $c->session->{icp}) {
        $form->get_field( {name => 'lab_id'} )
            ->options(
                $c->controller('Labs')->options($c)
            )
            ->value($c->session->{filter_results}{lab_id})
        ;
    }
    else {
        $form->remove_element( $form->get_field( {name => 'lab_id'} ) );
    }

    if ( $c->check_any_user_role ('customer' ) || $c->session->{icp}) {
        $form->remove_element( $form->get_field( {name => 'customer'} ) );
    }

    FIELD:
    foreach my $field (qw(
        sampling_begin
        sampling_end
        generated_begin
        generated_end
        customer
        patient
        show_archive
    )) {
        $form->get_field( {name => $field} )
            ->default($c->session->{filter_results}{$field})
        ;
    }

}

sub set_filter :Chained('base') :PathPart('set_filter') :Args(0) {
    my ( $self, $c ) = @_;

    my $args = $c->request->params;

    $c->session->{filter_results} = {};

    FIELD:
    foreach my $field (qw(
        lab_id
        sampling_begin
        sampling_end
        generated_begin
        generated_end
        customer
        patient
        show_archive
    )) {
        if ( $args->{$field}) {
            $c->session->{filter_results}{$field} = $args->{$field};
        }
        else {
            delete $c->session->{filter_results}{$field};
        }
    }
    $c->response->body('OK');
}

sub json :Chained('base') :PathPart('json') :Args(0) {
# http://richard.wallman.org.uk/2011/12/howto-use-jquerydatatables-with-perlcatalystdbic/
    my ( $self, $c ) = @_;

    my $args = $c->request->params;

    my $conditions;
    my @where = ();

    my $model = $c->session->{filter_results}{show_archive}
              ? $c->model('DB::ResultAll_view')
              : $c->model('DB::Result_view')
              ;


    # nucene omezujici podminky dle opravneni
    if ( ! $c->check_any_user_role('root')) {
        if ( $c->check_user_roles('customer')) {
            my @customer_ids = $c->user->customer->ids;
            $conditions->{customer_id} = { in => \@customer_ids};
        }
        if ( $c->check_user_roles('lab')) {
            $conditions->{lab_id} = $c->user->subject->id;
        }
#        if ( $c->check_user_roles('dealer')) {
#            $conditions->{dealer_account_login} = $c->user->username;
#        }
        if ( $c->session->{icp} ) {
            $conditions->{customer_icp} = [ split /\D/, $c->session->{icp}];
        }
    }

    my $unfiltered_count = $model->count(
        $conditions,
    );


#TODO: do samostatne tridy!!!
    my @columns = ();
    COLUMN:
    for my $n ( 0 .. $args->{iColumns} - 1 ) {
        my $column = $args->{'mDataProp_' . $n};

        push @columns, $column;

        my $search = $args->{'sSearch_' . $n};
        if ( defined $search && length( $search) ) {
            $conditions->{$column} //= {
                ilike => '%' . $search . '%',
            };
        }
    }

    my @sorting;
    SORT_COLUMN:
    for my $n ( 0 .. $args->{iSortingCols} - 1 ) {
        next SORT_COLUMN if not exists $args->{"iSortCol_$n"};
        if ($columns[$args->{"iSortCol_$n"}] eq 'name') { # TODO: mapping v constant
            push @sorting, {'-'. $args->{"sSortDir_$n"} => 'lastname'};
            push @sorting, {'-'. $args->{"sSortDir_$n"} => 'firstname'};
        }
        else {
            push @sorting, {'-'. $args->{"sSortDir_$n"} => $columns[$args->{"iSortCol_$n"}]};
        }
    }

    # TODO: samostatna funkce make filter conditions
    if ( $c->session->{filter_results}{lab_id} && ! $c->check_user_roles('lab') ) {
        $conditions->{lab_id} = $c->session->{filter_results}{lab_id};
    }

    # datum odberu
    if ( $c->session->{filter_results}{sampling_begin} ) {
        my $date = ParseDate($c->session->{filter_results}{sampling_begin});
        if ($date) {
            push @where, [
                'me.sampling' => { '>=' => UnixDate($date, '%Y-%m-%d')}
            ];
        }
    }

    if ( $c->session->{filter_results}{sampling_end} ) {
        my $date = ParseDate($c->session->{filter_results}{sampling_end});
        if ($date) {
            push @where, [
                'me.sampling' => { '<=' => UnixDate($date, '%Y-%m-%d')}
            ];
        }
    }

    # datum zpracovani
    if ( $c->session->{filter_results}{generated_begin} ) {
        my $date = ParseDate($c->session->{filter_results}{generated_begin});
        if ($date) {
            push @where, [
                'me.generated' => { '>=' => UnixDate($date, '%Y-%m-%d')}
            ];
        }
    }

    if ( $c->session->{filter_results}{generated_end} ) {
        my $date = ParseDate($c->session->{filter_results}{generated_end});
        if ($date) {
            push @where, [
                'me.generated' => { '<=' => UnixDate($date, '%Y-%m-%d 23:59:59')}
            ];
        }
    }

    if ( $c->session->{filter_results}{customer} && ! $c->check_user_roles('customer') ) {
        my $search = '%' . $c->session->{filter_results}{customer} . '%';
        push @where, [
            '-or' => [
                customer_name => { ilike => $search },
                customer_icz  => { like  => $search },
                customer_icp  => { like  => $search },
            ],
        ];
    }

    if ( $c->session->{filter_results}{patient} ) {
        my $search = '%' . $c->session->{filter_results}{patient} . '%';
        push @where, [
            '-or' => [
                firstname   => { ilike => $search },
                lastname    => { ilike => $search },
                name_cz_off => { ilike => $search },
                rc          => { like  => $search },
            ],
        ];
    }
    $conditions->{-and} = \@where if scalar @where;

    my $filtered_count = $model->count(
        $conditions
    );

    my $results = $model->search(
        $conditions,
        {
            order_by => \@sorting,
            rows     => $args->{iDisplayLength},
            offset   => $args->{iDisplayStart},
#            prefetch => 'owner',

        }
    );

    my @results = ();

    while (my $result = $results->next) {
        push @results, {
            id            => $result->id,
            lab           => $result->lab_icz . '/' . $result->lab_icp,
            lab_name      => substr($result->lab_name, 0, 25),
            customer_name => $result->customer_name,
            customer_icz  => $result->customer_icz,
            customer_icp  => $result->customer_icp,
            rc            => $result->rc,
            sampling      => $result->date('sampling'),
            generated     => $result->date('generated'),
            name          => (join ' ', $result->lastname, $result->firstname),
            value         => $result->value,
        },
    }

    $c->stash(
        output  => 'json',
        json    => {
            sEcho                => $args->{sEcho},
            aaData               => \@results,
            iTotalRecords        => $unfiltered_count,
            iTotalDisplayRecords => $filtered_count,
        },
    );

}

sub detail :Chained('result') :PathPart('detail') :Args(0)  {
    my ( $self, $c ) = @_;
    my $storage = ECData::Storage->new( $c->config->{workdir} );
    my $xml     = $storage->get_xml( $c->stash->{result}->xml_id );

    # najit blok dat pacienta dle rodneho cisla
    IP:
    foreach my $ip ( @{$xml->{is}{ip}} ) {
        if ( $ip->{rodcis}{content} eq $c->stash->{result}->patient->rc ) {
            $xml = $ip;
            $c->stash->{patient} = {
                firstname => $ip->{jmeno}{content} // $c->stash->{result}->patient->firstname,
                lastname  => $ip->{prijmeni}{content} // $c->stash->{result}->patient->lastname,
            };
            last IP;
        }
    }

    # pojistovna
    $c->stash->{insurance} = {
        id   => $xml->{p}{kodpoj}{content},
        name => dasta( $c, 'ZDRPOJ', $xml->{p}{kodpoj}{content} ),
    };

    # diagnozy
    my @diagnoses = ();

    DIAGNOSIS:
    foreach my $diagnosis_xml ( @{ $xml->{dg}{dgz}} ) {
        push @diagnoses, {
            id   => $diagnosis_xml->{diag}{content},
            name => dasta( $c, 'MKN10_5', $diagnosis_xml->{diag}{content} ),
        };
    }
    if ( scalar @diagnoses ) {
        $c->stash->{diagnoses} = \@diagnoses;
    }

    # vysledky
    my $tests;
    TEST:
    foreach my $test_xml ( @{ $xml->{v}{vr} } ) {
        my $layout = 'vrn';

        my $name_nclp = dasta( $c, 'NCLPPOL', $test_xml->{klic_nclp} );

        my $test = {
            id           => $test_xml->{klic_nclp},
            name         => $test_xml->{nazev_lclp}{content} || $name_nclp,
            name_nclp    => $name_nclp,
            sampling     => format_date($test_xml->{dat_du}{content}),
            type         => $test_xml->{typpol_fh},
            id_lis       => $test_xml->{id_lis},
        };

        if ( $test_xml->{typpol_fh} =~ /^[TE\d]$/ ) {
            $layout = 'vrn';
            my $block = $test_xml->{vrn};
            $test->{value}     = $block->{hodnota}{content};
            $test->{note}      = $block->{pozn}{content},
            $test->{error}     = $block->{nejistota}{nejst_var1};
            $test->{item}      = $block->{nazvy}{jednotka} || $block->{jednotka} || $block->{prepocet}{jednotka_lclp};
            $test->{min}       = $block->{skala}{s4}{content}; #TODO: zabarvit
            $test->{max}       = $block->{skala}{s5}{content};
            $test->{interpret} = $block->{skala}{interpret_g_z}{content};

            # generovani alertu
            if ( defined $test->{value} && $test->{value} =~ /^\d+([\.,]\d+)?$/) {

                my $value = $test->{value};
                $value =~ s/,/\./;

                if ( exists $test->{min} && $test->{min} =~ /^\d+([\.,]\d+)?$/) {
                    my $min = $test->{min};
                    $min =~ s/,/\./;
                    if ( $value < $min ) {
                        $test->{alert} = 1;
                    }
                }

                if ( exists $test->{max} && $test->{max} =~ /^\d+([\.,]\d+)?$/) {
                    my $max = $test->{max};
                    $max =~ s/,/\./;
                    if ( $value > $max ) {
                        $test->{alert} = 1;
                    }
                }
            }

            # priznak_kvant
            if ($block->{priznak_kvant} eq 'M') {
                $test->{value}  = '<' . $test->{value};
            }
            if ($block->{priznak_kvant} eq 'V') {
                $test->{value}  = '>' . $test->{value};
            }

        }
        elsif ( $test_xml->{typpol_fh} eq 'X' ) {
            my $block = $test_xml->{vrx};
            $test->{value} = $block->{hodnota_nt}{content};
            $test->{note}  = $block->{pozn}{content};

            if ( $c->config->{feature}{show_vrx_as_vrn} ) {
                $layout = 'vrn';
            }
            else {
                $layout = 'vrx';
            }
        }
        elsif ( $test_xml->{typpol_fh} eq 'B' ) {
            $layout = 'vrb';
            my $block = $test_xml->{vrb};
            $test->{value}  = $block->{text}{ptext}{content};
        }
        elsif ( $test_xml->{typpol_fh} eq 'R' ) {
            $layout = 'vrr';
            my $block = $test_xml->{vrr};
            $test->{value}  = $block->{text}{ptext}{content};
        }

#TODO:  F: vrf

        push @{ $tests->{ $layout } }, $test;
    }
    $c->stash->{tests} = $tests;

    if ( $c->check_any_user_role('root')) {
        $c->stash->{source_xml} = $storage->get_as_string(
            $c->stash->{result}->xml_id
        );
    }
    else {
#        if ( $c->check_any_user_role('dealer') ) {
#            $c->stash->{hide_patient_name} = 1;
#        }
    }

    if ( $c->req->param('format') && $c->req->param('format') eq 'pdf' ) {
        $c->stash->{no_wrapper} = 1;

        my $vendor_header = $c->path_to( 'vendor', $c->config->{vendor}, 'pdf', 'head.pdf' );

        if (-f $vendor_header) {
            $c->stash->{header} = $vendor_header;
        }
        else {
            $c->stash->{header} = $c->path_to( 'root','pdf', 'head.pdf' );
        }

        my $latex = $c->view("HTML")->render($c, 'results/detail_tex.tt2');

        my $latex_dir = '/tmp/' . $c->stash->{result}->id;
        make_path($latex_dir);

        write_file( "$latex_dir/result.tex", {binmode => ':utf8'}, $latex ) ;
        `pdflatex -interaction batchmode -output-directory $latex_dir result.tex`;

        binmode STDOUT;
        $c->response->content_type('application/pdf');
        $c->response->header('Content-Disposition', 'attachment; filename="' . $c->stash->{result}->id . '.pdf"');
        $c->serve_static_file( "$latex_dir/result.pdf" );
        remove_tree($latex_dir);

    }

}

sub dasta {
    my ($c, $list, $id) = @_;

    my $record = $c->model('DB::DastaList')->find(
        {
            list => $list,
            id   => $id,
        }
    );

    return '' if ! $record;

    if ( $list eq 'NCLPPOL' ) {
       return $record->content->{nkomp};
    }
    elsif ( $list eq 'ZDRPOJ' ) {
        return $record->content->{naz};
    }
    elsif ( $list eq 'MKN10_5' ) {
        return $record->content->{naz};
    }
}

# TODO: UNIFIKACE!
sub format_date {
    my $date = shift;

    if ( $date =~ /(\d{4})\-(\d\d)-(\d\d)\D(\d\d:\d\d)/) {
        return "$3.$2.$1";
#        return "$3.$2.$1 $4";
    }
    else {
        return $date;
    }
}

=head1 AUTHOR

Andrej Ramašeuski

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;

__END__
 for ($source->getElementsByTagName('ptext')) {
  my $content = $_->textContent;
	$content = wrap('', '', $content);
  my $new = XML::LibXML::Element->new( 'ptext' );
	$new->appendText($content);
	$_->replaceNode($new);
 }

# opravy invalidniho LIRS XML
 my ($is) = $source->getElementsByTagName('is');
 unless ($is->getAttribute('icz') || $is->getAttribute('icp') || $is->getAttribute('ico')) {
  my $a = XML::LibXML::Element->new( 'a' );
  my %a;
  $a{$_} = XML::LibXML::Element->new( $_ ) for qw/jmeno adr mesto/;
  $a{jmeno}->appendText($xml->is->jmeno);
  $a{adr}->appendText($xml->is->adr);
  $a{mesto}->appendText($xml->is->mesto);
  $a->addChild($a{$_}) for qw/jmeno adr mesto/;
  $is->replaceChild($a, $is->getElementsByTagName('a'));
 }

#cistka neciselnich id_pac
 if ($id_pac =~ s/\D+//g) {
  for ($source->getElementsByTagName('ip')) {
   my $id = $_->getAttribute('id_pac');
	 if ($id =~ s/\D+//g) {
    $_->setAttribute('id_pac', $id_pac);
	 }
  }
 }

# doplnovani chybejiciho jmena doktora
 my ($pm) = $source->getElementsByTagName('pm');
 my ($a) = $pm->getChildrenByTagName('a');
 unless ($a) {
  $a = XML::LibXML::Element->new( 'a' );
  $a = $pm->addChild($a);
 }

 my ($jmeno) = $a->getChildrenByTagName('jmeno');
 unless ($jmeno) {
  $jmeno = XML::LibXML::Element->new( 'jmeno' );
  my $pm = $DBIC->resultset('pm')->find($pm->getAttribute('icz') || $pm->getAttribute('icz'));
  $jmeno->appendText($pm->name) if $pm;
  $a->addChild($jmeno);
 }
sub detail1 :Chained('result') :PathPart('detail') :Args(0)  {
    my ( $self, $c ) = @_;

    my $storage = ECData::Storage->new( $c->config->{workdir} );
    my $xml     = $storage->get_xml( $c->stash->{result}->xml_id );

    my $xml_ip;

    # najit blok dat pacienta dle rodneho cisla
    IP:
    foreach my $ip ( @{$xml->{is}{ip}} ) {
        if ( $ip->{rodcis}{content} eq $c->stash->{result}->patient->rc ) {
            $xml_ip = $ip;
            last IP;
        }
    }

    # pojistovna
    $c->stash->{insurance} = $insurance;

    $c->stash->{xml} = $xml_ip;
}
