package ECData::Controller::Customers;
use Moose;
use namespace::autoclean;
use Date::Manip;
use utf8;

BEGIN {extends 'Catalyst::Controller::HTML::FormFu'; }

use constant BASE_URL => '/customers/';

use constant LIST_MODEL => 'DB::Customer_view';

use constant FILTER_NAME   => 'filter_customer';

use constant FILTER_FIELDS => {
    dealer_account_login => {},
    dealer_contract      => {},
    primary_lab_id       => {},
    clinic_id            => {},
    specialization_code  => { type => 'ilike', },
    name                 => { type => 'ilike', },
    login                => { type => 'ilike', },
    icx           => {
        type    => 'like',
        columns => [ qw(ico icp icz) ],
    },
};


sub auto :Private {
    my ($self, $c) = @_;
    $c->assert_any_user_role( qw(
        root
        dealer
        dealers_manager
        customers_manager
        customers_supervisor
    ) );
}
=head1 NAME

ECData::Controller::Customers - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut

sub base
    :Chained('/')
    :PathPart('customers')
    :CaptureArgs(0)
{
    my ($self, $c) = @_;
}

sub customer
    :Chained('base')
    :PathPart('')
    :CaptureArgs(1)
{
    my ($self, $c, $id) = @_;
    my $customer = $c->model('DB::Customer')->find($id);

    $c->stash(
        customer => $customer,
        title    => join ' ', (
            $customer->icp,
            $customer->name,
        )
    );

    $c->stash->{login} = $customer->login;

    if (
        (
            $customer->dealer_account_login &&
            $customer->dealer_account_login eq $c->user->username
        )
        ||
        $c->check_any_user_role(
            'root',
            'customers_manager',
            'dealers_manager',
        )
    ) {
        $c->stash->{editable} = 1;
    }
    elsif (
        $c->check_any_user_role(
            'dealer',
        )
        &&
        ! $customer->take_requests(
            {
                state                => 0,
                dealer_account_login => $c->user->username,
            }
        )->count()
    ) {
        $c->stash->{takeable} = 1;
    }

    die "Customer '$id' not found!" if ! $c->stash->{customer};

    $c->session->{customer_id} = $customer->id; #TODO: hack
}

sub add :Chained('base') :PathPart('add') :Args(0) :FormConfig('customer_add.yaml') {
    my ( $self, $c ) = @_;
    $c->assert_any_user_role( 'dealer', 'root', 'customer_manager' );
}

sub add_FORM_RENDER {
    my ( $self, $c ) = @_;

    my $form = $c->stash->{form};

    $form->auto_constraint_class( 'constraint_%t' );

    if ( ! $c->check_any_user_role( 'root','customers_manager' ) ) {
        my $dealer_account_login = $form->get_field(
            { name => 'dealer_account_login' }
        );

        $dealer_account_login->parent->remove_element(
            $dealer_account_login
        );
    }

    if ( ! $c->config->{feature}{show_value_in_list} ) {
        my $direct_results = $form->get_field(
            { name => 'direct_results' }
        );

        $direct_results->parent->remove_element(
            $direct_results
        );
    }

    # id (login) se generuje automaticky
#    my $field_id = $form->get_field( { name => 'id', type=>'Text' } );
#    $field_id->parent->remove_element( $field_id);
}

sub add_FORM_VALID {
    my ( $self, $c ) = @_;

    my $form = $c->stash->{form};

    $form->add_valid('role', 'customer');

    my $scope_guard  = $c->model('DB')->schema->txn_scope_guard;

    my $customer = $form->model->create() || die $!;

    $c->model('DB::Log')->create(
        {
            event         => 'customer_create',
            user_username => $c->user->username,
            address       => $c->req->address,
            data          => $customer->id,
        }
    );

    $scope_guard->commit;

    $c->response->redirect($c->uri_for(BASE_URL));
    $c->detach;
}

=head2 index

=cut

sub index :PathPart('') :Args(0) :FormConfig('filter_customer.yaml') {}

sub index_FORM_RENDER {
    my ( $self, $c ) = @_;

    my $form = $c->stash->{form} // return;

    FIELD:
    foreach my $field_name ( keys %{ $self->FILTER_FIELDS() } ) {

        my $field = FILTER_FIELDS->{$field_name};

        if ( ! exists $c->session->{$self->FILTER_NAME}{$field_name} ) {
            if ( exists $field->{default} ) {
                $c->session->{$self->FILTER_NAME}{$field_name}
                    = $field->{default};
            }
            else {
                next FIELD;
            }
        }

        my $form_field = $form->get_field( { name => $field_name } );

        if ( $form_field->can('default')) {
            $form_field->default($c->session->{$self->FILTER_NAME}{$field_name});
        }
        if ( $form_field->can('value') ) {
            $form_field->value($c->session->{$self->FILTER_NAME}{$field_name});
        }
    }
}


sub json :Chained('base') :PathPart('json') :Args(0) {
    my ( $self, $c ) = @_;

    my $args  = $c->request->params;
    my $model = $c->model( $self->LIST_MODEL() );

    my ( $columns, $sorting, $conditions )
        = $c->model('DB')->datatables_args( $args );

    my $unfiltered_count = $c->model('DB::Customer')->count(
        $conditions,
    );

    my $filter = $c->session->{$self->FILTER_NAME};

    my @where = $c->model('DB')->make_conditions_from_filter(
        {
            model         => $model,
            filter        => $filter,
            filter_fields => $self->FILTER_FIELDS(),
        }
    );

    $conditions->{-and} = \@where if scalar @where;

    my $filtered_count = $model->count( $conditions, );

    my $customers = $model->search(
        $conditions,
        {
            order_by => $sorting,
            rows     => $args->{iDisplayLength},
            offset   => $args->{iDisplayStart},

        }
    );

    my @customers = ();

    while (my $customer = $customers->next) {
        push @customers, {
            id            => $customer->id,
            icp           => $customer->icp,
            login         => $customer->login,
            name          => $customer->name,
            city          => $customer->city,
            dealer_name   => $customer->dealer_name_with_contract_label,
            created       => $customer->created_date,
            changed       => $customer->changed_date,
            last_login    => $customer->last_login_date,
        },
    }

    $c->stash(
        output  => 'json',
        json    => {
            sEcho                => $args->{sEcho},
            aaData               => \@customers,
            iTotalRecords        => $unfiltered_count,
            iTotalDisplayRecords => $filtered_count,
        },
    );

}

sub edit :Chained('customer') :PathPart('') :Args(0) :FormConfig('customer_edit.yaml') {
    my ( $self, $c ) = @_;

    $c->assert_any_user_role( 'root','customers_manager', 'dealer' );

    my $form     = $c->stash->{form};
    my $customer = $c->stash->{customer};

    my $share_form = $self->form;
    $share_form->action('/customers/' . $customer->id . '/share/');
    $share_form->load_config_file('customer_share.yaml');
    $c->stash->{share_form} = $share_form;

    my $add_meeting_form = $self->form;
    $add_meeting_form->action('/customers/' . $customer->id . '/add_meeting/');
    $add_meeting_form->load_config_file('meeting.yaml');
    if ( ! $add_meeting_form->submitted ) {
        $add_meeting_form->get_field( {name => 'date'} )
            ->value( UnixDate('now', '%d.%m.%Y') );
    }
    $c->stash->{add_meeting_form} = $add_meeting_form;

}

sub edit_FORM_RENDER {

    my ( $self, $c ) = @_;

    my $form = $c->stash->{form};

    my $customer = $c->stash->{customer};

    $form->model->default_values($customer);
    $form->auto_constraint_class( 'constraint_%t' );

    if (my $password_field = $form->get_field( {name => 'password'} ) ) {
        $password_field->value('');

        # Obchodnik nemusi mit opravneni menit heslo
        if ( $c->config->{feature}{disable_dealer_password_change} ) {
            if ( ! $c->check_any_user_role( 'root' ) ) {

                $password_field->parent->remove_element(
                    $password_field
                );
            }
        }

    }

    if ($c->stash->{share_form}) {
        my @shared = (
            $self->shared_options($c, $customer),
            $self->suggested_for_sharing_options($c, $customer),
        );

        my $share = $c->stash->{share_form}->get_field(
            {
                name => 'share'
            }
        );

        $share->options( \@shared );
    }

    my $credits = $c->model('DB::Credit_view')->summary_matrix(
        {
            customer_id => $c->stash->{customer}->id,
        }
    );

    if ( $credits->{years} && scalar @{ $credits->{years}} ) {
        $c->stash->{credits} = $credits;
    }

    my @commissions = ();
    my $commissions = $customer->commissions (
        {},
        {
            prefetch => 'period',
	 	    order_by => [
                { '-desc' => 'period.year' },
                { '-desc' => 'period.quarter' },
            ],
        }
    );

    PERIOD:
    foreach my $period ( $customer->commissions ) {
        push @commissions, $period;
    }

    if ( scalar @commissions ) {
        $c->stash->{commissions} = \@commissions;
    }
}

sub edit_FORM_NOT_VALID {
    my ( $self, $c ) = @_;
    $c->flash->{rc} = 'saved';
}

sub edit_FORM_VALID {
    my ( $self, $c ) = @_;
    my $form     = $c->stash->{form};
    my $customer = $c->stash->{customer};

    my $scope_guard  = $c->model('DB')->schema->txn_scope_guard;

    if ( ! $form->param_value('password')) {
        $form->add_valid( password => $customer->password );
    }

    $form->model->update($customer);
    $customer->update({ changed => \'now()'});

    $c->model('DB::Log')->create(
        {
            event         => 'customer_edit',
            user_username => $c->user->username,
            address       => $c->req->address,
            data          => $customer->id,
        }
    );

    $scope_guard->commit;

    $c->flash->{rc} = 'saved';
    $c->response->redirect($c->uri_for('/customers/' . $customer->id . '/'));
    $c->detach;
}


sub share :Chained('customer') :PathPart('share') :Args(0) {
    my ( $self, $c ) = @_;

    my $customer     = $c->stash->{customer};

    my $scope_guard  = $c->model('DB')->schema->txn_scope_guard;

    $customer->update(
        {
            share => [ $c->req->param('share') ],
        }
    );

    $c->model('DB::Log')->create(
        {
            event         => 'customer_edit_shared',
            user_username => $c->user->username,
            address       => $c->req->address,
            data          => $customer->id,
        }
    );

    $scope_guard->commit;

    $c->flash->{rc} = 'saved_share';
    $c->response->redirect($c->uri_for('/customers/' . $customer->id . '/'));
    $c->detach;
}

sub add_meeting :Chained('customer') :PathPart('add_meeting') :Args(0) {
    my ($self, $c) = @_;

    my $customer = $c->stash->{customer};
    my $args     = $c->request->params;

    my $scope_guard  = $c->model('DB')->schema->txn_scope_guard;

    $customer->add_to_meetings(
        {
            user_username => $c->user->username,
            state         => 1,
            date          => $args->{date},
            location      => $args->{location},
            reason        => $args->{reason},
            record        => $args->{record},

        }
    );

    $c->model('DB::Log')->create(
        {
            event         => 'add_meeting',
            user_username => $c->user->username,
            address       => $c->req->address,
            data          => $customer->id,
        }
    );

    $scope_guard->commit;

    $c->flash->{rc} = 'saved_meeting';
    $c->response->body('OK');
}

sub autocomplete :Chained('base') :PathPart('autocomplete') :Args(0) {
    my ( $self, $c ) = @_;

    my $search = '%' .$c->req->param('term') . '%';

    my $customers = $c->model('DB::Customer')->search(
        {
            id     => { '!=' => $c->req->param('customer_id' || undef )},
            active => 't',
            -or    => [
                name => { ilike => $search  },
                icp  => { like => $search  },
            ]
        },
        {
            order_by => 'name',
            rows     => 20,
        }
    );

    my @customers = ();

    while ( my $customer = $customers->next ) {
        push @customers, {
            value => $customer->id,
            label => $customer->icp . ' ' . $customer->name,
        }
    }

    $c->stash(
        output => 'json',
        json    => \@customers,
    );
}

sub suggested_for_sharing_options {
    my ( $self, $c, $customer ) = @_;

    my $suggesteds = $c->model('DB::Customer')->search(
        {
            active => 't',
            -and   => [
                id => { '!=' => $customer->id },
                $customer->share
                    ?  (id => { 'not in' => $customer->share } )
                    : ()
            ],
            -or    => [
                name => { ilike => $customer->name },
            ],
        },
    );

    my @options;

    while ( my $suggested = $suggesteds->next() ) {
        push @options, {
            value => $suggested->id,
            label => (join ' ', (
                $suggested->icp,
                $suggested->name,
                '(návrh)',
            )),
            attributes => {
                class => 'suggested'
            },
        };
    }

    return wantarray ? @options : \@options;

}

sub shared_options {
    my ( $self, $c, $customer ) = @_;

    return if ! $customer->share;

    my $shareds = $c->model('DB::Customer')->search(
        {
            id => { in => $customer->share },
        },
    );

    my @options;

    while ( my $shared = $shareds->next() ) {
        push @options, {
            value => $shared->id,
            label => (join ' ', (
                $shared->icp,
                $shared->name,
            )),
            attributes => {
                checked => 'checked'
            },
        };
    }

    return wantarray ? @options : \@options;

}

sub credits :Chained('customer') :PathPart('credits') :Args(0) {
    my ( $self, $c ) = @_;
    $c->assert_any_user_role( 'root', 'customers_manager', 'dealer', 'dealers_manager' );

    my $customer = $c->stash->{customer};

#TODO: jen prideleny obchodnik?

    $c->stash->{credits} = $c->model('DB::Credit_view')->summary_matrix(
        {
            customer_id => $customer->id,
        }
    );


}

sub credits_month :Chained('customer') :PathPart('credits') :Args(2) {
    my ( $self, $c, $year, $month ) = @_;
    $c->assert_any_user_role( 'root', 'customers_manager', 'dealer', 'dealers_manager' );

    $c->stash->{conditions} = {
        customer_id => $c->stash->{customer}->id,
    };

    $c->stash->{title} .= " za $month.$year";

    $c->controller('Credits')->month($c, $year, $month);


}

sub take :Chained('customer') :PathPart('take') :Args(0) {
    my ( $self, $c ) = @_;
    $c->assert_any_user_role( 'dealer' );

    my $customer    = $c->stash->{customer};
    my $scope_guard = $c->model('DB')->schema->txn_scope_guard;

    $customer->add_to_take_requests(
        {
            state                => 0,
            dealer_account_login => $c->user->username,
        }
    );

    $c->model('DB::Log')->create(
        {
            event         => 'customer_take_request',
            user_username => $c->user->username,
            address       => $c->req->address,
            data          => $customer->id,
        }
    );

    $scope_guard->commit;

    $c->response->body('OK');
#    $c->response->redirect($c->uri_for(BASE_URL));
#    $c->detach;
}

sub take_requests
    :Chained('base')
    :PathPart('take_requests')
    :Args(0)
{
    my ( $self, $c ) = @_;
    my $args = $c->request->params;

    if ( $c->request->method eq 'POST' ) {

        my $scope_guard = $c->model('DB')->schema->txn_scope_guard;

        my $requests = $c->model('DB::TakeCustomerRequest')->search(
            {
                id => { -in => $args->{id}},
            }
        );

        REQUEST:
        while ( my $request = $requests->next() ) {
            my $customer = $request->customer;

            if ( $args->{approve} ) {
                $customer->update(
                    {
                        dealer_account_login => $request->dealer_account_login
                    }
                );
            }
        }

        $requests->update(
            {
                state                 => $args->{approve} ? 1 : 2,
                decided               => \'now()',
                manager_account_login => $c->user->username,
            }
        );

        $scope_guard->commit;
    }

}

sub take_requests_json
    :Chained('base')
    :PathPart('take_requests_json')
    :Args(0)
{
    my ( $self, $c ) = @_;

    my $args = $c->request->params;

    my $conditions;
    my @where = ();

    # nucene omezujici podminky dle opravneni
    if ( ! $c->check_any_user_role('dealers_manager')) {
        $conditions->{dealer_account_login} = $c->user->username;
    }

    my $unfiltered_count = $c->model('DB::TakeCustomerRequest_view')->count(
        $conditions,
    );


#TODO: do samostatne tridy!!!
    my @columns = ();
    COLUMN:
    for my $n ( 0 .. $args->{iColumns} - 1 ) {
        my $column = $args->{'mDataProp_' . $n};

        push @columns, $column;

        my $search = $args->{'sSearch_' . $n};
        if ( defined $search && length( $search) ) {
            $conditions->{$column} //= {
                ilike => '%' . $search . '%',
            };
        }
    }

    my @sorting;
    SORT_COLUMN:
    for my $n ( 0 .. $args->{iSortingCols} - 1 ) {
        next SORT_COLUMN if not exists $args->{"iSortCol_$n"};
        push @sorting, {'-'. $args->{"sSortDir_$n"} => $columns[$args->{"iSortCol_$n"}]};
    }

    # TODO: samostatna funkce make filter conditions
    if ( $c->session->{filter_xml}{lab_id} && ! $c->check_user_roles('lab') ) {
        $conditions->{lab_id} = $c->session->{filter_xml}{lab_id};
    }

    # datum zpracovani
#    if ( $c->session->{filter_xml}{generated_begin} ) {
#        my $date = ParseDate($c->session->{filter_xml}{generated_begin});
#        if ($date) {
#            push @where, [
#                'me.generated' => { '>=' => UnixDate($date, '%Y-%m-%d')}
#            ];
#        }
#    }

#    if ( $c->session->{filter_xml}{generated_end} ) {
#        my $date = ParseDate($c->session->{filter_xml}{generated_end});
#        if ($date) {
#            push @where, [
#                'me.generated' => { '<=' => UnixDate($date, '%Y-%m-%d 23:59:59')}
#            ];
#        }
#    }

#    if ( $c->session->{filter_xml}{customer} && ! $c->check_user_roles('customer') ) {
#        my $search = '%' . $c->session->{filter_xml}{customer} . '%';
#        push @where, [
#            '-or' => [
#                customer_name => { ilike => $search },
#                customer_icz  => { like  => $search },
#                customer_icp  => { like  => $search },
#            ],
#        ];
#    }

    if ( ! $c->session->{filter_xml}{show_processed} ) {
        push @where, [
            'me.state' => 0,
        ]
    }

    $conditions->{-and} = \@where if scalar @where;

    my $filtered_count = $c->model('DB::TakeCustomerRequest_view')->count(
        $conditions
    );

    my $requests = $c->model('DB::TakeCustomerRequest_view')->search(
        $conditions,
        {
            order_by => \@sorting,
            rows     => $args->{iDisplayLength},
            offset   => $args->{iDisplayStart},

        }
    );

    my @requests = ();

    while (my $request = $requests->next) {
        push @requests, {
            $request->get_columns,
            status_name => '',
            created     => $request->date('created'),
        };
    }

    $c->stash(
        output => 'json',
        json    => {
            sEcho                => $args->{sEcho},
            aaData               => \@requests,
            iTotalRecords        => $unfiltered_count,
            iTotalDisplayRecords => $filtered_count,
        },
    );

}

sub set_filter :Chained('base') :PathPart('set_filter') :Args(0) {
    my ( $self, $c ) = @_;

    my $args = $c->request->params;

    $c->session->{ $self->FILTER_NAME() } = {};

    for my $field (
            keys %{ $self->FILTER_FIELDS() }
        ) {

        if ( exists $args->{$field} && length $args->{$field} ) {
            $c->session->{$self->FILTER_NAME}{$field} = $args->{$field};
        }
        else {
            delete $c->session->{$self->FILTER_NAME}{$field};
        }
    }

    $c->response->body('OK');
}



=head1 AUTHOR

Andrej Ramašeuski,,,,

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut


__PACKAGE__->meta->make_immutable;

1;

__END__


