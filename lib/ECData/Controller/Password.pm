package ECData::Controller::Password;
use Moose;
use Logger::Syslog;
use namespace::autoclean;

BEGIN {extends 'Catalyst::Controller::HTML::FormFu'; }

sub index :
Path
Args(0)
FormConfig('password.yaml')
{
    my ( $self, $c ) = @_;
}

sub index_FORM_VALID {
    my ( $self, $c ) = @_;

    my $args = $c->request->params;

    my $scope_guard = $c->model('DB')->schema->txn_scope_guard;

        my $user = $c->model('DB::User')->find(
            {
                username => $c->user->username,                
            }
        );

        $user //= $c->model('DB::Customer')->find(
            {
                login => $c->user->username,                
            }
        );

        if ( $user ) {
            $user->update( 
                {
                    password => $args->{new_password},
                }
            );            
            $c->model('DB::Log')->create(
                {
                    event         => 'change_password',
                    user_username => $c->user->username,
                    address       => $c->req->address,
                }
            );
        }


    $scope_guard->commit;

    $c->stash->{saved} = 1;
}

1;
