package ECData::Controller::Users;
use Moose;
use namespace::autoclean;
use utf8;

use constant REDIRECT_URL  => '/users/';

BEGIN {extends 'Catalyst::Controller::HTML::FormFu'; }

sub auto :Private {
    my ($self, $c) = @_;
    $c->assert_user_roles( 'root' );
}    

=head1 NAME

ECData::Controller::Users - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut

sub base :Chained('/') :PathPart('users') :CaptureArgs(0) { 
    my ($self, $c) = @_; 
    $c->assert_user_roles( 'root' );
} 

sub user :Chained('base') :PathPart('') :CaptureArgs(1) {
    my ($self, $c, $username) = @_;
    my $user = $c->model('DB::User')->find($username);
    $c->stash(
        user  => $user,
        title => $user->name || $user->username,
    );
    die "Account '$username' not found!" if ! $c->stash->{user};
    $c->stash->{login} = $user->username;
}

sub index :PathPart('') :Args(0)  {
    my ( $self, $c ) = @_;

    $c->assert_user_roles( 'root' );

    my $users = $c->model('DB::User')->search(
        {},
        {
            order_by => ['lastname', 'firstname']           
        }
    );

    my @users = ();

    while (my $user = $users->next) {
        push @users, $user;
    }

    $c->stash->{users} = \@users;

}

sub add :Chained('base') :PathPart('add') :Args(0) :FormConfig('user_add.yaml') {}

sub add_FORM_RENDER {
    my ( $self, $c ) = @_;

    my $form = $c->stash->{form};
    $form->auto_constraint_class( 'constraint_%t' );

    $form->remove_element( $form->get_field( {id => 'change_password'} ) );

    $form->get_field( {name => 'roles'} )
        ->options(
            $self->options_roles($c)
        );
}

sub add_FORM_VALID {
    my ( $self, $c ) = @_;
    my $form = $c->stash->{form};

    if ( $self->can('ADD_VALID') ) {
        $form->add_valid( %{ $self->ADD_VALID } );
    }

    my $save = sub {

        my $user = $form->model->create();

        $c->model('DB::Log')->create(
            {
                event         => 'user_create',
                user_username => $c->user->username,
                address       => $c->req->address,
                data          => $user->username,
            }
        );
    };

    eval {
        $c->model('DB')->schema->txn_do( $save );
    };
    $c->log->error($@) if $@;

    $c->response->redirect($c->uri_for($self->REDIRECT_URL));
    $c->detach;
}

sub edit :Chained('user') :PathPart('edit') :Args(0) :FormConfig('user_edit.yaml') {
    my ( $self, $c ) = @_;

    my $form = $c->stash->{form};
    $form->auto_constraint_class( 'constraint_%t' );

#    $form->remove_element( $form->get_field( {name => 'username'} ) );
    $form->get_field( {name => 'username'} )->attrs( { readonly=>1 } );
}

sub edit_FORM_RENDER {
    my ( $self, $c ) = @_;

    my $form = $c->stash->{form};
    my $user = $c->stash->{user}; 

    $form->model->default_values($user);

    $form->get_field( {name => 'roles'} )
        ->options(
            $self->options_roles($c)
        );
    $form->get_field( {name => 'password'} )->value('');
                
}

sub edit_FORM_VALID {
    my ( $self, $c ) = @_;
    my $form = $c->stash->{form};
    my $user = $c->stash->{user};

#    if ( $self->can('ADD_VALID') ) {
#        $form->add_valid( %{ $self->ADD_VALID } );
#    }

    if ( ! $form->param_value('password')) {
        $form->add_valid( password => $user->password );
    }

    my $save = sub {

        $form->model->update($user);

        $c->model('DB::Log')->create(
            {
                event         => 'user_edit',
                user_username => $c->user->username,
                address       => $c->req->address,
                data          => $user->username,
            }
        );
    };

    eval {
        $c->model('DB')->schema->txn_do( $save );
    };
    $c->log->error($@) if $@;

    $c->response->redirect($c->uri_for($self->REDIRECT_URL));
    $c->detach;
}    

sub delete :Chained('user') :PathPart('delete') :Args(0) {
    my ($self, $c) = @_;

    my $delete = sub {
        
        $c->model('DB::Log')->create(
            {
                event         => 'user_delete',
                user_username => $c->user->username,
                address       => $c->req->address,
                data          => $c->stash->{user}->username,
            }
        );

        $c->stash->{user}->delete();
    };

    eval {
        $c->model('DB')->schema->txn_do($delete);
    };
    $c->log->error($@) if $@;

    $c->response->redirect($c->uri_for($self->REDIRECT_URL));
    $c->detach;

}

sub lock :Chained('user') :PathPart('lock') :Args(0) {
    my ($self, $c) = @_;

    my $lock = sub {
        
        $c->model('DB::Log')->create(
            {
                event         => 'user_lock',
                user_username => $c->user->username,
                address       => $c->req->address,
                data          => $c->stash->{user}->username,
            }
        );

        $c->stash->{user}->update( { active => 'f' } );
    };

    eval {
        $c->model('DB')->schema->txn_do($lock);
    };
    $c->log->error($@) if $@;

    $c->response->redirect($c->uri_for($self->REDIRECT_URL));
    $c->detach;

}

sub unlock :Chained('user') :PathPart('unlock') :Args(0) {
    my ($self, $c) = @_;

    my $unlock = sub {
        
        $c->model('DB::Log')->create(
            {
                event         => 'user_unlock',
                user_username => $c->user->username,
                address       => $c->req->address,
                data          => $c->stash->{user}->username,
            }
        );

        $c->stash->{user}->update( { active => 't' } );
    };

    eval {
        $c->model('DB')->schema->txn_do($unlock);
    };
    $c->log->error($@) if $@;

    $c->response->redirect($c->uri_for($self->REDIRECT_URL));
    $c->detach;

}
sub options_roles {
    my ($self, $c) = @_;

    my $roles = $c->model('DB::Role')->search(
        {
            active => 't',
        },
        {
            order_by => {
                -asc => ['name'],
            },
        },
    );

    my ( @roles ) = ();
    while (my $role = $roles->next) {
        push @roles, { 
            value => $role->role, 
            label => $role->name, 
        };
    }  

    return \@roles;
}

sub options_by_role {
    my ($self, $c, $role) = @_;

    my @options = ();

    my $users = $c->model('DB::User')->search(
        {
            active                   => 'true',
            'user_roles.role_role' => $role,
        },
        {
            join => 'user_roles',
            order_by => {
                -asc => ['firstname', 'lastname'],
            },
        },
    );

    LAB:
    while (my $user = $users->next) {
        push @options, { 
            value => $user->username, 
            label => $user->name,
        };
    }  

    return \@options;
}


=head1 AUTHOR

Andrej Ramašeuski,,,,

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;

__END__

