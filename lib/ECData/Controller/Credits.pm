package ECData::Controller::Credits;
use Moose;
use namespace::autoclean;

use utf8;
use YAML;
use File::Temp qw{ tempdir };

BEGIN {extends 'Catalyst::Controller::HTML::FormFu'; }

=head1 NAME

ECData::Controller::Credits - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut

sub base
    :Chained('/')
    :PathPart('credits')
    :CaptureArgs(0)
{
    my ($self, $c) = @_;

    if ( $c->check_user_roles('dealer')) {
        $c->stash->{conditions} = {
            dealer_account_login => $c->user->username,
        };
    }
    elsif ( $c->check_user_roles('customer')) {
        $c->stash->{conditions} = {
            customer_id => $c->user->subject->id,
        };
    } elsif ( $c->check_user_roles('lab')) {
        $c->stash->{conditions} = {
            lab_id => $c->user->subject->id,
        };
    }

    if ( $c->check_user_roles('root') || $c->check_user_roles('customers_supervisor')) {
        $c->stash->{conditions} = {};
    }

}

=head2 index

=cut

sub index
    :Chained('base')
    :PathPart('')
    :Args(0)
{
    my ( $self, $c ) = @_;

    $c->stash->{credits} = $c->model('DB::Credit_view')->summary_matrix(
        $c->stash->{conditions}
    );

}

sub month
    :Chained('base')
    :PathPart('')
    :Args(2)
{
    my ( $self, $c, $year, $month ) = @_;
    $c->stash->{title} ||= "$month.$year";

    my $conditions = $c->stash->{conditions};
    $conditions->{year}  = $year;
    $conditions->{month} = $month;

    my $insurances = $c->model('DB::Credit_view')->search(
        $conditions,
        {
            select => [
                'insurance_code',
                { sum => 'credits' },
            ],
            as => [
                'insurance_code',
                'credits',
            ],
            group_by => 'insurance_code',
        }
    );

    my @insurances = ();
    while ( my $insurance = $insurances->next ) {

        # v klidu udelam to ve smycce - pojistoven je malo
        my $dasta = $c->model('DB::DastaList')->find(
            {
                id   => $insurance->insurance_code,
                list => 'ZDRPOJ',
            }
        );

        push @insurances, {
            $insurance->get_columns,
            $dasta ? (insurance_name => $dasta->content->{naz}) : ()
        };
    }

    $c->stash->{insurances} = [
        sort { $b->{credits} <=> $a->{credits} } @insurances
    ];

    my $specializations = $c->model('DB::Credit_view')->search(
        $conditions,
        {
            select => [
                'specialization_code',
                { sum => 'credits' },
            ],
            as => [
                'specialization_code',
                'credits',
            ],
            group_by => 'specialization_code',
        }
    );

    my @specializations = ();
    while ( my $specialization = $specializations->next ) {

        my $code = $specialization->specialization_code;
        if ( $code =~ /^\d{1,2}$/ ) {
            $code = sprintf '%03d', $code;
        }

        # v klidu udelam to ve smycce - pojistoven je malo
        my $dasta = $c->model('DB::DastaList')->find(
            {
                id   => $code,
                list => 'VZP_ODB',
            }
        );

        push @specializations, {
            $specialization->get_columns,
            $dasta ? (specialization_name => $dasta->content->{name}) : ()
        };
    }

    $c->stash->{specializations} = [
        sort { $b->{credits} <=> $a->{credits} } @specializations
    ];

    my $lab_is_primary = $c->model('DB::Credit_view')->search(
        $conditions,
        {
            select => [
                'lab_is_primary',
                { sum => 'credits' },
            ],
            as => [
                'lab_is_primary',
                'credits',
            ],
            group_by => 'lab_is_primary',
        }
    );

    my @lab_is_primary = ();
    while ( my $is = $lab_is_primary->next ) {

        push @lab_is_primary, {
            $is->get_columns,
        };
    }

    $c->stash->{lab_is_primary} = [
        sort { $b->{credits} <=> $a->{credits} } @lab_is_primary
    ];

    my $customers = $c->model('DB::Credit_view')->search(
        $conditions,
        {
            select => [
                'customer_id',
                'customer_icp',
                'customer_name',
                { sum => 'credits' },
            ],
            as => [
                'id',
                'icp',
                'name',
                'credits',
            ],
            group_by => ['customer_id', 'customer_icp','customer_name'],
        }
    );

    my @customers = ();
    while ( my $customer = $customers->next ) {

        push @customers, {
            $customer->get_columns,
        };
    }

    $c->stash->{customers} = \@customers;
}

sub upload
:Chained('base')
:PathPart('upload')
:Args(0)
:FormConfig('upload_credits.yaml')
{
}

sub upload_FORM_VALID {
    my ( $self, $c ) = @_;

    my $args = $c->request->params;
#    my $file = $c->req->upload('credits');

    my @files;

    for my $file ( $c->req->upload('credits') ) {
        if ( $file->type =~ /zip/ ) {
            my $dir = tempdir( CLEANUP => 1 );

            my $zip = $file->tempname;
            `unzip $zip -d $dir`;

            opendir DIR, $dir;

            while ( my $kdavka = readdir DIR ) {
                next if $kdavka =~ /^\./;
                push @files, {
                    file => "$dir/$kdavka",
                    name => $kdavka,
                }
            }
            closedir DIR;

        }
        else {
            push @files, {
                file => $file->tempname,
                name => $file->filename,
            };
        }
    }

    my $scope_guard  = $c->model('DB')->schema->txn_scope_guard;

    FILE:
    foreach my $file ( @files ) {

        my $kdavka = $c->model('KDAVKA')->load_file($file->{file});

        my $report = {
            file    => $file->{name},
            count   => $kdavka->{count},
            credits => 0,
        };

        CREDITS:
        foreach my $credits ( @{ $kdavka->{items}} ) {

            my $lab = $c->model('DB::Lab')->search(
                {
                    deleted => undef,
                    -or => [
                        icz => $credits->{lab_icz},
                        icp => $credits->{lab_icz},
                    ]
                }
            )->first;

            my $customer = $c->model('DB::Customer')->search(
                {
                    deleted => undef,
                    -or => [
                        icz => $credits->{customer_icz},
                        icp => $credits->{customer_icz},
                    ]
                }
            )->first;

            if ( ! $lab ) {
                $lab = $c->model('DB::Lab')->create({
                    role => 'lab',
                    icz  => $credits->{lab_icz},
                });
#                $c->log->error('UNKNOWN LAB ' . $credits->{lab_icz});
#                $report->{count}{unknown_lab}++;
#                next CREDITS;
            }
            if ( ! $customer ) {
                $customer = $c->model('DB::Customer')->create({
                    role => 'customer',
                    icz  => $credits->{customer_icz},
                });
#                $c->log->error('UNKNOWN CUSTOMER ' . $credits->{customer_icz});
#                $report->{count}{unknown_customer}++;
#                next CREDITS;
            }

            my $source = $c->model('DB::CreditSource')->find(
                {
                    source => $credits->{source},
                },
            );

            if ( $source ) {
                $report->{count}{duplicity}++;
                next CREDITS;
            }

            eval {

                $c->model('DB::CreditSource')->create(
                    {
                        source => $credits->{source},
                        date   => $credits->{date},
                    },
                );

                $c->model('DB::Credit')->create(
                    {
                        lab_id              => $lab->id,
                        customer_id         => $customer->id,
                        specialization_code => $credits->{specialization_code},
                        insurance_code      => $credits->{insurance_code},
                        date                => $args->{date},
                        credits             => $credits->{credits},
                    },
                    { key => 'secondary_key'},
                );

            };

            if ( $@ ) {
                $c->log->error('ERROR: ' . $@);
                $report->{count}{error}++;
            }

            $report->{count}{processed}++;
            $report->{credits} += $credits->{credits};

        }

        push @{ $c->stash->{reports} }, $report;

    }

    $scope_guard->commit;

#    $c->stash->{template} = 'credits/upload_report.tt2';
#    $c->response->redirect($c->uri_for('/credits/'));
#    $c->detach;

}


=head1 AUTHOR

Andrej Ramašeuski

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;
