package ECData::Controller::Labs;
use Moose;
use namespace::autoclean;
use utf8;

BEGIN {extends 'Catalyst::Controller::HTML::FormFu'; }

=head1 NAME

ECData::Controller::Labs - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut

sub base :Chained('/') :PathPart('labs') :CaptureArgs(0) { 
    my ($self, $c) = @_; 
} 

sub lab :Chained('base') :PathPart('') :CaptureArgs(1) {
    my ($self, $c, $id) = @_;
    my $lab = $c->model('DB::Lab')->find($id);
    $c->stash(
        lab   => $lab,
        title => $lab->name,
        login => $lab->login,
    );
    die "Laboratory '$id' not found!" if ! $c->stash->{lab};
}

=head2 index

=cut

sub index :PathPart('') :Args(0)  {
    my ( $self, $c ) = @_;

    my $labs = $c->model('DB::Lab')->search(
        {
            deleted => undef,
        },
        {
            order_by => ['name'], 
        }
    );

    my @labs = ();

    while (my $lab = $labs->next) {
        push @labs, $lab;
    }

    $c->stash->{labs} = \@labs;

}

sub edit :Chained('lab') :PathPart('edit') :Args(0) :FormConfig('lab.yaml') {
    my ( $self, $c ) = @_;

    my $form = $c->stash->{form};
    
}

sub edit_FORM_RENDER {
    my ( $self, $c ) = @_;

    my $form = $c->stash->{form};
    my $lab = $c->stash->{lab}; 

    $form->model->default_values($lab);
    $form->auto_constraint_class( 'constraint_%t' );

    $form->get_field( {name => 'password'} )->value('');

    $c->stash->{title} = $lab->name;

}

sub edit_FORM_VALID {
    my ( $self, $c ) = @_;
    my $form = $c->stash->{form};
    my $lab = $c->stash->{lab};

    my $save = sub {

        if ( ! $form->param_value('password')) {
            $form->add_valid( password => $lab->password );
        }
        $form->model->update($lab);

        $c->model('DB::Log')->create(
            {
                event         => 'lab_edit',
                user_username => $c->user->username,
                address       => $c->req->address,
                data          => $lab->id,
            }
        );
    };

    eval {
        $c->model('DB')->schema->txn_do( $save );
    };
    $c->log->error($@) if $@;

    $c->response->redirect($c->uri_for('/labs/'));
    $c->detach;
}    

sub options {
    my ($self, $c) = @_;

    my @options = ();

    my $labs = $c->model('DB::Lab')->search(
        {
            active => 'true',
        },
        {
            order_by => {
                -asc => ['name'],
            },
        },
    );

    LAB:
    while (my $lab = $labs->next) {
        push @options, { 
            value => $lab->id, 
            label => $lab->name,
        };
    }  

    return \@options;
}

sub credits :Chained('lab') :PathPart('credits') :Args(0) {
    my ( $self, $c ) = @_;

    $c->stash->{credits} = $c->model('DB::Credit_view')->summary_matrix(
        {
            lab_id => $c->stash->{lab}->id,
        }
    );
}

sub credits_month :Chained('lab') :PathPart('credits') :Args(2) {
    my ( $self, $c, $year, $month ) = @_;
    $c->assert_any_user_role( 'root', 'dealers_manager' );

    $c->stash->{conditions} = {
        lab_id => $c->stash->{lab}->id,
    };            

    $c->stash->{title} .= " za $month.$year";

    $c->controller('Credits')->month($c, $year, $month);

}
=head1 AUTHOR

Andrej Ramašeuski

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;
