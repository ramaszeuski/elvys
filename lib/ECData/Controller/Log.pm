package ECData::Controller::Log;
use Moose;
use namespace::autoclean;

use Date::Manip;

Date_Init('DateFormat=non-US');

BEGIN {extends 'Catalyst::Controller::HTML::FormFu'; }

=head1 NAME

ECData::Controller::Log - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut

sub base :Chained('/') :PathPart('log') :CaptureArgs(0) { 
    my ($self, $c) = @_; 
} 


=head2 index

=cut

sub index :PathPart('') :Args(0)  :FormConfig('filter_log.yaml') {
    my ( $self, $c ) = @_;
}

sub index_FORM_RENDER {
    my ( $self, $c ) = @_;

    my $form = $c->stash->{form} // return;

    $form->get_field( {name => 'event'} )
        ->options(
            $c->model('DB::Log_view')->events()
        )
        ->value($c->session->{filter_log}{event})
    ;

    FIELD:
    foreach my $field (qw(
        begin 
        end
        user
        address
    )) {
        $form->get_field( {name => $field} )
            ->default($c->session->{filter_log}{$field})
        ;
    }

}
sub set_filter :Chained('base') :PathPart('set_filter') :Args(0) {
    my ( $self, $c ) = @_;

    my $args = $c->request->params;

    $c->session->{filter_log} = {};                   

    FIELD:
    foreach my $field (qw(
        event
        begin 
        end
        user
        address
    )) {
        if ( $args->{$field}) {
            $c->session->{filter_log}{$field} = $args->{$field};        
        }
        else {
            delete $c->session->{filter_log}{$field};                   
        }
    }

    $c->response->body('OK');
}

sub json :Chained('base') :PathPart('json') :Args(0) {
    my ( $self, $c ) = @_;

    my $args  = $c->request->params;
    my $model = $c->model('DB::Log_view');

    my $conditions;
    my @where = ();

    my $unfiltered_count = $model->count(
        $conditions,
    );


#TODO: do samostatne tridy!!!
    my @columns = ();   
    COLUMN:
    for my $n ( 0 .. $args->{iColumns} - 1 ) {
        my $column = $args->{'mDataProp_' . $n};

        push @columns, $column;        

        my $search = $args->{'sSearch_' . $n};
        if ( defined $search && length( $search) ) {
            $conditions->{$column} //= {
                ilike => '%' . $search . '%',
            };            
        }
    }

    my @sorting;
    SORT_COLUMN:
    for my $n ( 0 .. $args->{iSortingCols} - 1 ) {
        next SORT_COLUMN if not exists $args->{"iSortCol_$n"};
        push @sorting, {'-'. $args->{"sSortDir_$n"} => $columns[$args->{"iSortCol_$n"}]};
    }       

    # TODO: samostatna funkce make filter conditions
    if ( $c->session->{filter_log}{event}) {
        $conditions->{event} = $c->session->{filter_log}{event};        
    }
    
    # datum zpracovani    
    if ( $c->session->{filter_log}{begin} ) {
        my $date = ParseDate($c->session->{filter_log}{begin});
        if ($date) {
            push @where, [ 
                'me.timestamp' => { '>=' => UnixDate($date, '%Y-%m-%d 00:00:00')} 
            ];
        }
    }

    if ( $c->session->{filter_log}{end} ) {
        my $date = ParseDate($c->session->{filter_log}{end});
        if ($date) {
            push @where, [ 
                'me.timestamp' => { '<=' => UnixDate($date, '%Y-%m-%d 23:59:59')} 
            ];
        }
    }

    if ( $c->session->{filter_log}{address} ) {
        my $search = '%' . $c->session->{filter_log}{address} . '%';
        push @where, [
                address => { ilike => $search },
        ];
    }        

    if ( $c->session->{filter_log}{user} ) {
        my $search = '%' . $c->session->{filter_log}{user} . '%';
        push @where, [
            '-or' => [ 
                user_username   => { ilike => $search },
                user_name       => { ilike => $search },
                user_firstname  => { ilike => $search },
                user_lastname   => { ilike => $search },
            ],
        ];
    }        

    $conditions->{-and} = \@where if scalar @where;

    my $filtered_count = $model->count(
        $conditions
    );

    my $records = $model->search(
        $conditions,
        {
            order_by => \@sorting,
            rows     => $args->{iDisplayLength},
            offset   => $args->{iDisplayStart},
             
        }
    );

    my @records = ();

    while (my $record = $records->next) {
        push @records, {
            event         => $record->event_name,
            user_username => $record->user_username,
            user_fullname => $record->user_fullname,
            address       => $record->address,
            timestamp     => $record->format_timestamp(),
            data          => $record->data,
        }, 
    }

    $c->stash(
        output => 'json',
        json    => {
            sEcho                => $args->{sEcho},
            aaData               => \@records,
            iTotalRecords        => $unfiltered_count,
            iTotalDisplayRecords => $filtered_count,
        },
    );

}

=head1 AUTHOR

Andrej Ramašeuski

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;
