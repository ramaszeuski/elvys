package ECData::Controller::Root;
use Moose;
use namespace::autoclean;
use YAML;

BEGIN {extends 'Catalyst::Controller::HTML::FormFu'; }

#
# Sets the actions in this controller to be registered with no prefix
# so they function identically to actions created in MyApp.pm
#
__PACKAGE__->config(namespace => '');

=head1 NAME

ECData::Controller::Root - Root Controller for ECData

=head1 DESCRIPTION

[enter your description here]

=head1 METHODS

=head2 index

The root page (/)

=cut

sub index :Path :Args(0) {
    my ( $self, $c ) = @_;
}

=head2 default

dhandler

=cut

sub default :Path {
    my ( $self, $c ) = @_;

    $c->response->body( 'Page not found' );
    $c->response->status(404);
    return;

# Jednoduchy dhandler
    my $template = $c->request->_path;
    $template  =~ s{/$}{\.html};

    my $file = join '/', (
        $c->config->{home},
        $c->config->{'View::HTML'}{INCLUDE_PATH}[0],
        $template,
    );

    if ( -f $file ) {
        $c->stash->{template} = $template;
    }
    else {
        $c->response->body( 'Page not found ' .  $file );
        $c->response->status(404);
    }
}

=head2 end

Attempt to render a view, if needed.

=cut

sub end : Private {
    my ($self, $c) = @_;

    if ( scalar @{ $c->error } ) {
        $c->stash->{errors}   = $c->error;
        for my $error ( @{ $c->error } ) {
            $c->log->error($error);
        }
        $c->clear_errors;
    }

# dopleni nekterych promennych v zavislosti na acl TODO: mapu do constants

    if ( $c->check_any_user_role( split /\W+/, $c->config->{roles_for_results} )) {
        $c->stash->{allowed_results} = 1;
    }

    if ( $c->check_any_user_role( split /\W+/, $c->config->{roles_for_results_list} )) {
        $c->stash->{allowed_results_list} = 1;
    }

    if ( $c->check_any_user_role(qw( customers_supervisor dealers_manager dealer root ))) {
        $c->stash->{allowed_customers} = 1;
    }

    if ( $c->check_any_user_role(qw( customers_manager dealers_manager dealer root ))) {
        $c->stash->{allowed_customers} = 1;
        $c->stash->{allowed_credits} = 1;
    }

    if ( $c->check_any_user_role(qw( customer ))) {
        if ( $c->user->customer ) {
            $c->stash->{allowed_credits} = 1;
        }
    }

    if ( $c->request->param('no_wrapper') ) {
        $c->stash->{no_wrapper} = 1;
    }

    if (
            ($c->req->param('output') && $c->req->param('output') eq 'json')
         || ($c->stash->{output} && $c->stash->{output} eq 'json')
    ) {
        $c->forward('View::JSON');
    }
    elsif ( (! $c->res->body) && ($c->res->status !~ /^3/) ) {
        $c->stash->{menu} = $c->model('Menu')->get_menu;
        $c->forward('View::HTML');
    }

}


# Note that 'auto' runs after 'begin' but before your actions and that
# 'auto's "chain" (all from application path to most specific class are run)
# See the 'Actions' section of 'Catalyst::Manual::Intro' for more info.
sub auto :Private {
    my ($self, $c) = @_;

    # Allow unauthenticated users to reach the login page.  This
    # allows unauthenticated users to reach any action in the Login
    # controller.  To lock it down to a single action, we could use:
    #   if ($c->action eq $c->controller('Login')->action_for('index'))
    # to only allow unauthenticated access to the 'index' action we
    # added above.
    if ($c->controller eq $c->controller('Login')) {
        return 1;
    }

    if ($c->controller eq $c->controller('Logout')) {
        return 1;
    }

    if ($c->controller eq $c->controller('Results') && $c->session->{icp} ) {
        return 1;
    }

    if (! $c->user_exists) {
        $c->log->debug('User not found, forwarding to /login/');
        $c->response->redirect($c->uri_for('/login/'));
        return 0;
    }

    if ( $c->user->has_weak_password && $c->controller ne $c->controller('Password')) {
        $c->stash->{weak_password} = 1;
    }

    return 1;
}

sub test :Local {
    my ($self, $c) = @_;
    $c->stash->{pdf_template} = 'test_pdf.tt2';
    $c->forward('View::PDF');
}



=head1 AUTHOR

Andrej Ramaszeuski,,,

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;
