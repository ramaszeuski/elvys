package ECData::Controller::Meetings;

use Moose;
use namespace::autoclean;
use Date::Manip;
use YAML;

BEGIN {extends 'Catalyst::Controller::HTML::FormFu'; }

use constant BASE_URL => '/meetings/';

use constant LIST_MODEL => 'DB::Meeting_view';

use constant FILTER_NAME   => 'filter_meeting';

use constant FILTER_FIELDS => {
    user_username  => {},
    customer_name => {
        type    => 'ilike',  
    },
    content       => {
        type    => 'ilike',  
        columns => [ qw(location reason program record ) ],
    },
    date_begin     => {
        type    => 'timestamp_begin',
        column  => 'date',
#        default => UnixDate('today', '%d.%m.%Y'),
    },
    date_end       => {
        type    => 'timestamp_end',
        column  => 'date',
#        default => UnixDate('tomorrow', '%d.%m.%Y'),
    },
    
};
=head1 NAME

ECData::Controller::Meeting - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut

sub base :Chained('/') :PathPart('meetings') :CaptureArgs(0) { 
    my ($self, $c) = @_; 
} 

sub meeting :Chained('base') :PathPart('') :CaptureArgs(1) {
    my ($self, $c, $id) = @_;

    my $meeting = $c->model('DB::Meeting')->find($id);

    if ( ! $c->check_any_user_role('root', 'dealer', 'dealer_manager', 'customers_supervisor')) {
        die 'Access denied';        
    }            

    $c->stash->{meeting} = $meeting;
}

sub index :PathPart('') :Args(0) :FormConfig('filter_meeting.yaml') {}

sub index_FORM_RENDER {
    my ( $self, $c ) = @_;

    my $form = $c->stash->{form} // return;

    FIELD:
    foreach my $field_name ( keys %{ $self->FILTER_FIELDS() } ) {

        my $field = FILTER_FIELDS->{$field_name};

        if ( ! exists $c->session->{$self->FILTER_NAME}{$field_name} ) {
            if ( exists $field->{default} ) {
                $c->session->{$self->FILTER_NAME}{$field_name}
                    = $field->{default};
            }
            else {
                next FIELD;
            }
        }

        my $form_field = $form->get_field( { name => $field_name } ) || next FIELD;

        if ( $form_field->can('default')) {
            $form_field->default($c->session->{$self->FILTER_NAME}{$field_name});
        }
        if ( $form_field->can('value') ) {
            $form_field->value($c->session->{$self->FILTER_NAME}{$field_name});
        }
    }
}

sub json :Chained('base') :PathPart('json') :Args(0) {
    my ( $self, $c ) = @_;

    my $args  = $c->request->params;
    my $model = $c->model( $self->LIST_MODEL() );

    my ( $columns, $sorting, $conditions )
        = $c->model('DB')->datatables_args( $args );

    if ( ! $c->check_any_user_role('root', 'dealer_manager', 'customers_supervisor')) {
        $conditions->{user_username} = $c->user->username;
    }            

    my $unfiltered_count = $model->count(
        $conditions,
    );

    my $filter = $c->session->{$self->FILTER_NAME};


    my @where = $c->model('DB')->make_conditions_from_filter(
        {
            model         => $model,
            filter        => $filter,
            filter_fields => $self->FILTER_FIELDS(),
        }
    );

    $conditions->{-and} = \@where if scalar @where;
    
    my $filtered_count = $model->count( $conditions, );

    my $meetings = $model->search(
        $conditions,
        {
            order_by => $sorting,
            rows     => $args->{iDisplayLength},
            offset   => $args->{iDisplayStart},
#            prefetch => ['dealer', 'customer']

        }
    );

    my @meetings = ();

    while (my $meeting = $meetings->next) {
        push @meetings, {
            DT_RowId      => $meeting->id,
            date          => $meeting->date,
            customer_name => $meeting->customer_name,
            dealer_name   => $meeting->dealer_name,
            location      => $meeting->location,
            reason        => $meeting->reason,
        }, 
    }

    $c->stash(
        output  => 'json',
        json    => {
            sEcho                => $args->{sEcho},
            aaData               => \@meetings,
            iTotalRecords        => $unfiltered_count,
            iTotalDisplayRecords => $filtered_count,
        },
    );

}
sub add :Chained('base') :PathPart('add') :Args(0) :FormConfig('meeting.yaml') {
    my ( $self, $c ) = @_;

    my $form = $c->stash->{form};

    if ( ! $form->submitted ) {
        $form->get_field( {name => 'date'} )
            ->value( UnixDate('now', '%d.%m.%Y') );
    }            

#    $form->insert_before(        
#        $form->element({
#            type  => 'Hidden',
#            value => $c->session->{customer_id}, #TODO: uglu hack
#        }),
#        $form->get_field( {name => 'date'} )
#    );        

    $c->stash->{customer_id} = $c->req->param('customer_id');

    $form->action('/meetings/add/');

    $c->stash->{no_wrapper}  = 1;
}

sub add_FORM_RENDER {
    my ( $self, $c ) = @_;

    my $form = $c->stash->{form};


}

sub add_FORM_VALID {
    my ( $self, $c ) = @_;

    my $form = $c->stash->{form};

    $form->add_valid('user_username', $c->user->username);
    $form->add_valid('customer_id',  $c->session->{customer_id});
    $form->add_valid('state',        1);

    my $scope_guard  = $c->model('DB')->schema->txn_scope_guard;

    my $meeting = $form->model->create();

    $c->model('DB::Log')->create(
        {
            event         => 'meeting_create',
            user_username => $c->user->username,
            address       => $c->req->address,
            data          => $meeting->id,
        }
    );

    $scope_guard->commit;

    $c->flash->{rc} = 'saved_meeting';

    $c->response->body('OK');
#    $c->response->redirect('/customers/' . $meeting->customer_id . '/');
#    $c->detach;
}

sub edit :Chained('meeting') :PathPart('edit') :Args(0) :FormConfig('meeting.yaml') {
    my ( $self, $c ) = @_;
    $c->stash->{no_wrapper}  = 1;
}

sub edit_FORM_RENDER {
    my ( $self, $c ) = @_;

    my $form    = $c->stash->{form};
    my $meeting = $c->stash->{meeting}; 

    $form->action('/meetings/' . $meeting->id . '/edit/');
    $form->model->default_values($meeting);
}

sub edit_FORM_VALID {
    my ( $self, $c ) = @_;

    my $form    = $c->stash->{form};
    my $meeting = $c->stash->{meeting};

    my $scope_guard  = $c->model('DB')->schema->txn_scope_guard;

    $form->model->update($meeting);

    $c->model('DB::Log')->create(
        {
            event         => 'meeting_edit',
            user_username => $c->user->username,
            address       => $c->req->address,
            data          => $meeting->id,
        }
    );
    $scope_guard->commit;

    $c->flash->{rc} = 'saved_meeting';

    $c->response->body('OK');

#    $c->response->redirect('/customers/' . $meeting->customer_id . '/');
#    $c->detach;
}    

sub record :Chained('meeting') :PathPart('record') :Args(0) {
    my ( $self, $c ) = @_;
    $c->stash->{no_wrapper} = 1;
}        

sub set_filter :Chained('base') :PathPart('set_filter') :Args(0) {
    my ( $self, $c ) = @_;

    my $args = $c->request->params;

    $c->session->{ $self->FILTER_NAME() } = {};

    for my $field (
            keys %{ $self->FILTER_FIELDS() }
        ) {

        if ( exists $args->{$field} && length $args->{$field} ) {
            $c->session->{$self->FILTER_NAME}{$field} = $args->{$field};
        }
        else {
            delete $c->session->{$self->FILTER_NAME}{$field};
        }
    }

    $c->response->body('OK');
}

=head1 AUTHOR

Andrej Ramašeuski

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;
__END__
sub json :Chained('base') :PathPart('json') :Args(0) {
    my ( $self, $c ) = @_;

    my $args = $c->request->params;

    my $conditions = {
        customer_id => $args->{customer_id},
    };

    my $unfiltered_count = $c->model('DB::Meeting')->count(
        $conditions,
    );

    $args->{conditions} = $conditions;
    my $rc = $c->model('DB')->conditions_and_sorting_from_datatables( $args );

    $conditions = $rc->{conditions};

    my $filtered_count = $c->model('DB::Meeting')->count(
        $conditions
    );

    my $meetings = $c->model('DB::Meeting')->search(
        $conditions,
        {
            order_by => $rc->{sorting},
            rows     => $args->{iDisplayLength},
            offset   => $args->{iDisplayStart},
             
        }
    );

    my @meetings = ();

    while (my $meeting = $meetings->next) {
        push @meetings, {
            id       => $meeting->id,
            date     => $meeting->date,
            location => $meeting->location,
            reason   => $meeting->reason,
        }, 
    }

    $c->stash(
        output  => 'json',
        json    => {
            sEcho                => $args->{sEcho},
            aaData               => \@meetings,
            iTotalRecords        => $unfiltered_count,
            iTotalDisplayRecords => $filtered_count,
        },
    );

}

