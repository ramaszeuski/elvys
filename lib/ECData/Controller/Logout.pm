package ECData::Controller::Logout;
use Moose;
use namespace::autoclean;

BEGIN {extends 'Catalyst::Controller'; }

=head1 NAME

ECData::Controller::Logout - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut


=head2 index

=cut

sub index :Path :Args(0) {
    my ( $self, $c ) = @_;
    $c->model('DB::Log')->create(
        {
            event         => 'user_logout',
            user_username => $c->user ? $c->user->username : "PORTAL",
            address       => $c->req->address,
        }
    );
    $c->delete_session('session expired');
    $c->logout;
    $c->response->redirect('/');
}


=head1 AUTHOR

Andrej Ramašeuski

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;
