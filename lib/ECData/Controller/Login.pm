package ECData::Controller::Login;
use Moose;
use Logger::Syslog;
use namespace::autoclean;

use MIME::Base64;
use Crypt::OpenSSL::RSA;
use Crypt::OpenSSL::X509;
use YAML;

BEGIN {extends 'Catalyst::Controller::HTML::FormFu'; }

use constant REDIRECT_MAP => [
    { role => 'root',                 url => '/log/' },        
    { role => 'dealers_manager',      url => '/dealers/' },        
    { role => 'dealer',               url => '/customers/' },        
    { role => 'customers_supervisor', url => '/customers/' },        

    { role => 'lab',                  url => '/results/' },        
    { role => 'customer',             url => '/results/' },        
];

=head1 NAME

ECData::Controller::Login - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut


=head2 index

=cut

sub index :Path :Args(0) :FormConfig('login.yaml') {}

sub index_FORM_VALID {
    my ( $self, $c ) = @_;

    my $form = $c->stash->{form};

    my $username = $c->request->params->{username};
    my $password = $c->request->params->{password};

    if ($c->authenticate(
        {
            username => $username,
            password => $password
        }
    )) {
        $c->model('DB::Log')->create(
            {
                event         => 'user_login',
                user_username => $username,
                address       => $c->req->address,
            }
        );

        if ( $c->user->customer ) {        
            $c->user->customer->update({last_login => \'now()'});
        }            

        if ( $c->user->subject && $c->user->subject->tfa ) {
            my $tfa    = $c->user->subject->tfa;
            my $code   = sprintf('%06d', rand(1000000));
            my $sended = 0;

            if ( $tfa == 1 ) {
                # poslat mail
                $sended = 1;                
            }         

            if ( $sended ) {
                $c->user->update_or_create_related(
                    'two_factor_code',
                    { code => $code } 
                );
            }

        }

        my $redirect;

        ROLES:
        foreach my $role ( @{ REDIRECT_MAP() } ) {
            if ($c->check_user_roles( $role->{role} )) {
                $redirect = $role->{url};
                last ROLES;
            }
        }  
        
        $c->response->redirect($c->uri_for($redirect));

        return;
    } else {
        error('BAD PASSWORD FROM ' . $c->req->address);
        $form->get_field('password')
            ->get_constraint({ type => 'Callback' })
            ->force_errors(1);
        $form->process;
    }

}

sub token :Local :Args(0) {
    my ( $self, $c ) = @_;
    my $token = $c->request->param('token');

    my $x509 = Crypt::OpenSSL::X509->new_from_file($c->config->{portal_crt});
    my $pubkey = Crypt::OpenSSL::RSA->new_public_key($x509->pubkey());
    $pubkey->use_sha512_hash();

    my $decoded = decode_base64($token);

    my ($data, $sign) = split /^/m, $decoded;

    $data = decode_base64($data);
    $sign = decode_base64($sign);

    my $valid = $pubkey->verify($data, $sign);

    if ( ! $valid ) {
        $c->stash->{error} = 'Invalid token';
        return;    
    }

    $data =~ s/^.+?---//s;

    $data = Load($data);


    if ( $data->{medicalWorkplace} !~ /\d+/ ) {
        $c->stash->{error} = 'Missing ICP';
        return;
    }

    $c->session->{icp} = $data->{medicalWorkplace};
    $c->response->redirect($c->uri_for('/results/'));

}

=head1 AUTHOR

Andrej Ramašeuski

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;
