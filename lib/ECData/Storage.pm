package ECData::Storage;

use strict;
use warnings;
use File::Temp qw/tempdir/;
use File::Path qw(make_path);
use File::Copy;
use File::Slurp;
use Digest::SHA;
use Date::Manip;
use JSON;
use XML::Simple;
use Encode;

our $VERSION = '0.01';

sub new {
    my $classname = shift;
    my $workdir   = shift || die "WorkDir directory not defined";
    my $self      = {}; 

    my @directories = (
        'storage',
	    'inbox',
	    'outbox',
	    'dropped',
    );

    DIRECTORY:
    foreach my $dirname ( @directories ) {
        my $path = "$workdir/$dirname";
        $self->{$dirname} = $path;
        -d $path || make_path( $path ) || die "Can't create directory $path\n";
    }

    bless ($self, $classname);
    return $self;
}

sub store {
    my $self = shift;
    my $file = shift;
    my $id   = shift || Digest::SHA->new->addfile( $file, 'b')->hexdigest();
    my $fullname  = $self->fullname( $id );

    move( $file, $fullname );
}

sub drop {
    my $self     = shift;
    my $file     = shift;
    my $error    = shift;
    my $id       = Digest::SHA->new->addfile( $file, 'b')->hexdigest();
    my $dropname = $self->dropname( $id );

    move( $file, $dropname );

    # ulozime metadata
    open my $META, ">$dropname.json";
    print $META to_json (
        {
            filename => $file,
            error    => $error,
        },
    );
    close $META;
}

sub get_xml {
    my $self = shift;	
    my $id   = shift;

    my $xml = XMLin (
        $self->fullname($id),
        ForceArray   => ['ip', 'vr', 'dgz'], 
        ForceContent => 1, 
    ); 

    return $xml;
}

sub fullname {
    my $self = shift;	
    my $id   = shift;

    # nginx cache like
    $id =~ /(..)(.)$/;		
    my $dir = $self->{storage} . "/$2/$1";

    if ( ! -d $dir ) {
    	make_path($dir);
    }
    return "$dir/$id.xml";	 
}

sub dropname {
    my $self = shift;	
    my $id   = shift;
    my $day  = shift || 'today';

    my $dir = $self->{dropped} . UnixDate($day, '/%Y%m%d/');

    if ( ! -d $dir ) {
    	make_path($dir);
    }
    return "$dir/$id";	 
}

sub zip {
    my $self = shift;	
    my $ids  = shift;

    my $tempdir = tempdir ( DIR => $self->{outbox}, UNLINK => 0 );

    ID:
    foreach my $id ( @{$ids} ) {
        my $src = $self->fullname($id);
        next ID if ! -f $src;
        copy ($src, $tempdir);
    }

    my $filename = UnixDate('now', '%Y%m%d%H%M%S') . '.zip';
    `zip -j $tempdir/$filename $tempdir/*.xml`;

    return {
        filename => $filename,
        tempdir  => $tempdir,
    };
}

sub get_as_string {
    my $self = shift;	
    my $id   = shift;

    my $content = read_file( $self->fullname($id) );

    return decode('windows-1250', $content);
}

__END__
