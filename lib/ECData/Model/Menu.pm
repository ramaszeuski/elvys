package ECData::Model::Menu;

use strict;
use warnings;

use Moose;
extends 'Catalyst::Model::MenuGrinder';

__PACKAGE__->config(
    menu_config => {
        filename => 'root/menu.xml',
        plugins  => {
            loader  => 'XMLLoader',
            on_load => [
                qw(
                    DefaultTarget
                    Hotkey
                ),
            ],
            per_request => [
                qw(
                    RequirePrivilege
                    FileReloader
                    ActivePath
                    Variables
                ),
            ],
        },
    },
);

=head1 NAME

ECData::Model::Menu - MenuGrinder Model

=head1 SYNOPSIS

See L<ECData>

=head1 AUTHOR

Andrej Ramašeuski

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;

