package ECData::Model::DB;

use strict;
use base 'Catalyst::Model::DBIC::Schema';
use Date::Manip;

__PACKAGE__->config(
    schema_class => 'ECData::Schema',
    'connect_info' => {
        on_connect_do  => [
            "set datestyle to German",
            "set timezone to 'Europe/Prague'",
        ],
        AutoCommit     => 1,
        quote_char     => q("),
        name_sep       => q(.),
        pg_enable_utf8 => 1,
    },
);

#DEPRECATED - pouzivate datatables_args
sub conditions_and_sorting_from_datatables {
# http://richard.wallman.org.uk/2011/12/howto-use-jquerydatatables-with-perlcatalystdbic/
    my $self = shift;
    my $args = shift;

    my $conditions = $args->{conditions} || {};

    my @columns = ();   
    COLUMN:
    for my $n ( 0 .. $args->{iColumns} - 1 ) {
        my $column = $args->{'mDataProp_' . $n};

        push @columns, $column;        

        my $search = $args->{'sSearch_' . $n};
        if ( defined $search && length( $search) ) {
            $conditions->{$column} //= {
                ilike => '%' . $search . '%',
            };            
        }
    }

    my @sorting;
    SORT_COLUMN:
    for my $n ( 0 .. $args->{iSortingCols} - 1 ) {
        next SORT_COLUMN if not exists $args->{"iSortCol_$n"};
#        if ($columns[$args->{"iSortCol_$n"}] eq 'name') { # TODO: mapping v constant
#            push @sorting, {'-'. $args->{"sSortDir_$n"} => 'lastname'}; 
#            push @sorting, {'-'. $args->{"sSortDir_$n"} => 'firstname'};
#        }
#        else {            
            push @sorting, {'-'. $args->{"sSortDir_$n"} => $columns[$args->{"iSortCol_$n"}]};
#        }            
    }       

    return {
        conditions => $conditions, 
        sorting    => \@sorting
    }        

}

sub datatables_args {
    my $self = shift;
    my $args = shift;

    my $conditions = {};

    my @columns = ();
    COLUMN:
    for my $n ( 0 .. $args->{iColumns} - 1 ) {
        my $column = $args->{'mDataProp_' . $n};

        push @columns, $column;

        my $search = $args->{'sSearch_' . $n};

        if ( defined $search && length( $search) ) {
            $conditions->{$column} //= {
                ilike => '%' . $search . '%',
            };
        }
        next COLUMN if ! exists $args->{sSearch};
        next COLUMN if ! length( $args->{sSearch} );


        if ( $args->{'bSearchable_' . $n} eq 'true' ) {
            $conditions->{$column} //= {
                ilike => '%' . $args->{sSearch} . '%',
            };
        }


    }

    my @sorting;
    SORT_COLUMN:
    for my $n ( 0 .. $args->{iSortingCols} - 1 ) {
        next SORT_COLUMN if not exists $args->{"iSortCol_$n"};
        push @sorting, {'-'. $args->{"sSortDir_$n"} => $columns[$args->{"iSortCol_$n"}]};
    }

    return ( \@columns, \@sorting, $conditions );

}

sub make_conditions_from_filter {
    my $self   = shift;
    my $args   = shift;

    my $model  = $args->{model};
    my $filter = $args->{filter};
    my $fields = $args->{filter_fields};

    my @where = ();

    ITEM:
    foreach my $name ( keys %{ $filter } ) {

        my $item = $fields->{$name};

        $item->{type} //= 'eq';

        next ITEM if $item->{type} eq 'custom';

        my $value = $filter->{ $name } // next ITEM;

        if ( exists $item->{regexp} && $value !~ $item->{regexp}) {
            next ITEM;
        }
        if ( $item->{type} eq 'ilike' ) {
            $value = { 'ilike' => "%$value%" };
        }
        elsif ( $item->{type} eq 'like' ) {
            $value = { 'like' => '%' . $value . '%' };
        }
        elsif ( $item->{type} =~ /timestamp_(begin|end)/ ) {

            my $bound = $1;
            my $time;

            if ( exists $item->{time} ) {
                if ( $filter->{$item->{time}} =~ /\d{1,2}:\d{1,2}/) {
                    $time = $filter->{$item->{time}};
                }
            }

            if ( $bound eq 'begin' ) {
                $value = ParseDate( "$value " . ( $time // '00:00') );
                $value &&= { '>=' => UnixDate($value, '%Y-%m-%d %H:%M:00')};
            }
            else {
                $value = ParseDate( "$value " . ( $time // '23:59') );
                $value &&= { '<=' => UnixDate($value, '%Y-%m-%d %H:%M:59')};
            }
        }            
        elsif ( $item->{type} eq 'range' ) {

            next ITEM if $value !~ /(\d+)\D+(\d+)/;
            next ITEM if $1 > $2;

            my ( $min, $max ) = ( $1, $2 );
            my $multiplier = $item->{multiplier} || 1;
            my @range = ();

            $min = undef if exists $item->{min} && $min <= $item->{min};
            $max = undef if exists $item->{max} && $max >= $item->{max};

            push @range, { '>=' => $min * $multiplier } if defined $min;
            push @range, { '<=' => $max * $multiplier } if defined $max;

            $value = \@range;

        }

        my @columns;

        if ( exists $item->{columns} && ref $item->{columns} eq 'ARRAY' ) {
            @columns = @{$item->{columns}};
        }
        elsif ( exists $item->{column}) {
            @columns = ( $item->{column} );
        }
        else {
            @columns = ( $name );
        }

        @columns = grep { $model->result_source->has_column($_) } @columns;
        my @values = ( ref $value eq 'ARRAY') ? @{ $value } : ( $value );

        if ( scalar @columns > 1 ) {
            foreach my $value ( @values ) {
                push @where, [
                    map { { $_ => $value } } @columns
                ];
            }
        }
        elsif ( scalar @columns ) {
            foreach my $value ( @values ) {
                push @where, { $columns[0] => $value };
            }
        }

    }

    return @where;
}




=head1 NAME

ECData::Model::DB - Catalyst DBIC Schema Model

=head1 SYNOPSIS

See L<ECData>

=head1 DESCRIPTION

L<Catalyst::Model::DBIC::Schema> Model using schema L<ECData::Schema>

=head1 GENERATED BY

Catalyst::Helper::Model::DBIC::Schema - 0.41

=head1 AUTHOR

Andrej Ramašeuski

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
