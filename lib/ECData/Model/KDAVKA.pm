package ECData::Model::KDAVKA;
use Moose;
use namespace::autoclean;

extends 'Catalyst::Model';

use constant FORMAT => {
    D => {
        unpack => 'A1 A1 A2 A8 A4 A4 A2 A6 A3 A11 A18 A1 A13 A13 A1',
        map    => {
            3 => 'lab_icz',
            5 => 'month',
            6 => 'year',
            7 => 'number',
        } 
    }, 
    E => {
        unpack => 'A1 A7 A1 A1 A3 A3 A1 A8 A6 A3 A10 A5 A1 A8 A7 A8 A10 A7 A3 A6 A1',
        map    => {
            5  => 'insurance_code',
            7  => 'lab_icz',
            13 => 'customer_icz',
            15 => 'date',
            17 => 'credits',
            18 => 'specialization_code',
        } 
    }, 
};

sub load_file {
    my $self = shift;
    my $file  = shift // die "File not defined";

    open (my $FH, $file);   

    $self->{header}  = {}; 
    $self->{items}   = []; 
    $self->{count} = {
        rows => 0,
    };

    LINE:
    while ( my $line = <$FH> ) {

        $self->{count}{rows}++;

        chomp $line;
        $line =~ /^(\w)/;        
 
        my $record_type = $1;
        $self->{count}{$record_type}++;
        
        my $format = FORMAT->{$record_type} // next LINE;
        my @fields = unpack $format->{unpack}, $line;

        my %item = ();

        COLUMN:
        foreach my $column ( keys %{ $format->{map}} ) {
            $fields[$column] =~ s/^\s+//;
            $item{$format->{map}{$column}} = $fields[$column];
        } 

        if ( $record_type eq 'D' ) {
            $self->{header} //= \%item;
        }
        elsif ( $record_type eq 'E' ) {
            if ( $item{date} =~ /(\d{2})(\d{2})(\d{4})/ ) {
                $item{date}   = "$1.$2.$3"; 
                $item{source} = $line;
            }
            push @{ $self->{items} }, \%item;
        }
                
    }

    close $FH;
    return $self;
}
=head1 NAME

ECData::Model::KDAVKA - Catalyst Model

=head1 DESCRIPTION

Catalyst Model.

=head1 AUTHOR

Andrej Ramašeuski

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

#__PACKAGE__->meta->make_immutable;

1;
