package ECData::Schema::Result::TwoFactorCodes;

use strict;
use warnings;

use base 'ECData::Schema::Result::Subject';

our $VERSION = 1;

__PACKAGE__->table('two_factor_codes');

__PACKAGE__->add_columns(
    qw(
        username
        code
    ),
);

__PACKAGE__->set_primary_key('username');

1;
