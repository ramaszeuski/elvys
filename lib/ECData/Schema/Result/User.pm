package ECData::Schema::Result::User;

use strict;
use warnings;

use base 'DBIx::Class::Core';

our $VERSION = 1;

__PACKAGE__->table('user');

__PACKAGE__->add_columns(
    username => {
        data_type     => 'varchar',
        is_nullable   => 0,
        size          => 32,
    },
    password => {
        data_type     => 'varchar',
        is_nullable   => 0,
        size          => 32,
    },
    active => {
        data_type     => 'bool',
        is_nullable   => 0,
        default_value => 'true',
    },
    firstname => {
        data_type     => 'text',
    },
    lastname => {
        data_type     => 'text',
    },
    email => {
        data_type     => 'text',
    },
#    phone => {
#        data_type     => 'text',
#    },
#    address => {
#        data_type     => 'text',
#    },
#    city => {
#        data_type     => 'text',
#    },
#    zip => {
#        data_type     => 'text',
#    },
    'tfa' => {
        data_type     => 'integer',
        is_nullable   => 0,
        default_value => 0,
    },
);

__PACKAGE__->set_primary_key('username');

__PACKAGE__->has_many(
    user_roles => 'ECData::Schema::Result::UserRole',
    {
        'foreign.user_username' => 'self.username'
    },
);

__PACKAGE__->many_to_many(
    roles => 'user_roles', 'role'
);

__PACKAGE__->has_many(
    log_events => 'ECData::Schema::Result::Log',
    {
        'foreign.user_username' => 'self.username'
    },
);

__PACKAGE__->has_many(
    take_customer_requests => 'ECData::Schema::Result::TakeCustomerRequest',
    {
        'foreign.dealer_account_login' => 'self.username'
    },
);

sub deletable {
    my $self = shift;    
    return 1;
}

sub one_role {
    my $self = shift;    
    return $self->user_roles->count() == 1;
}

sub name {
    my $self = shift;
    return 
        join ' ',
        grep /\w/,
        (
            $self->firstname,
            $self->lastname,
        );
}

1;

__END__
