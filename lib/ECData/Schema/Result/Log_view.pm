package ECData::Schema::Result::Log_view;

use strict;
use warnings;
 
use base 'ECData::Schema::Result::Log';

our $VERSION = 1;

__PACKAGE__->table('log_view');

__PACKAGE__->add_columns(
    qw(
        user_role 
        user_name 
        user_firstname 
        user_lastname 
    ),  
);

sub format_timestamp {
    my $self = shift;
#    if ($self->timestamp =~ /(\d{4})\-(\d{2})\-(\d{2}) (\d{2}:\d{2})/) {
#        return "$3.$2.$1 $4";    
#    }
#    else {
        return $self->timestamp;
#    }
}

sub user_fullname {
    my $self = shift;   

    if ($self->user_name) {
        return $self->user_name;
    }
    elsif ($self->user_firstname || $self->user_lastname) {
        return $self->user_firstname . ' ' . $self->user_lastname;
    }
    else {
        return $self->user_username;
    }
}

sub event_name {
    my $self = shift;   
    return $self->resultset_class->event_name($self->event);
}
1;
