package ECData::Schema::Result::TakeCustomerRequest_view;

use strict;
use warnings;

use base 'ECData::Schema::Result::TakeCustomerRequest';

our $VERSION = 1;

__PACKAGE__->table('take_customer_request_view');

__PACKAGE__->add_columns(
    qw(
        customer_name
        pretender_name
        dealer_name
        manager_name
    ),
);

__PACKAGE__->set_primary_key('id');

sub date {
    my $self = shift;
    my $column = shift;

    $self->get_column( $column ) =~ /(\d{4})\-(\d{2})\-(\d{2})/;
    return "$3.$2.$1";    
}

1;

