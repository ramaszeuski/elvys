package ECData::Schema::Result::Creator;

use strict;
use warnings;

use base 'DBIx::Class::Core';

our $VERSION = 1;

__PACKAGE__->table('creator');

__PACKAGE__->add_columns(
    id => {
        sequence    => 'uid_seq',
        data_type   => 'integer',
        is_nullable => 0,
    },
    qw(
        name 
        kod_firmy 
        kod_prog 
        verze_prog 
        liccis_prog
    ),
);

__PACKAGE__->set_primary_key('id');

__PACKAGE__->add_unique_constraint(
    version => [ 
        qw(kod_firmy kod_prog verze_prog)
    ]
);

__PACKAGE__->has_many(
    xmls => 'ECData::Schema::Result::XML',
    {
        'foreign.creator_id' => 'self.id'
    },
);

1;


