package ECData::Schema::Result::LoginRole_view;

use strict;
use warnings;

use base 'DBIx::Class::Core';

our $VERSION = 1;

__PACKAGE__->table('login_role_view');

__PACKAGE__->add_columns(
    qw( login role ),
);

__PACKAGE__->set_primary_key(
    qw( login role ),
);

1;

__END__


