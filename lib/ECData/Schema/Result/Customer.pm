package ECData::Schema::Result::Customer;

use strict;
use warnings;
use JSON;

use base 'ECData::Schema::Result::Subject';

our $VERSION = 1;

__PACKAGE__->table('customer');

__PACKAGE__->add_columns(
    qw(
        ico
        icz
        icp
        icl
        pcz
        dealer_account_login
        dealer_contract
        email_notify
        region_id
        share
        specialization_code
        bonusprogramm_percents
        changed
        last_login
        direct_results
    ),
    primary_lab_id => { data_type => "INTEGER", is_nullable => 1 },
    clinic_id      => { data_type => "INTEGER", is_nullable => 1 },
);

__PACKAGE__->set_primary_key('id');
__PACKAGE__->add_unique_constraint(
    icx => [qw(ico icz icp icl)]
);

__PACKAGE__->belongs_to(
    region => 'ECData::Schema::Result::Region',
    {
        'foreign.id' => 'self.region_id',
    },
    {
        join_type => 'left',
    }
);

__PACKAGE__->belongs_to(
    dealer => 'ECData::Schema::Result::User',
    {
        'foreign.username' => 'self.dealer_account_login',
    },
    {
        join_type => 'left',
    }
);

__PACKAGE__->belongs_to(
    primary_lab => 'ECData::Schema::Result::Lab',
    {
        'foreign.id' => 'self.primary_lab_id',
    },
    {
        join_type => 'left',
    }
);

__PACKAGE__->belongs_to(
    clinic => 'ECData::Schema::Result::Clinic',
    {
        'foreign.id' => 'self.clinic_id',
    },
    {
        join_type => 'left',
    }
);

__PACKAGE__->has_many(
    xmls => 'ECData::Schema::Result::XML',
    {
        'foreign.customer_id' => 'self.id'
    },
);

__PACKAGE__->has_many(
    credits => 'ECData::Schema::Result::Credit_view',
    {
        'foreign.customer_id' => 'self.id'
    },
);

__PACKAGE__->has_many(
    take_requests => 'ECData::Schema::Result::TakeCustomerRequest',
    {
        'foreign.customer_id' => 'self.id'
    },
);

__PACKAGE__->has_many(
    meetings => 'ECData::Schema::Result::Meeting',
    {
        'foreign.customer_id' => 'self.id'
    },
);

__PACKAGE__->has_many(
    commissions => 'ECData::Schema::Result::Commission',
    {
        'foreign.customer_id' => 'self.id'
    },
);

__PACKAGE__->inflate_column(
    'share',
    {
        inflate => sub { from_json(shift) },
        deflate => sub { to_json(shift)   },
    },
);

sub specialization_name {
    my $self = shift;

    return '' if ! $self->specialization_code;

    my $schema = $self->result_source->schema;

    my $specialization = $schema->resultset('DastaList')->search(
        {
            id   => $self->specialization_code,
            list => 'VZP_ODB',
        }
    )->first || return '';

    return $specialization->content->{name};
}

sub created_date {
    my $self = shift;

    my $created = $self->created;
    $created =~ s/\s.*//;

    if ( $created =~ /(\d+)\-(\d+)\-(\d+)/ ) {
        $created = "$3.$2.$1";
    }

    return $created;
}

sub changed_date {
    my $self = shift;

    my $changed = $self->changed;
    $changed =~ s/\s.*//;

    if ( $changed =~ /(\d+)\-(\d+)\-(\d+)/ ) {
        $changed = "$3.$2.$1";
    }

    return $changed;
}

sub last_login_date {
    my $self = shift;

    my $last_login = $self->last_login;
    $last_login =~ s/\s.*//;

    if ( $last_login =~ /(\d+)\-(\d+)\-(\d+)/ ) {
        $last_login = "$3.$2.$1";
    }

    return $last_login;
}

sub ids {
    my $self = shift;

    my @ids = ( $self->id );

    if ( ref $self->share ) {
        @ids = ( @ids, @{ $self->share } );
    }

    return wantarray ? @ids : \@ids;
}

1;

