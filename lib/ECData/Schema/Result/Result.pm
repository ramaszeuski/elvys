package ECData::Schema::Result::Result;

use strict;
use warnings;

use base 'DBIx::Class::Core';

our $VERSION = 1;

__PACKAGE__->table('result');

__PACKAGE__->add_columns(
    qw(
        id
        sampling
        viewed
        notified
        xml_id
        patient_id
        value
    ),
);

__PACKAGE__->set_primary_key('id');

__PACKAGE__->add_unique_constraint(
    uid => [qw(xml_id patient_id)]
);

__PACKAGE__->belongs_to(
    xml => 'ECData::Schema::Result::XML',
    {
        'foreign.id' => 'self.xml_id',
    },
);

__PACKAGE__->belongs_to(
    patient => 'ECData::Schema::Result::Patient',
    {
        'foreign.id' => 'self.patient_id',
    },
);

__PACKAGE__->belongs_to(
    view => 'ECData::Schema::Result::Result_view',
    {
        'foreign.id' => 'self.id',
    },
);
1;
