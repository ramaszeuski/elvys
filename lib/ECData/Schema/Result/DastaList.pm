package ECData::Schema::Result::DastaList;

use strict;
use warnings;

use base 'DBIx::Class::Core';
use JSON;

our $VERSION = 1;

__PACKAGE__->table('dasta_list');

__PACKAGE__->add_columns(
    qw(
        list
        id
        content
    ),
);

__PACKAGE__->set_primary_key('list', 'id');

__PACKAGE__->inflate_column(
    'content',
    {
        inflate => sub { from_json(shift) },
        deflate => sub { to_json(shift)   },
    },        
);

1;

