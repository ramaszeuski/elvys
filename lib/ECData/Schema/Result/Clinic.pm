package ECData::Schema::Result::Clinic;

use strict;
use warnings;

use base 'ECData::Schema::Result::Subject';

our $VERSION = 1;

__PACKAGE__->table('clinic');

__PACKAGE__->set_primary_key('id');

1;

