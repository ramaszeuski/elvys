package ECData::Schema::Result::XML_Archive;

use strict;
use warnings;

use base 'DBIx::Class::Core';

our $VERSION = 1;

__PACKAGE__->table('xml_archive');

__PACKAGE__->add_columns(
    qw(
        id 
        generated 
        processed 
        downloaded 
        deleted 
        lab_id 
        customer_id 
        creator_id

        dasta_id_soubor 
        dasta_verze_ds 
        dasta_verze_nclp 
        dasta_bin_priloha 
        dasta_ur 
        dasta_typ_odesm 
        dasta_ozn_soub 
        dasta_potvrzeni 

        has_result
    ),  
);

__PACKAGE__->set_primary_key('id');

__PACKAGE__->might_have(
    result => 'ECData::Schema::Result::Result_Archive',
    {
        'foreign.xml_id' => 'self.id'
    },
);
1;

