package ECData::Schema::Result::Customer_view;

use strict;
use warnings;

use base 'ECData::Schema::Result::Customer';
our $VERSION = 1;

use constant CONTRACT_LABEL => '[S]';

__PACKAGE__->table('customer_view');

__PACKAGE__->add_columns(
    qw(
        dealer_name
        region_name
        primary_lab_name
    ),
);

sub dealer_name_with_contract_label {
    my $self  = shift;
    my $label = shift // CONTRACT_LABEL;

    my $name = $self->dealer_name;

    if ( $self->dealer_contract ) {
        $name .= " $label";
    }

    return $name;
}

1;
