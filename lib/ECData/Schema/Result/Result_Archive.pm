package ECData::Schema::Result::Result_Archive;

use strict;
use warnings;

use base 'DBIx::Class::Core';

our $VERSION = 1;

__PACKAGE__->table('result_archive');

__PACKAGE__->add_columns(
    qw(
        id 
        sampling 
        viewed 
        xml_id 
        patient_id
    ),  
);

__PACKAGE__->set_primary_key('id');

