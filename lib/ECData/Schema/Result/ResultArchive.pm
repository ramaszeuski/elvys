package ECData::Schema::Result::ResultArchive;

use strict;
use warnings;

use base 'ECData::Schema::Result::Result';

our $VERSION = 1;

__PACKAGE__->table('result_archive');

1;

