package ECData::Schema::Result::Region;

use strict;
use warnings;

use base 'DBIx::Class::Core';

our $VERSION = 1;

__PACKAGE__->table('region');

__PACKAGE__->add_columns(
    id => {
        data_type     => 'varchar',
        is_nullable   => 0,
        size          => 8,
    },
    name => {
        data_type     => 'varchar',
        is_nullable   => 0,
    },
);

__PACKAGE__->set_primary_key('id');

1;
