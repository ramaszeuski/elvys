package ECData::Schema::Result::Meeting_view;

use strict;
use warnings;

use base 'ECData::Schema::Result::Meeting';

our $VERSION = 1;

__PACKAGE__->table('meeting_view');

__PACKAGE__->add_columns(
    qw( 
        dealer_firstname
        dealer_lastname
        customer_name 
    ),
);

sub dealer_name {
    my $self = shift;
    return 
        join ' ',
        grep /\w/,
        (
            $self->dealer_firstname,
            $self->dealer_lastname,
        );
}
1;

__END__

