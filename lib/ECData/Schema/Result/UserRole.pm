package ECData::Schema::Result::UserRole;

use strict;
use warnings;

use base 'DBIx::Class::Core';

our $VERSION = 1;

__PACKAGE__->table('user_role');

__PACKAGE__->add_columns(
    user_username => {
        data_type     => 'varchar',
        is_nullable   => 0,
        size          => 32,
    },
    role_role => {
        data_type     => 'varchar',
        is_nullable   => 0,
        size          => 32,
    },
);

__PACKAGE__->set_primary_key('user_username', 'role_role');

__PACKAGE__->belongs_to(
    user => 'ECData::Schema::Result::User',
    {
        'foreign.username' => 'self.user_username',
    },
);

__PACKAGE__->belongs_to(
    role => 'ECData::Schema::Result::Role',
    {
        'foreign.role' => 'self.role_role',
    },
);

1;

__END__

