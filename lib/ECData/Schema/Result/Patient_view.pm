package ECData::Schema::Result::Patient_view;

use strict;
use warnings;

use base 'ECData::Schema::Result::Patient';

our $VERSION = 1;

__PACKAGE__->table('patient_view');

__PACKAGE__->add_columns(
    qw(
        lastname_cz_off
    ),  
);

1;

