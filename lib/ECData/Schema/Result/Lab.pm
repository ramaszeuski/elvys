package ECData::Schema::Result::Lab;

use strict;
use warnings;

use base 'ECData::Schema::Result::Subject';

our $VERSION = 1;

__PACKAGE__->table('lab');

__PACKAGE__->add_columns(
    qw(
        ico
        icz
        icp
        icl
        pcz
    ),
);

__PACKAGE__->set_primary_key('id');

__PACKAGE__->add_unique_constraint(
    icx => [qw(ico icz icp icl)]
);

1;
