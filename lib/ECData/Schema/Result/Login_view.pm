package ECData::Schema::Result::Login_view;

use strict;
use warnings;

use base 'DBIx::Class::Core';
use Data::Password qw(:all);

# TODO: sync s lib/ECData/FormFu/Validator/StrongPassword.pm
$DICTIONARY = 5;
$GROUPS     = 3;
$MINLEN     = 7;
$MAXLEN     = 32;

our $VERSION = 1;

__PACKAGE__->table('login_view');

__PACKAGE__->add_columns(
    qw(
        username
        password
    ),
);

__PACKAGE__->set_primary_key('username');

__PACKAGE__->has_many(
    login_roles => 'ECData::Schema::Result::LoginRole_view',
    {
        'foreign.login' => 'self.username'
    },
);

__PACKAGE__->might_have(
    subject => 'ECData::Schema::Result::Subject',
    {
        'foreign.login' => 'self.username'
    },
);

__PACKAGE__->might_have(
    customer => 'ECData::Schema::Result::Customer',
    {
        'foreign.login' => 'self.username'
    },
);

__PACKAGE__->might_have(
    two_factor_code => 'ECData::Schema::Result::TwoFactorCodes',
    {
        'foreign.username' => 'self.username'
    },
);

sub has_weak_password {
    my $self = shift;
    return IsBadPassword($self->password);
}

1;

__END__
#
