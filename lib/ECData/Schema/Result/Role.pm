package ECData::Schema::Result::Role;

use strict;
use warnings;

use base 'DBIx::Class::Core';

our $VERSION = 1;

__PACKAGE__->table('role');

__PACKAGE__->add_columns(
    role => {
        data_type     => 'varchar',
        is_nullable   => 0,
        size          => 32,
    },
    active => {
        data_type     => 'bool',
        is_nullable   => 0,
        default_value => 'true',
    },
    name => {
        data_type     => 'text',
        is_nullable   => 0,
    },
);

__PACKAGE__->set_primary_key('role');


__PACKAGE__->has_many(
    user_roles => 'ECData::Schema::Result::UserRole',
    {
        'foreign.role_role' => 'self.role'
    },
);

__PACKAGE__->many_to_many(
    users => 'user_roles', 'user'
);

1;

__END__

