package ECData::Schema::Result::Dealer_view;

use strict;
use warnings;

use base 'ECData::Schema::Result::User';
our $VERSION = 1;

__PACKAGE__->table('dealer_view');

__PACKAGE__->add_columns(
    qw(
        name
    ),
);

1;

