package ECData::Schema::Result::ResultAll_view;

use strict;
use warnings;

use base 'ECData::Schema::Result::Result_view';

our $VERSION = 1;

__PACKAGE__->table('result_all_view');

1;

