package ECData::Schema::Result::Meeting;

use strict;
use warnings;

use base 'DBIx::Class::Core';

our $VERSION = 1;

__PACKAGE__->table('meeting');

__PACKAGE__->add_columns(
    id => {
        sequence    => 'uid_seq',
        data_type   => 'integer',
        is_nullable => 0,
    },
    qw(
        user_username
        customer_id
        created
        deleted
        state
        date
        location
        reason
        program
        record
        notes
    ),
);

__PACKAGE__->set_primary_key('id');

__PACKAGE__->belongs_to(
    dealer => 'ECData::Schema::Result::User',
    {
        'foreign.username' => 'self.user_username',
    },
);

__PACKAGE__->belongs_to(
    customer => 'ECData::Schema::Result::Customer',
    {
        'foreign.id' => 'self.customer_id',
    },
);

1;

__END__
