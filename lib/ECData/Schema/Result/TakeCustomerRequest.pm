package ECData::Schema::Result::TakeCustomerRequest;

use strict;
use warnings;

use base 'DBIx::Class::Core';

our $VERSION = 1;

__PACKAGE__->table('take_customer_request');

__PACKAGE__->add_columns(
    id => {
        sequence    => 'uid_seq',
        data_type   => 'integer',
        is_nullable => 0,
    },
    qw(
        state 
        created 
        decided 
        customer_id 
        dealer_account_login 
        manager_account_login 
        request response
    ),
);

__PACKAGE__->set_primary_key('id');

__PACKAGE__->belongs_to(
    customer => 'ECData::Schema::Result::Customer',
    {
        'foreign.id' => 'self.customer_id',
    },
);

1;
