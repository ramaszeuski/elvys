package ECData::Schema::Result::Credit;

use strict;
use warnings;

use base 'DBIx::Class::Core';

our $VERSION = 1;

__PACKAGE__->table('credit');

__PACKAGE__->add_columns(
    id => {
        sequence    => 'uid_seq',
        data_type   => 'integer',
        is_nullable => 0,
    },
    qw(
        lab_id 
        customer_id 
        specialization_code 
        insurance_code 
        date
        credits
    ),
);

__PACKAGE__->set_primary_key('id');

__PACKAGE__->add_unique_constraint(
    'secondary_key' => [qw(
        lab_id 
        customer_id 
        specialization_code 
        insurance_code 
        date
    )]
);

__PACKAGE__->belongs_to(
    customer => 'ECData::Schema::Result::Customer',
    {
        'foreign.id' => 'self.customer_id',
    },
);

1;
