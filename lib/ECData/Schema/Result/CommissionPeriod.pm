package ECData::Schema::Result::CommissionPeriod;

use strict;
use warnings;

use base 'DBIx::Class::Core';

our $VERSION = 1;

__PACKAGE__->table('commission_period');

__PACKAGE__->add_columns(
    id => {
        sequence    => 'uid_seq',
        data_type   => 'integer',
        is_nullable => 0,
    },
    qw(
        year 
        quarter 
        coefficient 
        consultation_price 
        calculated
    ),
);

__PACKAGE__->set_primary_key('id');

__PACKAGE__->add_unique_constraint(
    'secondary_key' => [qw(
        year 
        quarter 
    )]
);

__PACKAGE__->has_many(
    commissions => 'ECData::Schema::Result::Commission',
    {
        'foreign.period_id' => 'self.id'
    },
);

sub is_ready_to_calculate {
    my $self = shift;
    return if $self->calculated;    
    return if ! $self->coefficient;    
    return if ! $self->consultation_price;    
    return 1;
}

1;

