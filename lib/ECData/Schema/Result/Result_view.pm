package ECData::Schema::Result::Result_view;

use strict;
use warnings;

use base 'DBIx::Class::Core';

our $VERSION = 1;

__PACKAGE__->table('result_view');

__PACKAGE__->add_columns(
    qw(
        id
        sampling
        xml_id
        generated
        lab_id
        lab_icz
        lab_icp
        lab_name
        customer_icz
        customer_id
        customer_icp
        customer_name
        customer_email
        dealer_account_login
        patient_id
        rc
        firstname
        lastname
        name_cz_off
        value
    ),
);

__PACKAGE__->set_primary_key('id');

sub date {
    my $self   = shift;
    my $column = shift;
    my $date   = $self->get_column( $column );

    if ( $date =~ /(\d{4})\-(\d{2})\-(\d{2})/ ) {
        return "$3.$2.$1";
    }
    else {
        $date =~ s/\s.+//;
        return $date;
    }
}

1;
