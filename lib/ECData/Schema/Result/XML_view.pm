package ECData::Schema::Result::XML_view;

use strict;
use warnings;

use base 'DBIx::Class::Core';

our $VERSION = 1;

__PACKAGE__->table('xml_view');

__PACKAGE__->add_columns(
    qw(
        id 
        generated 
        processed 
        downloaded 
        deleted 
        lab_id 
        customer_id 
        creator_id 
        dasta_id_soubor 
        dasta_verze_ds 
        dasta_verze_nclp 
        dasta_bin_priloha 
        dasta_ur 
        dasta_typ_odesm 
        dasta_ozn_soub 
        dasta_potvrzeni 
        lab_icz 
        lab_icp 
        lab_name 
        customer_icz 
        customer_icp 
        customer_name
    ),  
);

__PACKAGE__->set_primary_key('id');

sub timestamp {
    my $self = shift;
    my $column = shift;
    my $timestamp = $self->get_column( $column );
    if ( $timestamp && $timestamp =~ /(\d{4})\-(\d{2})\-(\d{2}) (\d{2}:\d{2})/) {
        return "$3.$2.$1 $4";    
    }
    else {
        return $timestamp;
    }
}

1;

