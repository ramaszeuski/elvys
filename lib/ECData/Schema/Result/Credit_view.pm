package ECData::Schema::Result::Credit_view;

use strict;
use warnings;

use base 'ECData::Schema::Result::Credit';

our $VERSION = 1;

__PACKAGE__->table('credit_view');

__PACKAGE__->add_columns(
    qw(
        year
        month
        quarter
        dealer_account_login
        dealer_contract
        lab_is_primary
        bonusprogramm_percents
        allow_commissions
        customer_icp
        customer_name
    ),  
);

1;
