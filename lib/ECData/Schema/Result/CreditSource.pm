package ECData::Schema::Result::CreditSource;

use strict;
use warnings;

use base 'DBIx::Class::Core';

our $VERSION = 1;

__PACKAGE__->table('credit_source');

__PACKAGE__->add_columns(
    qw(
        source 
        date
        processed
    ),
);

__PACKAGE__->set_primary_key('source');

1;
