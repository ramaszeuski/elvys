package ECData::Schema::Result::Commission;

use strict;
use warnings;

use base 'DBIx::Class::Core';

our $VERSION = 1;

__PACKAGE__->table('commission');

__PACKAGE__->add_columns(
    id => {
        sequence    => 'uid_seq',
        data_type   => 'integer',
        is_nullable => 0,
    },
    qw(
        period_id 
        customer_id 
        transfered 
        calculated 
        consultations 
        commissions 
        transfer 
        invoiced
    ),
);

__PACKAGE__->set_primary_key('id');

__PACKAGE__->belongs_to(
    period => 'ECData::Schema::Result::CommissionPeriod',
    {
        'foreign.id' => 'self.period_id',
    },
);

__PACKAGE__->belongs_to(
    customer => 'ECData::Schema::Result::Customer',
    {
        'foreign.id' => 'self.customer_id',
    },
);

1;
