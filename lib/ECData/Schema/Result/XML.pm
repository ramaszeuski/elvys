package ECData::Schema::Result::XML;

use strict;
use warnings;

use base 'DBIx::Class::Core';

our $VERSION = 1;

__PACKAGE__->table('xml');

__PACKAGE__->add_columns(
    qw(
        id 
        generated 
        processed 
        downloaded 
        deleted 
        lab_id 
        customer_id 
        creator_id

        dasta_id_soubor 
        dasta_verze_ds 
        dasta_verze_nclp 
        dasta_bin_priloha 
        dasta_ur 
        dasta_typ_odesm 
        dasta_ozn_soub 
        dasta_potvrzeni 
    ),  
);

__PACKAGE__->set_primary_key('id');

__PACKAGE__->belongs_to(
    lab => 'ECData::Schema::Result::Lab',
    {
        'foreign.id' => 'self.lab_id',
    },
);

__PACKAGE__->belongs_to(
    customer => 'ECData::Schema::Result::Customer',
    {
        'foreign.id' => 'self.customer_id',
    },
);

__PACKAGE__->belongs_to(
    creator => 'ECData::Schema::Result::Creator',
    {
        'foreign.id' => 'self.creator_id',
    },
);

__PACKAGE__->has_many(
    results => 'ECData::Schema::Result::Result',
    {
        'foreign.xml_id' => 'self.id'
    },
);

1;

