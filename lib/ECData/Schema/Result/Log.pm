package ECData::Schema::Result::Log;

use strict;
use warnings;

use base 'DBIx::Class::Core';

our $VERSION = 1;

__PACKAGE__->table('log');

__PACKAGE__->add_columns(
    id => {
        sequence    => 'uid_seq',
        data_type         => 'integer',
        is_nullable       => 0,
    },
    timestamp => {
        data_type     => 'datetime',
        is_nullable   => 0,
        default_value => \'current_timestamp',
    },
    event => {
        data_type     => 'varchar',
        is_nullable   => 0,
        size          => 32,
    },
    user_username => {
        data_type     => 'varchar',
        is_nullable   => 1,
        size          => 32,
    },
    address       => {
        data_type     => 'inet',
        is_nullable   => 1,
    },
    data => {
        data_type     => 'text',
    },
);

__PACKAGE__->set_primary_key('id');

1;

__END__

