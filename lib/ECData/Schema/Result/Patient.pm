package ECData::Schema::Result::Patient;

use strict;
use warnings;

use base 'ECData::Schema::Result::Subject';

our $VERSION = 1;

__PACKAGE__->table('patient');

__PACKAGE__->add_columns(
    qw(
        rc
        birthdate
        sex
        insurance_id
    ),
);

__PACKAGE__->set_primary_key('id');
__PACKAGE__->add_unique_constraint(
    rc => [qw(rc)]
);

1;

