package ECData::Schema::ResultSet::Log_view;

use strict;
use warnings;
use utf8;

use base 'DBIx::Class::ResultSet';

our $VERSION = 1;

use constant EVENT => {
    user_login           => 'Přihlásil se',
    user_logout          => 'Odhlásil se',   
    xml_download         => 'Stahnul XML',
    customer_edit        => 'Editoval zakázníka',
    user_create          => 'Přidal uživatele',
    lab_edit             => 'Editoval laboratoř',
    user_edit            => 'Editoval uživatele',
    customer_edit_shared => 'Nastavil sdílení výsledků',
    meeting_create       => 'Přidal návštěvu (jednání)',
    meeting_edit         => 'Editoval návštěvu (jednání)',
    change_password      => 'Změnil heslo',

};

sub events {
    return [
        map { 
            { value => $_, label => EVENT->{$_} }
        } ( sort keys %{ EVENT() } )
    ];        
}

sub event_name {
    my $self  = shift;
    my $event = shift;
    return EVENT->{$event} || $event;
}

1;
