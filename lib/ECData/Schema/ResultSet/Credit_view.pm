package ECData::Schema::ResultSet::Credit_view;

use strict;
use warnings;
use utf8;

use base 'DBIx::Class::ResultSet';

our $VERSION = 1;

sub summary {
    my $self       = shift;
    my $conditions = shift;    

    my @group_by = qw( month quarter year );

    return $self->search(
        $conditions,
        {
            select => [
                @group_by,
                { sum => 'credits' },
            ],
            as => [
                @group_by,
                'credits',
            ],
            group_by => \@group_by,
            order_by => \@group_by,
        }
    );
}

sub summary_matrix {
    my $self = shift;

    my $matrix;
    my $summary = $self->summary(@_);

    return undef if ! $summary->count;

    while ( my $r = $summary->next() ) {
        $matrix->{months}{ $r->year }{ $r->month } = $r->credits;
        $matrix->{quarters}{ $r->year }{ $r->quarter } += $r->credits;
        $matrix->{all}{ $r->year } += $r->credits;
    }

    $matrix->{years} = [ sort keys %{ $matrix->{all} } ];

    return $matrix;
}

1;
