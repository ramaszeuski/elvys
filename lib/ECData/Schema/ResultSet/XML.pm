package ECData::Schema::ResultSet::XML;

use strict;
use warnings;
use utf8;

use base 'DBIx::Class::ResultSet';

use XML::Simple;
use Digest::SHA;
use String::MkPasswd qw(mkpasswd);

our $VERSION = 1;

use constant EXTRACT => {
    50700 => 1,
};

sub create_from_xml_file {
    my $self = shift;
    my $file = shift // die "412 Filename required";

    -r $file || die "404 File $file not found";

    my $id = Digest::SHA->new->addfile( $file, 'b')->hexdigest();

    my $xml = XMLin (
        $file,
        ForceArray   => ['ip', 'vr'],
        ForceContent => 1,
    );

    my $dbic = $self->result_source->schema;
    my $guard = $dbic->txn_scope_guard;

    # informacni system
    my $creator = $dbic->resultset('Creator')->find_or_create(
        {
            kod_firmy   => $xml->{zdroj_is}{kod_firmy},
            kod_prog    => $xml->{zdroj_is}{kod_prog},
            verze_prog  => $xml->{zdroj_is}{verze_prog},
            liccis_prog => $xml->{zdroj_is}{liccis_prog},
        },
        {
            key => 'version',
        }
    );

    my $lab = $self->find_by_xml('Lab', $xml->{is});

    if ( ! defined $lab ) {
        $lab = $dbic->resultset('Lab')->create(
            {
                icx_key($xml->{is}),
                password => password(),
                role     => 'lab',
                name     => $xml->{is}{a}{jmeno}{content},
                country  => $xml->{is}{a}{stat}{content},
                city     => $xml->{is}{a}{mesto}{content},
                address  => $xml->{is}{a}{adr}{content},
                zip      => $xml->{is}{a}{psc}{content},
            },
        );
        $lab->update({ login => 'AL' . $lab->id });
    }

    my $customer = $self->find_by_xml('Customer', $xml->{pm});

    if ( ! defined $customer ) {
        $customer = $dbic->resultset('Customer')->create(
            {
                icx_key($xml->{pm}),
                password => password(),
                role     => 'customer',
                name     => $xml->{pm}{a}{jmeno}{content},
                country  => $xml->{pm}{a}{stat}{content},
                city     => $xml->{pm}{a}{mesto}{content},
                address  => $xml->{pm}{a}{adr}{content},
                zip      => $xml->{pm}{a}{psc}{content},
            },
        );
        $customer->update({ login => 'AZ' . $customer->id });
    }

    my $xml_rs = $dbic->resultset('XML')->find_or_create(
        {
            id                => $id,
            generated         => filter_date( $xml->{dat_vb} ),
            lab_id            => $lab->id,
            creator_id        => $creator->id,
            customer_id       => $customer->id,

            dasta_id_soubor   => $xml->{id_soubor},
            dasta_verze_ds    => $xml->{verze_ds},
            dasta_verze_nclp  => $xml->{verze_nclp},
            dasta_bin_priloha => $xml->{bin_priloha},
            dasta_ur          => $xml->{ur},
            dasta_typ_odesm   => $xml->{typ_odesm},
            dasta_ozn_soub    => $xml->{ozn_soub},
            dasta_potvrzeni   => $xml->{potvrzeni},
        },
    );

    IP:
    foreach my $ip ( @{$xml->{is}->{ip}} ) {

        my $rc = $ip->{rodcis}{content} || $ip->{id_pac} || '0000000000';

        my %patient = (
            password     => password(),
            role         => 'patient',
            rc           => substr($rc, 0, 10),
            firstname    => $ip->{jmeno}{content},
            lastname     => $ip->{prijmeni}{content},
            sex          => $ip->{sex}{content},
            birthdate    => $ip->{dat_dn}{content},
            $ip->{p} && $ip->{p}{kodpoj} =~ /(\d+)/ ? (
                insurance_id => $1,
            ) : (),
        );

        # najdeme vsechny pacienty s zadanym rodnym cislem
        my $patients = $dbic->resultset('Patient')->search(
            {
                rc => $patient{rc},
            },
        );

        # ale budeme vyzadovat i shodu prijmeni
        PATIENT:
        while ( my $patient = $patients->next() ) {
            if ( cz_off( $patient{lastname} ) eq cz_off($patient->lastname) ) {
                $patient{id} = $patient->id;
                last PATIENT;
            }
        }

        if ( ! exists $patient{id} ) {
            my $patient = $dbic->resultset('Patient')->create( \%patient );
            $patient{id} = $patient->id;
        }

        my $value = '';

        # HLEDANI EXTRAHOVATELNE HODNOTY
        foreach my $test ( @{$ip->{v}{vr}} ) {
            if (exists EXTRACT->{ $test->{klic_nclp} } ) {
                eval { #TODO: ruzne extractory
                    $value = $test->{vrx}{hodnota_nt}->{content};
                };
            }
        }

        $dbic->resultset('Result')->find_or_create(
            {
                xml_id         => $id,
                patient_id     => $patient{id},
                sampling       => filter_date($ip->{v}{vr}[0]{dat_du}->{content}),
                value          => $value,
#               generated  => filter_date($ip->{v}{dat_vb}),
            },
            {
                key => 'uid',
            },
        );


    }
    $guard->commit;

    return $xml_rs;
}

sub icx_key {
    my $object = shift;
    my %key;
    foreach my $ic (qw(ico icz icp icl)) {
        if ( exists $object->{$ic} && $object->{$ic}) {
            $key{$ic} = $object->{$ic};
        }
    }
    return %key;
}

sub find_by_xml {
    my $self  = shift;
    my $class = shift;
    my $xml   = shift;

    my $dbic = $self->result_source->schema;
    my $resultset = $dbic->resultset($class);

    my $conditions = {};

    KEY:
#   foreach my $key ( 'icp', 'icz', 'ico' ) {
    foreach my $key ( 'icp', 'icz' ) {
        next KEY if ! $xml->{$key};
        $conditions->{$key} = $xml->{$key};
        my $rs = $resultset->search( $conditions );

        if ( $rs->count ) {
            return $rs->first;
        }
        else {
            return undef;
        }
    }
}

sub password {
     my $password = mkpasswd(
        -length     => 36,
        -minnum     => 12,
        -minlower   => 12,
        -minupper   => 12,
        -minspecial => 0,
        -distribute => 1,
    );
    $password =~ s/[ijlJI1oDO0zZ2]//g;
    return substr($password, 0, 8);
}

sub filter_date {
    my $date = shift;
    $date =~ s/T/ /;
    return $date;
}

sub cz_off {
    my $text = shift;
    $text =~ s/á/a/g;
    $text =~ s/č/c/g;
    $text =~ s/ď/d/g;
    $text =~ s/é/e/g;
    $text =~ s/ě/e/g;
    $text =~ s/í/i/g;
    $text =~ s/ĺ/l/g;
    $text =~ s/ň/n/g;
    $text =~ s/ó/o/g;
    $text =~ s/ř/r/g;
    $text =~ s/š/s/g;
    $text =~ s/ť/t/g;
    $text =~ s/ú/u/g;
    $text =~ s/ů/u/g;
    $text =~ s/ý/y/g;
    $text =~ s/ž/z/g;
    $text =~ s/Á/a/g;
    $text =~ s/Č/c/g;
    $text =~ s/Ď/d/g;
    $text =~ s/É/e/g;
    $text =~ s/Ě/e/g;
    $text =~ s/Í/i/g;
    $text =~ s/Ĺ/l/g;
    $text =~ s/Ň/n/g;
    $text =~ s/Ó/o/g;
    $text =~ s/Ř/r/g;
    $text =~ s/Š/s/g;
    $text =~ s/Ť/t/g;
    $text =~ s/Ú/u/g;
    $text =~ s/Ů/u/g;
    $text =~ s/Ý/y/g;
    $text =~ s/Ž/z/g;
    return $text;
}

1;
