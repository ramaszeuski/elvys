#!/bin/sh

VARS='$DB_PORT:$DB_HOST:$DB_NAME:$DB_USER:$DB_PASSWORD:$WORKDIR:$VENDOR:$ROLES_FOR_RESULTS:$DISABLE_DEALER_PASSWORD_CHANGE:$DISABLE_PDF:$DISABLE_PRINT:$DISABLE_SHARING:$DISABLE_MANAGER_UPLOAD:$SHOW_VRX_AS_VRN:$SHOW_VALUE_IN_LIST'

cat /opt/ecdata/config-template.yaml | envsubst "$VARS" > $WORKDIR/config.yaml

for DIR in backup backup/sql backup/xml dropped inbox outbox storage
do
    [ -d $WORKDIR/$DIR  ] || mkdir $WORKDIR/$DIR
done

/opt/ecdata/script/ecdata_server.pl
