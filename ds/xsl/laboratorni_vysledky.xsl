<?xml version="1.0" encoding="windows-1250"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">  
<xsl:output method="html" encoding="windows-1250" /> 

<xsl:strip-space elements="*"/>
<xsl:preserve-space elements="ptext"/>

<xsl:template match="/">
<html>
<head>
<title>MZDR</title>
<link href="styl.css" rel="STYLESHEET" type="text/css" /></head>
<body>
        <table border="0" cellpadding="2" cellspacing="2">
          <xsl:if test="dasta/garant_dat[.!='']">
             <tr><td class="ft" colspan="7">V�ker� uveden� data p�edal(garantuje):&#xA0;
                                                 <xsl:value-of select="dasta/garant_dat" />&#xA0;&#xA0; 
                                                 ID: <xsl:value-of select="dasta/garant_dat/@id_garant" />&#xA0;&#xA0; 
                                                 <xsl:if test="dasta/garant_dat/@odbornost[.!='']">odbornost: &#xA0; 
                                                    <xsl:value-of select="dasta/garant_dat/@odbornost" />
                                                 </xsl:if>

                 </td>
             </tr>
          </xsl:if> 
          <tr><td class="H1" colspan="7">Seznam pacient� v souboru <a><xsl:attribute name="name">Zpet</xsl:attribute></a></td></tr>
          <xsl:choose>
            <xsl:when test="count(dasta/is/ip) = 0">
               <tr><td class="pozn" colspan="7"><b>V souboru nejsou uvedeny ��dn� bloky s daty pacient�</b></td></tr>
            </xsl:when>    
            <xsl:otherwise>     
              <xsl:apply-templates select="dasta/is/ip/@id_pac"/>	
              <table border="0" cellpadding="2" cellspacing="2">
                <tr><td class="H1" colspan="7">&#xA0;&#xA0;<b>Laboratorn� v�sledky</b></td></tr>
                <tr> </tr>
                <tr> </tr>
                <xsl:apply-templates select="dasta/is/ip"/>
              </table>
            </xsl:otherwise>
         </xsl:choose>
	</table>
</body>
</html>
</xsl:template>




<xsl:template match="dasta/is/ip">  	

  <tr><td class="pac" colspan="7"><a><xsl:attribute name="name"><xsl:value-of select="@id_pac"/></xsl:attribute></a><b>Pacient :&#xA0;&#xA0;<xsl:value-of select="prijmeni" />&#xA0;<xsl:value-of select="jmeno" /></b>&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;<xsl:if test="rod_prijm[.!='']"><font class="pac_u">(rodn� p��jmen� &#xA0; - &#xA0; <xsl:value-of select="rod_prijm" />&#xA0;)</font></xsl:if></td></tr>
   <td colspan="7" >
    <table border="0" cellpadding="2" cellspacing="2">
        <tr><td class="pac_u" ><b>R�:</b>&#xA0;<xsl:if test="rodcis[.!='']"><xsl:value-of select="substring(rodcis,1,6)" />/<xsl:value-of select="substring(rodcis,7,4)" /></xsl:if></td>
	    <td class="pac_u" ><b>narozen(a):</b>&#xA0;<xsl:if test="dat_dn[.!='']"><xsl:value-of select="substring(dat_dn,9,2)" />.<xsl:value-of select="substring(dat_dn,6,2)" />.<xsl:value-of select="substring(dat_dn,1,4)" /></xsl:if></td>
	    <td class="pac_u" ><b>pohlav�:</b>&#xA0;
		<xsl:if test="sex[.='M']">mu�</xsl:if>
		<xsl:if test="sex[.='F']">�ena</xsl:if>
		<xsl:if test="sex[.='X']">neur�eno</xsl:if>
	    </td>
           <td class="pac_u" ><b>stav:</b>&#xA0;<xsl:value-of select="n/rodstav" /></td>
           <td class="pac_u" ><b>st�tn� p��slu�nost:</b>&#xA0;<xsl:value-of select="n/statpris" /></td>
        </tr>
        <tr><td class="pac_u" colspan="2"><b>zdravotn� poji��ovna:</b>&#xA0;<xsl:value-of select="p/kodpoj" /></td>
            <td class="pac_u" colspan="1"><b>poji�t�nec:</b>&#xA0;<xsl:value-of select="p/cispoj" /></td>
            <td class="pac_u" colspan="2"><b>poji�t�n(a) od:</b>&#xA0;<xsl:if test="p/dat_od[.!='']"><xsl:value-of select="substring(p/dat_od,9,2)" />.<xsl:value-of select="substring(p/dat_od,6,2)" />.<xsl:value-of select="substring(p/dat_od,1,4)" /></xsl:if></td>
        </tr>
        <tr><td class="pac_u" colspan="5"><b>v��ka / hmotnost :</b>&#xA0;&#xA0;
            <xsl:if test="h/@vyska[.!='']"><xsl:value-of select="h/@vyska" />&#xA0;cm</xsl:if>&#xA0;&#xA0;
            <xsl:if test="h/@hmotnost[.!='']"><xsl:value-of select="h/@hmotnost" />&#xA0;kg</xsl:if></td>
        </tr>
    </table>
  </td>
  <tr> </tr>
  <tr> </tr>
  <xsl:if test="count(v/vr) = 0"> 
    <tr><td class="pozn" colspan="7"><b>Pacient nem� uvedeny ��dn� laboratorn� v�sledky</b></td></tr>
  </xsl:if> 
  <xsl:if test="v/garant_dat[.!='']">
    <tr><td class="ft" colspan="7">Data uveden� v bloku Laboratorn� v�sledky p�edal(garantuje):&#xA0;
                                     <xsl:value-of select="v/garant_dat" />&#xA0;&#xA0; 
                                     ID: <xsl:value-of select="v/garant_dat/@id_garant" />&#xA0;&#xA0; 
                                     <xsl:if test="v/garant_dat/@odbornost[.!='']">odbornost: &#xA0; 
                                       <xsl:value-of select="v/garant_dat/@odbornost" />
                                     </xsl:if>
         </td>
    </tr>
  </xsl:if> 
  <xsl:if test="v/vr/@typpol_fh[.='F' or .='0' or .='1' or .='2' or .='3' or .='4' or .='5' or .='6' or .='T']" >
    <tr><td class="lab" colspan="7">&#xA0;<b>Formalizovan� laboratorn� v�sledky</b></td></tr>
    <tr><td class="head" colspan="1">&#xA0; <b>N�LP</b></td>
        <td class="head" colspan="1">&#xA0;<b>n�zev</b></td>
        <td class="head" colspan="1">&#xA0;<b>hodnota (nejistota)</b></td>
        <td class="head" colspan="1">&#xA0;<b>jednot.</b></td>
        <td class="head" colspan="1">&#xA0;<b>ref.rozmez�</b></td>
        <td class="head" colspan="1">&#xA0;<b>stav</b></td>
       <td class="head" colspan="1">&#xA0;<b>sd�len�</b></td>
    </tr>
    <tr><td class="pozn" colspan="1">&#xA0;lab. vzorek</td>
        <td class="pozn" colspan="2">&#xA0;datum a �as ud�losti (odb�ru)</td>
        <td class="pozn" colspan="2">&#xA0;datum a �as p�ijet� vzorku</td>
        <td class="pozn" colspan="2">&#xA0;datum a �as vyd�n� v�sledku</td>
    </tr>
    <xsl:if test="v/vr/@typpol_fh[.='0' or .='1' or .='2' or .='3' or .='4' or .='5' or .='6' or .='T' ]" > 
      <xsl:apply-templates select="v/vr/vrn"/>
    </xsl:if>
    <xsl:if test="v/vr/@typpol_fh[.='F']" > 
      <xsl:apply-templates select="v/vr/vrf"/>
    </xsl:if>
  </xsl:if>
  <xsl:if test="v/vr/@typpol_fh[.='X']" >
    <tr> </tr>
    <tr> </tr>
    <tr><td class="lab" colspan="7">&#xA0;<b>Neformalizovan� laboratorn� v�sledky</b></td></tr>
    <tr><td class="head" colspan="1">&#xA0;<b>N�LP</b></td>
        <td class="head" colspan="1">&#xA0;<b>n�zev</b></td>
        <td class="head" colspan="3">&#xA0;<b>hodnota</b></td>
        <td class="head" colspan="1">&#xA0;<b>stav</b></td>
        <td class="head" colspan="1">&#xA0;<b>sd�len�</b></td>
    </tr>
    <tr><td class="pozn" colspan="1">&#xA0;lab. vzorek</td>
        <td class="pozn" colspan="2">&#xA0;datum a �as ud�losti (odb�ru)</td>
        <td class="pozn" colspan="2">&#xA0;datum a �as p�ijet� vzorku</td>
        <td class="pozn" colspan="2">&#xA0;datum a �as vyd�n� v�sledku</td>
    </tr>
    <xsl:apply-templates select="v/vr/vrx"/>
  </xsl:if>
  <xsl:if test="v/vr/@typpol_fh[.='K']" >
    <tr><td class="lab" colspan="7">&#xA0;<b>Mikrobiologick� laboratorn� v�sledky</b></td></tr>
    <tr><td class="head" colspan="1">&#xA0;<b>N�LP</b></td>
        <td class="head" colspan="4">&#xA0;<b>n�zev</b></td>
        <td class="head" colspan="1">&#xA0;<b>stav</b></td>
        <td class="head" colspan="1">&#xA0;<b>sd�len�</b></td>
    </tr>
    <tr><td class="pozn" colspan="1">&#xA0;lab. vzorek</td>
        <td class="pozn" colspan="2">&#xA0;datum a �as ud�losti (odb�ru)</td>
        <td class="pozn" colspan="2">&#xA0;datum a �as p�ijet� vzorku</td>
        <td class="pozn" colspan="2">&#xA0;datum a �as vyd�n� v�sledku</td>
    </tr>
    <xsl:apply-templates select="v/vr/vrk"/>
  </xsl:if>
  <xsl:if test="v/vr/@typpol_fh[.='B']" >
    <tr><td class="lab" colspan="7">&#xA0;<b>Interpretace</b></td></tr>
    <xsl:apply-templates select="v/vr/vrb"/>
  </xsl:if>
  <xsl:if test="v/vr/@typpol_fh[.='F' or .='0' or .='1' or .='2' or .='3' or .='4' or .='5' or .='6' or .='T' or .='K' or .='B' or .='X' ]" > 
    <tr><td class="pozn_n" colspan="7">&#xA0;<b>Dopl�uj�c� informace k v�sledk�m:</b></td></tr>
    <xsl:if test="count(v/vr/@urg_info) > 0">
      <xsl:if test="v/vr/@urg_info[.='E' or .='R' or .='Z' or .='P']"> 
        <tr><td class="pozn" colspan="1">&#xA0;<b>hodnoty:</b></td>
            <td class="pozn" colspan="6">&#xA0;
                <font class="flag_e_v"><b>extr�mn� (E),</b></font>&#xA0;&#xA0;&#xA0;
                <font class="flag_r_v"><b>extr�mn� zm�na (R),</b></font>&#xA0;&#xA0;&#xA0;
                <font class="flag_z_v"><b>extr�mn� a zm�na (Z),</b></font>&#xA0;&#xA0;&#xA0;
                <font class="flag_p_v"><b>pozoruhodn� (P)</b></font>
            </td>
        </tr>
      </xsl:if>
    </xsl:if>
    <tr><td class="pozn" colspan="1">&#xA0;<b>stav:</b></td>
        <td class="pozn" colspan="6">&#xA0;
          rozpracovan� (R),&#xA0;&#xA0;&#xA0;neautorizovan� (N),&#xA0;&#xA0;&#xA0;
          autorizovan� (A),&#xA0;&#xA0;&#xA0;autorizovan� s drobnou koliz� (D),&#xA0;&#xA0;&#xA0;
          autorizovan� se z�sadn� koliz� (D),&#xA0;&#xA0;&#xA0;autorizovan� s nerozli�enou koliz� (K),&#xA0;&#xA0;&#xA0;
          nebude zpracov�no (E)
        </td>
    </tr>
    <tr><td class="pozn" colspan="1">&#xA0;<b>sd�len�:</b></td>
        <td class="pozn" colspan="6">&#xA0;
          nov� dodan� laborato�� (N),&#xA0;&#xA0;&#xA0;arch�vn� z datab�ze laborato�e (A),&#xA0;&#xA0;&#xA0;
          arch�vn� z datab�ze u�ivatele (D),&#xA0;&#xA0;&#xA0;p�ed�van� laborato�i subdodavatelem (L),&#xA0;&#xA0;&#xA0;
          vr�cen� objednateli (V)
        </td>
    </tr>       
  </xsl:if>
  <xsl:if test="v/vr/@typpol_fh[.='R']" >
    <tr><td class="lab" colspan="7">&#xA0;<b>Laboratorn� v�sledky ve form�tu tiskov�ch sestav</b></td></tr>
    <xsl:apply-templates select="v/vr[@typpol_fh='R']"/>
  </xsl:if>
  <tr> </tr>
  <tr> </tr>
  <tr><td class="odkaz" colspan="7">&#xA0;<a><xsl:attribute name="href">#Zpet</xsl:attribute>Zp�t na seznam pacient�</a></td></tr>
  <tr> </tr>
  <tr> </tr>
  <tr> </tr>
  <tr> </tr>
  <tr> </tr>
  <tr> </tr>
  <tr> </tr>																																							
</xsl:template> 


<xsl:template match="dasta/is/ip/@id_pac"> 
	<tr><td class="head" colspan="7"><b>Pacient:</b></td></tr>
	<tr><td class="text"><a><xsl:attribute name="href">#<xsl:value-of select="."/></xsl:attribute><xsl:value-of select="../jmeno" />&#xA0;<xsl:value-of select="../prijmeni" /></a></td>
            <td class="text"><b>R�:</b>&#xA0;<xsl:if test="../rodcis[.!='']"><xsl:value-of select="substring(../rodcis,1,6)" />/<xsl:value-of select="substring(../rodcis,7,4)" /></xsl:if></td>
		<td class="text"><b>narozen(a):</b>&#xA0;<xsl:value-of select="substring(../dat_dn,9,2)" />.<xsl:value-of select="substring(../dat_dn,6,2)" />.<xsl:value-of select="substring(../dat_dn,1,4)" /></td>
		<td class="text"><b>pohlav�:</b>&#xA0;
			<xsl:if test="../sex[.='M']">mu�</xsl:if>
			<xsl:if test="../sex[.='F']">�ena</xsl:if>
			<xsl:if test="../sex[.='X']">neur�eno</xsl:if>
		</td>
	</tr>
</xsl:template> 


<xsl:template match="v/vr/vrn">
  <tr>
   <td class="text" colspan="1" ><b>&#xA0;<xsl:value-of select="../@klic_nclp" /></b></td>
   <td class="text" colspan="1" ><b>&#xA0;<xsl:value-of select="../nazev_lclp" /></b></td>
   <td class="text" colspan="1" >
      <xsl:choose>
       <xsl:when test="../@typpol_fh[.='T']">
         <xsl:choose>
           <xsl:when test="../@urg_info[.='E']">
             <font class="flag_e"><b>&#xA0;&#xA0;1:<xsl:value-of select="hodnota" /></b></font>
           </xsl:when>
           <xsl:when test="../@urg_info[.='R']">
             <font class="flag_r"><b>&#xA0;&#xA0;1:<xsl:value-of select="hodnota" /></b></font>
           </xsl:when>
           <xsl:when test="../@urg_info[.='Z']">
             <font class="flag_e"><b>&#xA0;&#xA0;1:<xsl:value-of select="hodnota" /></b></font>
           </xsl:when>
           <xsl:when test="../@urg_info[.='P']">
             <font class="flag_e"><b>&#xA0;&#xA0;1:<xsl:value-of select="hodnota" /></b></font>
           </xsl:when>
           <xsl:otherwise>
             <b>&#xA0;&#xA0;1:<xsl:value-of select="hodnota" /></b>
           </xsl:otherwise>
         </xsl:choose>
       </xsl:when>
       <xsl:otherwise>
         <xsl:choose>
           <xsl:when test="../@urg_info[.='E']">
             <font class="flag_e">   
               <xsl:if test="@priznak_kvant[.='R']">
               <b>&#xA0;&#xA0;&#xA0;<xsl:value-of select="hodnota" /><xsl:if test="nejistota/nejist_var1/@h_nejist[.!='']"> &#xA0;&#xA0;( +- &#xA0;<xsl:value-of select="nejistota/nejist_var1/@h_nejist" />)</xsl:if></b>
               </xsl:if>
               <xsl:if test="@priznak_kvant[.='M']">
               <b>&#xA0;&#xA0;&#xA0;<xsl:value-of select="hodnota" /><xsl:if test="nejistota/nejist_var1/@h_nejist[.!='']"> &#xA0;&#xA0;( +- &#xA0;<xsl:value-of select="nejistota/nejist_var1/@h_nejist" />)</xsl:if></b>
               </xsl:if>
               <xsl:if test="@priznak_kvant[.='V']">
               <b>&#xA0;&#xA0;&#xA0;<xsl:value-of select="hodnota" /><xsl:if test="nejistota/nejist_var1/@h_nejist[.!='']"> &#xA0;&#xA0;( +- &#xA0;<xsl:value-of select="nejistota/nejist_var1/@h_nejist" />)</xsl:if></b>
               </xsl:if>
             </font>
           </xsl:when>
           <xsl:when test="../@urg_info[.='R']">
             <font class="flag_r">   
               <xsl:if test="@priznak_kvant[.='R']">
               <b>&#xA0;&#xA0;&#xA0;<xsl:value-of select="hodnota" /><xsl:if test="nejistota/nejist_var1/@h_nejist[.!='']"> &#xA0;&#xA0;( +- &#xA0;<xsl:value-of select="nejistota/nejist_var1/@h_nejist" />)</xsl:if> </b>
               </xsl:if>
               <xsl:if test="@priznak_kvant[.='M']">
               <b>&#xA0;&#xA0;&#xA0;<xsl:value-of select="hodnota" /><xsl:if test="nejistota/nejist_var1/@h_nejist[.!='']"> &#xA0;&#xA0;( +- &#xA0;<xsl:value-of select="nejistota/nejist_var1/@h_nejist" />)</xsl:if></b>
               </xsl:if>
               <xsl:if test="@priznak_kvant[.='V']">
               <b>&#xA0;&#xA0;&#xA0;<xsl:value-of select="hodnota" /><xsl:if test="nejistota/nejist_var1/@h_nejist[.!='']"> &#xA0;&#xA0;( +- &#xA0;<xsl:value-of select="nejistota/nejist_var1/@h_nejist" />)</xsl:if></b>
               </xsl:if>
             </font>
           </xsl:when>
           <xsl:when test="../@urg_info[.='Z']">
             <font class="flag_z">   
               <xsl:if test="@priznak_kvant[.='R']">
               <b>&#xA0;&#xA0;&#xA0;<xsl:value-of select="hodnota" /><xsl:if test="nejistota/nejist_var1/@h_nejist[.!='']"> &#xA0;&#xA0;( +- &#xA0;<xsl:value-of select="nejistota/nejist_var1/@h_nejist" />)</xsl:if></b>
               </xsl:if>
               <xsl:if test="@priznak_kvant[.='M']">
               <b>&#xA0;&#xA0;&#xA0;<xsl:value-of select="hodnota" /><xsl:if test="nejistota/nejist_var1/@h_nejist[.!='']"> &#xA0;&#xA0;( +- &#xA0;<xsl:value-of select="nejistota/nejist_var1/@h_nejist" />)</xsl:if></b>
               </xsl:if>
               <xsl:if test="@priznak_kvant[.='V']">
               <b>&#xA0;&#xA0;&#xA0;<xsl:value-of select="hodnota" /><xsl:if test="nejistota/nejist_var1/@h_nejist[.!='']"> &#xA0;&#xA0;( +- &#xA0;<xsl:value-of select="nejistota/nejist_var1/@h_nejist" />)</xsl:if></b>
               </xsl:if>
             </font>
           </xsl:when>
           <xsl:when test="../@urg_info[.='P']">
             <font class="flag_p">   
               <xsl:if test="@priznak_kvant[.='R']">
               <b>&#xA0;&#xA0;&#xA0;<xsl:value-of select="hodnota" /><xsl:if test="nejistota/nejist_var1/@h_nejist[.!='']"> &#xA0;&#xA0;( +- &#xA0;<xsl:value-of select="nejistota/nejist_var1/@h_nejist" />)</xsl:if></b>
               </xsl:if>
               <xsl:if test="@priznak_kvant[.='M']">
               <b>&#xA0;&#xA0;&#xA0;<xsl:value-of select="hodnota" /><xsl:if test="nejistota/nejist_var1/@h_nejist[.!='']"> &#xA0;&#xA0;( +- &#xA0;<xsl:value-of select="nejistota/nejist_var1/@h_nejist" />)</xsl:if></b>
               </xsl:if>
               <xsl:if test="@priznak_kvant[.='V']">
               <b>&#xA0;&#xA0;&#xA0;<xsl:value-of select="hodnota" /><xsl:if test="nejistota/nejist_var1/@h_nejist[.!='']"> &#xA0;&#xA0;( +- &#xA0;<xsl:value-of select="nejistota/nejist_var1/@h_nejist" />)</xsl:if></b>
               </xsl:if>
             </font>
           </xsl:when>
           <xsl:otherwise>
             <xsl:if test="@priznak_kvant[.='R']">
               <b>&#xA0;&#xA0;&#xA0;<xsl:value-of select="hodnota" /><xsl:if test="nejistota/nejist_var1/@h_nejist[.!='']"> &#xA0;&#xA0;( +- &#xA0;<xsl:value-of select="nejistota/nejist_var1/@h_nejist" />)</xsl:if></b>
             </xsl:if>
             <xsl:if test="@priznak_kvant[.='M']">
               <b>&#xA0;&#xA0;&#xA0;<xsl:value-of select="hodnota" /><xsl:if test="nejistota/nejist_var1/@h_nejist[.!='']"> &#xA0;&#xA0;( +- &#xA0;<xsl:value-of select="nejistota/nejist_var1/@h_nejist" />)</xsl:if></b>
             </xsl:if>
             <xsl:if test="@priznak_kvant[.='V']">
               <b>&#xA0;&#xA0;&#xA0;<xsl:value-of select="hodnota" /><xsl:if test="nejistota/nejist_var1/@h_nejist[.!='']"> &#xA0;&#xA0;( +- &#xA0;<xsl:value-of select="nejistota/nejist_var1/@h_nejist" />)</xsl:if></b>
             </xsl:if>
           </xsl:otherwise> 
         </xsl:choose>
       </xsl:otherwise>
     </xsl:choose>
   </td>
   <td class="text" colspan="1" ><b>&#xA0;<xsl:value-of select="../polozka_nclp/@njedn" /></b></td>
   <td class="text" colspan="1">&#xA0;<xsl:if test="skala/s4[.!='']">(<xsl:value-of select="skala/s4" />-<xsl:value-of select="skala/s5" />)</xsl:if></td>
   <td class="text" colspan="1" >&#xA0;<xsl:value-of select="../@stav_vys" /></td>
   <td class="text" colspan="1" >&#xA0;<xsl:value-of select="../@typ_sdel_vys" /></td>
  </tr>
  <tr><td class="pozn" colspan="1">&#xA0;<xsl:value-of select="../@id_lis" /></td>
      <td class="pozn" colspan="2">&#xA0;<xsl:if test="../dat_du[.!='']">
                                            <xsl:value-of select="substring(../dat_du,9,2)" />.
                                            <xsl:value-of select="substring(../dat_du,6,2)" />.
                                            <xsl:value-of select="substring(../dat_du,1,4)" />&#xA0;&#xA0;
                                            <xsl:value-of select="substring(../dat_du,12,2)" />:
                                            <xsl:value-of select="substring(../dat_du,15,2)" />:
                                            <xsl:value-of select="substring(../dat_du,18,2)" />
                                          </xsl:if></td>
           
      <td class="pozn" colspan="2">&#xA0;<xsl:if test="../dat_pl[.!='']">
                                            <xsl:value-of select="substring(../dat_pl,9,2)" />.
                                            <xsl:value-of select="substring(../dat_pl,6,2)" />.
                                            <xsl:value-of select="substring(../dat_pl,1,4)" />&#xA0;&#xA0;
                                            <xsl:value-of select="substring(../dat_pl,12,2)" />:
                                            <xsl:value-of select="substring(../dat_pl,15,2)" />
                                          </xsl:if></td>
      <td class="pozn" colspan="2">&#xA0;<xsl:if test="../dat_vv[.!='']">
                                            <xsl:value-of select="substring(../dat_vv,9,2)" />.
                                            <xsl:value-of select="substring(../dat_vv,6,2)" />.
                                            <xsl:value-of select="substring(../dat_vv,1,4)" />&#xA0;&#xA0;
                                            <xsl:value-of select="substring(../dat_vv,12,2)" />:
                                            <xsl:value-of select="substring(../dat_vv,15,2)" />
                                          </xsl:if></td>
  </tr>
  <xsl:choose>
    <xsl:when test="../autor">
      <tr><td class="pozn" colspan="3">&#xA0;autorizoval(a): &#xA0;<xsl:value-of select="../autor" /></td>
            <td class="pozn" colspan="4">&#xA0;odeslal(a): &#xA0; <xsl:value-of select="../odeslal" /></td>
      </tr>
    </xsl:when>
   <xsl:otherwise>
       <xsl:if test="../odeslal[.!='']">
         <tr><td class="pozn" colspan="3">&#xA0;autorizoval(a): &#xA0;<xsl:value-of select="../autor" /></td>
               <td class="pozn" colspan="4">&#xA0;odeslal(a): &#xA0; <xsl:value-of select="../odeslal" /></td>
         </tr>
       </xsl:if>
   </xsl:otherwise>
 </xsl:choose>

    <xsl:if test="../sci/@id_sci_is[.!='']">
        <tr><td class="ft" colspan="7"><b>Hodnota p��slu�� k funk�n�mu testu</b></td></tr>
        <tr><td class="ft" colspan="2">&#xA0;<b>n�zev:</b>&#xA0;<xsl:value-of select="../sci/polozka_nclp/@nkomp" /></td>
               <td class="ft" colspan="2">&#xA0;<b>identifikace testu:</b>&#xA0;<xsl:value-of select="../sci/@id_sci_is"/></td>
               <td class="ft" colspan="1">&#xA0;<b>krok :</b>&#xA0;<xsl:value-of select="../sci/@krok" /></td>
              <td class="ft" colspan="2">&#xA0;<b>pr�b�h :</b>&#xA0;
                                <xsl:if test="../sci/@prubeh[.='Z']">za��tek</xsl:if>
		<xsl:if test="../sci/@prubeh[.='P']">pr�b�h</xsl:if>		
		<xsl:if test="../sci/@prubeh[.='K']">konec</xsl:if>	
              </td>
          </tr>
    </xsl:if>


</xsl:template>


<xsl:template match="v/vr/vrf">

  <tr>
   <td class="text" colspan="1" ><b>&#xA0;<xsl:value-of select="../@klic_nclp" /></b></td>
   <td class="text" colspan="1" ><b>&#xA0;<xsl:value-of select="../nazev_lclp" /></b></td>
   <td class="text" colspan="1" ><b>&#xA0;&#xA0;&#xA0;<xsl:value-of select="hodnota_kod" /></b></td>
   <td class="text" colspan="1" ><b>&#xA0;<xsl:value-of select="../polozka_nclp/@njedn" /></b></td>
   <td class="text" colspan="1">&#xA0;</td>
   <td class="text" colspan="1" >&#xA0;<xsl:value-of select="../@stav_vys" /></td>
   <td class="text" colspan="1" >&#xA0;<xsl:value-of select="../@typ_sdel_vys" /></td>
  </tr>
  <tr><td class="pozn" colspan="1">&#xA0;<xsl:value-of select="../@id_lis" /></td>
      <td class="pozn" colspan="2">&#xA0;<xsl:if test="../dat_du[.!='']">
                                            <xsl:value-of select="substring(../dat_du,9,2)" />.
                                            <xsl:value-of select="substring(../dat_du,6,2)" />.
                                            <xsl:value-of select="substring(../dat_du,1,4)" />&#xA0;&#xA0;
                                            <xsl:value-of select="substring(../dat_du,12,2)" />:
                                            <xsl:value-of select="substring(../dat_du,15,2)" />:
                                            <xsl:value-of select="substring(../dat_du,18,2)" />
                                          </xsl:if></td>
           
      <td class="pozn" colspan="2">&#xA0;<xsl:if test="../dat_pl[.!='']">
                                            <xsl:value-of select="substring(../dat_pl,9,2)" />.
                                            <xsl:value-of select="substring(../dat_pl,6,2)" />.
                                            <xsl:value-of select="substring(../dat_pl,1,4)" />&#xA0;&#xA0;
                                            <xsl:value-of select="substring(../dat_pl,12,2)" />:
                                            <xsl:value-of select="substring(../dat_pl,15,2)" />
                                          </xsl:if></td>
      <td class="pozn" colspan="2">&#xA0;<xsl:if test="../dat_vv[.!='']">
                                            <xsl:value-of select="substring(../dat_vv,9,2)" />.
                                            <xsl:value-of select="substring(../dat_vv,6,2)" />.
                                            <xsl:value-of select="substring(../dat_vv,1,4)" />&#xA0;&#xA0;
                                            <xsl:value-of select="substring(../dat_vv,12,2)" />:
                                            <xsl:value-of select="substring(../dat_vv,15,2)" />
                                          </xsl:if></td>
  </tr>	
</xsl:template>

<xsl:template match="v/vr/vrb">
  <tr><td class="head" colspan="1">&#xA0;<b>N�LP</b></td>
      <td class="head" colspan="4">&#xA0;<b>n�zev</b></td>
      <td class="head" colspan="1">&#xA0;<b>stav</b></td>
      <td class="head" colspan="1">&#xA0;<b>sd�len�</b></td>
  </tr>
  <tr><td class="pozn" colspan="1">&#xA0;lab. vzorek</td>
      <td class="pozn" colspan="2">&#xA0;datum a �as ud�losti (odb�ru)</td>
      <td class="pozn" colspan="2">&#xA0;datum a �as p�ijet� vzorku</td>
      <td class="pozn" colspan="2">&#xA0;datum a �as vyd�n� v�sledku</td>
  </tr>
  <tr>
   <td class="text" colspan="1" ><b>&#xA0;<xsl:value-of select="../@klic_nclp" /></b></td>
   <td class="text" colspan="4" ><b>&#xA0;<xsl:value-of select="../nazev_lclp" /></b></td>
   <td class="text" colspan="1" >&#xA0;<xsl:value-of select="../@stav_vys" /></td>
   <td class="text" colspan="1" >&#xA0;<xsl:value-of select="../@typ_sdel_vys" /></td>

  </tr>
  <tr><td class="pozn" colspan="1">&#xA0;<xsl:value-of select="../@id_lis" /></td>
      <td class="pozn" colspan="2"><xsl:if test="../dat_du[.!='']">
                                            <xsl:value-of select="substring(../dat_du,9,2)" />.
                                            <xsl:value-of select="substring(../dat_du,6,2)" />.
                                            <xsl:value-of select="substring(../dat_du,1,4)" />&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;
                                            <xsl:value-of select="substring(../dat_du,12,2)" />:
                                            <xsl:value-of select="substring(../dat_du,15,2)" />:
                                            <xsl:value-of select="substring(../dat_du,18,2)" />
                                          </xsl:if></td>
           
      <td class="pozn" colspan="2"><xsl:if test="../dat_pl[.!='']">
                                            <xsl:value-of select="substring(../dat_pl,9,2)" />.
                                            <xsl:value-of select="substring(../dat_pl,6,2)" />.
                                            <xsl:value-of select="substring(../dat_pl,1,4)" />&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;
                                            <xsl:value-of select="substring(../dat_pl,12,2)" />:
                                            <xsl:value-of select="substring(../dat_pl,15,2)" />
                                          </xsl:if></td>
      <td class="pozn" colspan="2">&#xA0;<xsl:if test="../dat_vv[.!='']">
                                            <xsl:value-of select="substring(../dat_vv,9,2)" />.
                                            <xsl:value-of select="substring(../dat_vv,6,2)" />.
                                            <xsl:value-of select="substring(../dat_vv,1,4)" />&#xA0;&#xA0;
                                            <xsl:value-of select="substring(../dat_vv,12,2)" />:
                                            <xsl:value-of select="substring(../dat_vv,15,2)" />
                                          </xsl:if></td>
  </tr>
  <tr><td class="head" colspan="1">&#xA0;<b>typ vzniku</b></td>
      <td class="head" colspan="2">&#xA0;<b>m�sto vzniku</b></td>
      <td class="head" colspan="2">&#xA0;<b>autorizace</b></td>
      <td class="head" colspan="2">&#xA0;<b>autor</b></td>
  </tr>
  <tr><td class="text" colspan="1">&#xA0;<xsl:if test="@typ_vzniku[.='N']">neuvedeno</xsl:if> 
                                         <xsl:if test="@typ_vzniku[.='A']">automaticky</xsl:if> 
                                         <xsl:if test="@typ_vzniku[.='R']">ru�n�</xsl:if> 
                                         <xsl:if test="@typ_vzniku[.='O']">automaticky i ru�n�</xsl:if> 
      </td>
      <td class="text" colspan="2">&#xA0;<xsl:if test="@misto_vzniku[.='L']">v r�mci LIS</xsl:if> 
                                         <xsl:if test="@misto_vzniku[.='A']">mimo LIS laborato�e a mimo IS p��jemce</xsl:if> 
                                         <xsl:if test="@misto_vzniku[.='P']">ru�n�</xsl:if> 
      </td>
      <td class="text" colspan="2">&#xA0;<xsl:if test="@autorizace[.='A']">autorizov�no</xsl:if> 
                                         <xsl:if test="@autorizace[.='N']">neautorizov�no</xsl:if> 
      </td>
      <td class="text" colspan="2">&#xA0;
        <xsl:choose>
          <xsl:when test="autor[.!='']">
            <xsl:value-of select="autor" />
          </xsl:when>
          <xsl:otherwise>
            <i>neuveden</i>
          </xsl:otherwise>
        </xsl:choose>
      </td>
  </tr>
  <tr><td class="head" colspan="7"><b>Text interpretace</b></td></tr>
  <tr><td class="text" colspan="7"><PRE><xsl:value-of select="text/ptext"/></PRE></td></tr>
</xsl:template>


<xsl:template match="v/vr">  	
    <xsl:if test="@typpol_fh='R'">
          <tr><td class="head" colspan="7">&#xA0;<b>Typ v�sledkov� sestavy:</b>&#xA0;&#xA0;&#xA0;
              <xsl:choose>
                <xsl:when test="@klic_nclp[.='20002' or .='20003' or .='20019' or .='20020'  or .='20472' or .='20473'  or .='20478' or .='20479'  or .='20480' or .='20481'  or .='20489' or .='20490']" >   
                   <font class="flag_h"><b>&#xA0;<xsl:value-of select="polozka_nclp/@nkomp" /></b></font>
                </xsl:when>
                <xsl:otherwise>
                     <b>&#xA0;<xsl:value-of select="polozka_nclp/@nkomp" /></b>
               </xsl:otherwise>
              </xsl:choose>	
          </td>
        </tr>
        <tr><td class="text" colspan="7"><PRE><xsl:value-of select="vrr/text/ptext"/></PRE></td></tr>
        <tr><td class="pozn_n" colspan="7"><b>Dopl�uj�c� informace k sestav�:</b></td></tr>
        <tr><td class="pozn" colspan="3"><b>Datum a �as ud�losti:</b></td>
            <td class="pozn" colspan="4">&#xA0;<xsl:value-of select="substring(dat_du,9,2)" />.
                                   <xsl:value-of select="substring(dat_du,6,2)" />.
                                   <xsl:value-of select="substring(dat_du,1,4)" />&#xA0;
                                   <xsl:value-of select="substring(dat_du,12,8)" />
            </td></tr>
        <tr><td class="pozn" colspan="3"><b>Identifikace vzorku v laborato�i:</b></td>
            <td class="pozn" colspan="4">&#xA0;<xsl:value-of select="@id_lis" /></td>
        </tr>
        <tr><td class="pozn" colspan="3"><b>Stav v�sledku:</b></td>
            <td class="pozn" colspan="4">&#xA0;<xsl:if test="@stav_vys[.='N']">neautorizovan�</xsl:if>
                                   <xsl:if test="@stav_vys[.='A']">autorizovan�</xsl:if>
                                   <xsl:if test="@stav_vys[.='D']">autorizovan� <font class="flag_1">s drobnou koliz�</font></xsl:if>
                                   <xsl:if test="@stav_vys[.='Z']">autorizovan� <font class="flag_1">se z�sadn� koliz�</font></xsl:if>
                                   <xsl:if test="@stav_vys[.='K']">autorizovan� <font class="flag_1">se z�sadn� koliz�</font></xsl:if>
            </td></tr>
        <tr><td class="pozn" colspan="3"><b>Typ sd�len� v�sledku:</b></td>
            <td class="pozn" colspan="4">&#xA0;<xsl:if test="@typ_sdel_vys[.='N']">nov� v�sledek dodan� laborato��</xsl:if>
                                   <xsl:if test="@typ_sdel_vys[.='A']">archivn� v�sledek z datab�ze laborato�e</xsl:if>
                                   <xsl:if test="@typ_sdel_vys[.='D']">archivn� v�sledek z datab�ze u�ivatele</xsl:if>
            </td>
        </tr>
    </xsl:if>
       <tr> </tr>
       <tr> </tr>
</xsl:template> 

<xsl:template match="v/vr/vrx">
  <tr>
   <td class="text" colspan="1" ><b>&#xA0;<xsl:value-of select="../@klic_nclp" /></b></td>
   <td class="text" colspan="1" ><b>&#xA0;<xsl:value-of select="../nazev_lclp" /></b></td>
   <td class="text" colspan="3" >
         <xsl:choose>
           <xsl:when test="../@urg_info[.='E']">
             <font class="flag_e"><b>&#xA0;&#xA0;<xsl:value-of select="hodnota_nt" /></b></font>
           </xsl:when>
           <xsl:when test="../@urg_info[.='R']">
             <font class="flag_r"><b>&#xA0;&#xA0;<xsl:value-of select="hodnota_nt" /></b></font>
           </xsl:when>
           <xsl:when test="../@urg_info[.='Z']">
             <font class="flag_e"><b>&#xA0;&#xA0;<xsl:value-of select="hodnota_nt" /></b></font>
           </xsl:when>
           <xsl:when test="../@urg_info[.='P']">
             <font class="flag_e"><b>&#xA0;&#xA0;<xsl:value-of select="hodnota_nt" /></b></font>
           </xsl:when>
           <xsl:otherwise>
             <b>&#xA0;&#xA0;1:<xsl:value-of select="hodnota_nt" /></b>
           </xsl:otherwise>
         </xsl:choose>
   </td>
   <td class="text" colspan="1" >&#xA0;<xsl:value-of select="../@stav_vys" /></td>
   <td class="text" colspan="1" >&#xA0;<xsl:value-of select="../@typ_sdel_vys" /></td>
  </tr>
  <tr><td class="pozn" colspan="1">&#xA0;<xsl:value-of select="../@id_lis" /></td>
      <td class="pozn" colspan="2">&#xA0;<xsl:if test="../dat_du[.!='']">
                                            <xsl:value-of select="substring(../dat_du,9,2)" />.
                                            <xsl:value-of select="substring(../dat_du,6,2)" />.
                                            <xsl:value-of select="substring(../dat_du,1,4)" />&#xA0;&#xA0;
                                            <xsl:value-of select="substring(../dat_du,12,2)" />:
                                            <xsl:value-of select="substring(../dat_du,15,2)" />:
                                            <xsl:value-of select="substring(../dat_du,18,2)" />
                                          </xsl:if></td>
           
      <td class="pozn" colspan="2">&#xA0;<xsl:if test="../dat_pl[.!='']">
                                            <xsl:value-of select="substring(../dat_pl,9,2)" />.
                                            <xsl:value-of select="substring(../dat_pl,6,2)" />.
                                            <xsl:value-of select="substring(../dat_pl,1,4)" />&#xA0;&#xA0;
                                            <xsl:value-of select="substring(../dat_pl,12,2)" />:
                                            <xsl:value-of select="substring(../dat_pl,15,2)" />
                                          </xsl:if></td>
      <td class="pozn" colspan="2">&#xA0;<xsl:if test="../dat_vv[.!='']">
                                            <xsl:value-of select="substring(../dat_vv,9,2)" />.
                                            <xsl:value-of select="substring(../dat_vv,6,2)" />.
                                            <xsl:value-of select="substring(../dat_vv,1,4)" />&#xA0;&#xA0;
                                            <xsl:value-of select="substring(../dat_vv,12,2)" />:
                                            <xsl:value-of select="substring(../dat_vv,15,2)" />
                                          </xsl:if></td>
  </tr>	
</xsl:template>

<xsl:template match="v/vr/vrk">
  <tr>
   <td class="text" colspan="1" ><b>&#xA0;<xsl:value-of select="../@klic_nclp" /></b></td>
   <td class="text" colspan="4" ><b>&#xA0;<xsl:value-of select="../nazev_lclp" /></b></td>
   <td class="text" colspan="1" >&#xA0;<xsl:value-of select="../@stav_vys" /></td>
   <td class="text" colspan="1" >&#xA0;<xsl:value-of select="../@typ_sdel_vys" /></td>
  </tr>
  <tr><td class="pozn" colspan="1">&#xA0;<xsl:value-of select="../@id_lis" /></td>
      <td class="pozn" colspan="2">&#xA0;<xsl:if test="../dat_du[.!='']">
                                            <xsl:value-of select="substring(../dat_du,9,2)" />.
                                            <xsl:value-of select="substring(../dat_du,6,2)" />.
                                            <xsl:value-of select="substring(../dat_du,1,4)" />&#xA0;&#xA0;
                                            <xsl:value-of select="substring(../dat_du,12,2)" />:
                                            <xsl:value-of select="substring(../dat_du,15,2)" />:
                                            <xsl:value-of select="substring(../dat_du,18,2)" />
                                          </xsl:if></td>
           
      <td class="pozn" colspan="2">&#xA0;<xsl:if test="../dat_pl[.!='']">
                                            <xsl:value-of select="substring(../dat_pl,9,2)" />.
                                            <xsl:value-of select="substring(../dat_pl,6,2)" />.
                                            <xsl:value-of select="substring(../dat_pl,1,4)" />&#xA0;&#xA0;
                                            <xsl:value-of select="substring(../dat_pl,12,2)" />:
                                            <xsl:value-of select="substring(../dat_pl,15,2)" />
                                          </xsl:if></td>
      <td class="pozn" colspan="2">&#xA0;<xsl:if test="../dat_vv[.!='']">
                                            <xsl:value-of select="substring(../dat_vv,9,2)" />.
                                            <xsl:value-of select="substring(../dat_vv,6,2)" />.
                                            <xsl:value-of select="substring(../dat_vv,1,4)" />&#xA0;&#xA0;
                                            <xsl:value-of select="substring(../dat_vv,12,2)" />:
                                            <xsl:value-of select="substring(../dat_vv,15,2)" />
                                          </xsl:if></td>
  </tr>
  <xsl:if test="vrmmn[.!='']">
    <tr><td class="head" colspan="7">&#xA0;<b>Mikroskopick� n�lez</b></td></tr>
    <tr><td class="text" colspan="7">&#xA0;<xsl:value-of select="vrmmn"/></td></tr>
  </xsl:if>
  <xsl:if test="vrmahn[.!='']">
    <tr><td class="head" colspan="7">&#xA0;<b>Automatick� hodnocen� kompletn�ho n�lezu</b></td></tr>
    <tr><td class="text" colspan="7">&#xA0;<xsl:value-of select="vrmahn"/></td></tr>
  </xsl:if>
  <xsl:if test="vrmlpn[.!='']">
    <tr><td class="head" colspan="7">&#xA0;<b>Laboratorn� pozn�mka ke kompletn�mu n�lezu</b></td></tr>
    <tr><td class="text" colspan="7">&#xA0;<xsl:value-of select="vrmlpn"/></td></tr>
  </xsl:if>
  <xsl:if test="vrmzhl[.!='']">
    <tr><td class="head" colspan="7">&#xA0;<b>Z�v�re�n� hodnocen� l�ka�em</b></td></tr>
    <tr><td class="text" colspan="7">&#xA0;<xsl:value-of select="vrmzhl"/></td></tr>
  </xsl:if>
  <xsl:if test="count(vrkpa) > 0 ">
    <xsl:apply-templates select="vrkpa"/>
  </xsl:if>
</xsl:template>


<xsl:template match="vrkpa">
  <tr><td class="agens" colspan="7">&#xA0;<b>P��tomn� agens:&#xA0;&#xA0;<xsl:value-of select="@agens_text"/></b></td></tr>
  <tr><td class="head" colspan="1">&#xA0;<b>k�d</b></td>
      <td class="head" colspan="1">&#xA0;<b>p��tomnost</b></td>
      <td class="head" colspan="2">&#xA0;<b>kvantita</b></td>
      <td class="head" colspan="3">&#xA0;<b>informace o vy�et�ov�n�</b></td>
  </tr>
  <tr><td class="text" colspan="1">&#xA0;<xsl:value-of select="@agens_kod"/></td>
      <td class="text" colspan="1">&#xA0;<xsl:if test="@pritomnost[.='M']">p��tomno masivn�</xsl:if>
                                         <xsl:if test="@pritomnost[.='P']">p��tomno</xsl:if>
                                         <xsl:if test="@pritomnost[.='O']">p��tomno ojedin�le</xsl:if>
                                         <xsl:if test="@pritomnost[.='N']">neprok�z�no</xsl:if>
      </td>
      <td class="text" colspan="2">&#xA0;
        <xsl:choose>
          <xsl:when test="kvantita/@priznak[.='M']">
            &lt;<xsl:value-of select="kvantita"/>
          </xsl:when> 
          <xsl:when test="kvantita/@priznak[.='V']">
            &gt;<xsl:value-of select="kvantita"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="kvantita"/>
          </xsl:otherwise>
        </xsl:choose>
      </td>
      <td class="text" colspan="3">&#xA0;<xsl:if test="@dalsi_vys[.='N']">nebude d�le vy�et�ov�no</xsl:if>
                                         <xsl:if test="@dalsi_vys[.='A']">bude d�le vy�et�ov�no - je�t� nen� zapo�ato</xsl:if>
                                         <xsl:if test="@dalsi_vys[.='N']">bude d�le vy�et�ov�no - rozpracov�no</xsl:if>
                                         <xsl:if test="@dalsi_vys[.='D']">vy�et�en� dokon�eno</xsl:if>

      </td>
  </tr>
  <xsl:if test="vrav/@vl_kod[.!='']">
    <tr><td class="head" colspan="7">&#xA0;<b>Testovan� vlastnosti agens</b></td></tr>
    <tr><td class="head" colspan="1">&#xA0;<b>k�d</b></td>
        <td class="head" colspan="1">&#xA0;<b>n�zev</b></td>
        <td class="head" colspan="2">&#xA0;<b>p��tomnost</b></td>
        <td class="head" colspan="3">&#xA0;<b>dodatkov� text</b></td>
    </tr>
    <xsl:apply-templates select="vrav"/>
  </xsl:if>
  <xsl:if test="vrac/@latka_kod[.!='']">
    <tr><td class="head" colspan="7">&#xA0;<b>Citlivost agens na antimikrobi�ln� l�tku</b></td></tr>
    <tr><td class="head" colspan="1">&#xA0;<b>k�d</b></td>
        <td class="head" colspan="3">&#xA0;<b>n�zev</b></td>
        <td class="head" colspan="3">&#xA0;<b>citlivost</b></td>
    </tr>
    <xsl:apply-templates select="vrac"/>
  </xsl:if>
  <xsl:if test="dourceni[.!='']">
    <tr><td class="head" colspan="7">&#xA0;<b>dour�en� agens</b></td></tr>
    <tr><td class="text" colspan="7">&#xA0;<xsl:value-of select="dourceni"/></td></tr>
  </xsl:if>
  <xsl:if test="vrmahn[.!='']">
    <tr><td class="head" colspan="7">&#xA0;<b>Automatick� hodnocen� vy�et�en� agens</b></td></tr>
    <tr><td class="text" colspan="7">&#xA0;<xsl:value-of select="vrmahn"/></td></tr>
  </xsl:if>
  <xsl:if test="vrmlpn[.!='']">
    <tr><td class="head" colspan="7">&#xA0;<b>Laboratorn� pozn�mka k vy�et�en� agens</b></td></tr>
    <tr><td class="text" colspan="7">&#xA0;<xsl:value-of select="vrmlpn"/></td></tr>
  </xsl:if>
  <xsl:if test="vrmzhl[.!='']">
    <tr><td class="head" colspan="7">&#xA0;<b>Z�v�re�n� hodnocen� vy�et�en� agens l�ka�em</b></td></tr>
    <tr><td class="text" colspan="7">&#xA0;<xsl:value-of select="vrmzhl"/></td></tr>
  </xsl:if>
</xsl:template>

<xsl:template match="vrav">
  <tr><td class="text" colspan="1">&#xA0;<xsl:value-of select="@vl_kod"/></td>
      <td class="text" colspan="1">&#xA0;<xsl:value-of select="@vl_text"/></td>
      <td class="text" colspan="2">&#xA0;<xsl:if test="@pritomnost[.='P']">p��tomna</xsl:if>
                                         <xsl:if test="@hod_citlivosti[.='N']">nep��tomna</xsl:if>
                                         <xsl:if test="@hod_citlivosti[.='X']">nelze hodnotit</xsl:if></td>    
      <td class="text" colspan="3">&#xA0;<xsl:if test="@dodatek[.!='']"><xsl:value-of select="@dodatek"/></xsl:if></td>    
  </tr>
</xsl:template>




<xsl:template match="vrac">
  <tr><td class="text" colspan="1">&#xA0;<xsl:value-of select="@latka_kod"/></td>
      <td class="text" colspan="3">&#xA0;<xsl:value-of select="@latka_text"/></td>
      <td class="text" colspan="3">&#xA0;<xsl:if test="@hod_citlivosti[.='C']">citliv�</xsl:if>
                                         <xsl:if test="@hod_citlivosti[.='R']">rezistentn�</xsl:if>
                                         <xsl:if test="@hod_citlivosti[.='I']">intermedi�rn�</xsl:if>
                                         <xsl:if test="@hod_citlivosti[.='N']">netestov�no</xsl:if>
                                         <xsl:if test="@hod_citlivosti[.='K']">kontaminov�no</xsl:if>
                                         <xsl:if test="@hod_citlivosti[.='L']">v�sledek dostupn� pouze v laborato�i</xsl:if>
      </td>    
  </tr>
</xsl:template>




</xsl:stylesheet> 