<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">  
<xsl:output method="html" encoding="utf-8" /> 
<xsl:param name="id" />

<xsl:template match="/"> 
<dl class="ResultHeader">
<xsl:apply-templates select="dasta/pm" />
<xsl:apply-templates select="dasta/is/ip[@id_pac=$id][1]" mode="Header" />
</dl>
<div class="Cleaner"></div>
<xsl:apply-templates select="dasta/is/ip[@id_pac=$id]"/>
<div class="Footer">
<xsl:apply-templates select="dasta/is" />
</div>
</xsl:template> 

<xsl:template match="dasta/is">
<strong>Provádějící laboratoř:</strong>
<xsl:choose>
<xsl:when test="@icz"><xsl:value-of select="@icz" /></xsl:when>
<xsl:when test="@icp"><xsl:value-of select="@icp" /></xsl:when>
</xsl:choose>
<xsl:if test="a/jmeno[.!='']">&#xA0;<xsl:value-of select="a/jmeno" /></xsl:if>
<xsl:if test="a/adr[.!='']">,&#xA0;<xsl:value-of select="a/adr"/></xsl:if>
<xsl:if test="a/dop1[.!='']">,&#xA0;<xsl:value-of select="a/dop1"/></xsl:if> 
<xsl:if test="a/dop2[.!='']">,&#xA0;<xsl:value-of select="a/dop2"/></xsl:if> 
<xsl:if test="a/psc[.!='']">,&#xA0;<xsl:value-of select="a/psc"/></xsl:if> 
<xsl:if test="a/mesto[.!='']">,&#xA0;<xsl:value-of select="a/mesto"/></xsl:if> 
</xsl:template> 

<xsl:template match="dasta/pm">
<dt>Požadující lékař:</dt>
<dd>
<strong>
<xsl:choose>
<xsl:when test="@icz"><xsl:value-of select="@icz" /></xsl:when>
<xsl:when test="@icp"><xsl:value-of select="@icp" /></xsl:when>
</xsl:choose>
</strong>&#xA0;<xsl:value-of select="a/jmeno" /></dd>
</xsl:template> 

<xsl:template match="dasta/is/ip" mode="Header">
<dt>Pacient:</dt>
<dd>
<xsl:if test="rodcis"><strong><xsl:value-of select="rodcis" /></strong>&#xA0;</xsl:if>
<xsl:value-of select="prijmeni" />&#xA0;<xsl:value-of select="jmeno" />
<!--<xsl:if test="rod_prijm"> (rodné příjmení - <xsl:value-of select="rod_prijm" />)</xsl:if> -->
</dd>
<dt>Zdravotní pojišťovna:</dt>
<dd>
<xsl:choose>
<xsl:when test="p">
 <xsl:variable name="kod" select="p/kodpoj"/>
 <strong><xsl:value-of select="p/kodpoj" /></strong> {ZDRPOJ-<xsl:value-of select="p/kodpoj"/>}
</xsl:when>
<xsl:otherwise>není uvedena</xsl:otherwise>
</xsl:choose>
</dd>

<xsl:if test="dg/dgz/diag[.!='']">     
<dt>Diagnozy:</dt>
<dd><xsl:apply-templates select="dg"/></dd>
</xsl:if>

<xsl:apply-templates select="v/vr[1]" mode="Header"/>
</xsl:template> 

<xsl:template match="v/vr" mode="Header">  	
 <xsl:if test="@typpol_fh='R'">
    <dt>Datum a čas události:</dt>
    <dd>
		 <xsl:value-of select="substring(dat_du,9,2)" />.<xsl:value-of select="substring(dat_du,6,2)" />.<xsl:value-of select="substring(dat_du,1,4)" />&#xA0;
		 <xsl:value-of select="substring(dat_du,12,8)" />
    </dd>
    <xsl:if test="@id_lis">
    <dt>Identifikace vzorku v laboratoři:</dt>
    <dd><xsl:value-of select="@id_lis" /></dd>
    </xsl:if>
 </xsl:if>

</xsl:template> 

<xsl:template match="dasta/is/ip">

<xsl:if test="count(v/vr) = 0"> 
<p>Pacient nemá uvedeny žádné laboratorní výsledky</p>
</xsl:if> 

<xsl:if test="v/vr/@typpol_fh[.='F' or .='0' or .='1' or .='2' or .='3' or .='4' or .='5' or .='6' or .='T']" >
<table class="Result">
<tr>
 <th>Datum odběru</th>
 <th>NČLP</th>
 <th>Název</th>
 <th>Hodnota (nejistota)</th>
 <th>Jednot.</th>
 <th>Ref.rozmezí</th>
</tr>
 <xsl:if test="v/vr/@typpol_fh[.='0' or .='1' or .='2' or .='3' or .='4' or .='5' or .='6' or .='T']" > 
  <xsl:apply-templates select="v/vr/vrn"/>
 </xsl:if>
 <xsl:if test="v/vr/@typpol_fh[.='F']" > 
  <xsl:apply-templates select="v/vr/vrf"/>
 </xsl:if>
</table> 
</xsl:if>

<xsl:if test="v/vr/@typpol_fh[.='X' or .='B']" >
<h2>Neformalizované laboratorní výsledky</h2>
<table class="Result">
<tr>
 <th>NČLP</th>
 <th>název</th>
 <th colspan="5">hodnota</th>
</tr>
<tr>
 <td colspan="1">lab. vzorek</td>
 <td colspan="2">datum a čas události (odběru)</td>
 <td colspan="2">datum a čas přijetí vzorku</td>
 <td colspan="2">datum a čas vydání výsledku</td>
</tr>
 <xsl:apply-templates select="v/vr/vrx"/>
</table> 
</xsl:if>

<xsl:if test="v/vr/@typpol_fh[.='K']" >
<h2>Mikrobiologické laboratorní výsledky</h2>
<table class="Result">
<tr>
 <th>NČLP</th>
 <th>název</th>
</tr>
<tr>
 <td colspan="1">lab. vzorek</td>
 <td colspan="2">datum a čas události (odběru)</td>
 <td colspan="2">datum a čas přijetí vzorku</td>
 <td colspan="2">datum a čas vydání výsledku</td>
</tr>
 <xsl:apply-templates select="v/vr/vrk"/>
</table> 
</xsl:if>

<xsl:if test="v/vr/@typpol_fh[.='B']" >
<h2>Interpretace</h2>
<table class="Result">
 <xsl:apply-templates select="v/vr/vrb"/>
</table> 
</xsl:if>

<xsl:if test="v/vr/@typpol_fh[.='R']" >
<h2>Laboratorní výsledky ve formátu tiskových sestav</h2>
 <xsl:apply-templates select="v/vr[@typpol_fh='R']"/>
</xsl:if>

</xsl:template> 


<xsl:template match="v/vr">  	
 <xsl:if test="@typpol_fh='R'">
  <pre><xsl:value-of select="vrr/text/ptext" disable-output-escaping="yes" /></pre>
 </xsl:if>
</xsl:template> 

<xsl:template match="v/vr/vrn">
 <tr>
  <td>
	 <xsl:if test="../dat_du[.!='']">
   <xsl:value-of select="substring(../dat_du,9,2)" />.<xsl:value-of select="substring(../dat_du,6,2)" />.<xsl:value-of select="substring(../dat_du,1,4)" />	 &#xA0;<xsl:value-of select="substring(../dat_du,12,5)" />
  </xsl:if>
	</td>
	 <xsl:variable name="klic" select="../@klic_nclp"/>
    
  <td><b><xsl:value-of select="../@klic_nclp" /></b></td>
  <td><b>{NCLPPOL-<xsl:value-of select="../@klic_nclp"/>}</b></td>
  <td>
   <xsl:choose>
    <xsl:when test="../@typpol_fh[.='T']">
     <xsl:choose>
      <xsl:when test="../@urg_info[.='E']"><span class="flag_e"><b>1:<xsl:value-of select="hodnota" /></b></span></xsl:when>
      <xsl:when test="../@urg_info[.='R']"><span class="flag_r"><b>1:<xsl:value-of select="hodnota" /></b></span></xsl:when>
      <xsl:when test="../@urg_info[.='Z']"><span class="flag_e"><b>1:<xsl:value-of select="hodnota" /></b></span></xsl:when>
      <xsl:when test="../@urg_info[.='P']"><span class="flag_e"><b>1:<xsl:value-of select="hodnota" /></b></span></xsl:when>
      <xsl:otherwise><b>1:<xsl:value-of select="hodnota" /></b></xsl:otherwise>
     </xsl:choose>
    </xsl:when>
    <xsl:otherwise>
     <xsl:choose>
      <xsl:when test="../@urg_info[.='E']"> 
			 <span class="flag_e">   
         <xsl:if test="@priznak_kvant[.='R']">
          <b><xsl:value-of select="hodnota" /><xsl:if test="nejistota/nejist_var1/@h_nejist[.!='']"> ( +- <xsl:value-of select="nejistota/nejist_var1/@h_nejist" />)</xsl:if></b>
         </xsl:if>
         <xsl:if test="@priznak_kvant[.='M']">
        <b>&#xA0;<xsl:value-of select="hodnota" /><xsl:if test="nejistota/nejist_var1/@h_nejist[.!='']"> &#xA0;&#xA0;( +- &#xA0;<xsl:value-of select="nejistota/nejist_var1/@h_nejist" />)</xsl:if></b>
               </xsl:if>
               <xsl:if test="@priznak_kvant[.='V']">
               <b>&#xA0;&#xA0;&#xA0;<xsl:value-of select="hodnota" /><xsl:if test="nejistota/nejist_var1/@h_nejist[.!='']"> &#xA0;&#xA0;( +- &#xA0;<xsl:value-of select="nejistota/nejist_var1/@h_nejist" />)</xsl:if></b>
               </xsl:if>
             </span>
           </xsl:when>
           <xsl:when test="../@urg_info[.='R']">
             <span class="flag_r">   
               <xsl:if test="@priznak_kvant[.='R']">
               <b>&#xA0;&#xA0;&#xA0;<xsl:value-of select="hodnota" /><xsl:if test="nejistota/nejist_var1/@h_nejist[.!='']"> &#xA0;&#xA0;( +- &#xA0;<xsl:value-of select="nejistota/nejist_var1/@h_nejist" />)</xsl:if> </b>
               </xsl:if>
               <xsl:if test="@priznak_kvant[.='M']">
               <b>&#xA0;&#xA0;&#xA0;<xsl:value-of select="hodnota" /><xsl:if test="nejistota/nejist_var1/@h_nejist[.!='']"> &#xA0;&#xA0;( +- &#xA0;<xsl:value-of select="nejistota/nejist_var1/@h_nejist" />)</xsl:if></b>
               </xsl:if>
               <xsl:if test="@priznak_kvant[.='V']">
               <b>&#xA0;&#xA0;&#xA0;<xsl:value-of select="hodnota" /><xsl:if test="nejistota/nejist_var1/@h_nejist[.!='']"> &#xA0;&#xA0;( +- &#xA0;<xsl:value-of select="nejistota/nejist_var1/@h_nejist" />)</xsl:if></b>
               </xsl:if>
             </span>
           </xsl:when>
           <xsl:when test="../@urg_info[.='Z']">
             <span class="flag_z">   
               <xsl:if test="@priznak_kvant[.='R']">
               <b>&#xA0;&#xA0;&#xA0;<xsl:value-of select="hodnota" /><xsl:if test="nejistota/nejist_var1/@h_nejist[.!='']"> &#xA0;&#xA0;( +- &#xA0;<xsl:value-of select="nejistota/nejist_var1/@h_nejist" />)</xsl:if></b>
               </xsl:if>
               <xsl:if test="@priznak_kvant[.='M']">
               <b>&#xA0;&#xA0;&#xA0;<xsl:value-of select="hodnota" /><xsl:if test="nejistota/nejist_var1/@h_nejist[.!='']"> &#xA0;&#xA0;( +- &#xA0;<xsl:value-of select="nejistota/nejist_var1/@h_nejist" />)</xsl:if></b>
               </xsl:if>
               <xsl:if test="@priznak_kvant[.='V']">
               <b>&#xA0;&#xA0;&#xA0;<xsl:value-of select="hodnota" /><xsl:if test="nejistota/nejist_var1/@h_nejist[.!='']"> &#xA0;&#xA0;( +- &#xA0;<xsl:value-of select="nejistota/nejist_var1/@h_nejist" />)</xsl:if></b>
               </xsl:if>
             </span>
           </xsl:when>
           <xsl:when test="../@urg_info[.='P']">
             <span class="flag_p">   
               <xsl:if test="@priznak_kvant[.='R']">
               <b>&#xA0;&#xA0;&#xA0;<xsl:value-of select="hodnota" /><xsl:if test="nejistota/nejist_var1/@h_nejist[.!='']"> &#xA0;&#xA0;( +- &#xA0;<xsl:value-of select="nejistota/nejist_var1/@h_nejist" />)</xsl:if></b>
               </xsl:if>
               <xsl:if test="@priznak_kvant[.='M']">
               <b>&#xA0;&#xA0;&#xA0;<xsl:value-of select="hodnota" /><xsl:if test="nejistota/nejist_var1/@h_nejist[.!='']"> &#xA0;&#xA0;( +- &#xA0;<xsl:value-of select="nejistota/nejist_var1/@h_nejist" />)</xsl:if></b>
               </xsl:if>
               <xsl:if test="@priznak_kvant[.='V']">
               <b>&#xA0;&#xA0;&#xA0;<xsl:value-of select="hodnota" /><xsl:if test="nejistota/nejist_var1/@h_nejist[.!='']"> &#xA0;&#xA0;( +- &#xA0;<xsl:value-of select="nejistota/nejist_var1/@h_nejist" />)</xsl:if></b>
               </xsl:if>
             </span>
           </xsl:when>
           <xsl:otherwise>
             <xsl:if test="@priznak_kvant[.='R']">
               <b>&#xA0;&#xA0;&#xA0;<xsl:value-of select="hodnota" /><xsl:if test="nejistota/nejist_var1/@h_nejist[.!='']"> &#xA0;&#xA0;( +- &#xA0;<xsl:value-of select="nejistota/nejist_var1/@h_nejist" />)</xsl:if></b>
             </xsl:if>
             <xsl:if test="@priznak_kvant[.='M']">
               <b>&#xA0;&#xA0;&#xA0;<xsl:value-of select="hodnota" /><xsl:if test="nejistota/nejist_var1/@h_nejist[.!='']"> &#xA0;&#xA0;( +- &#xA0;<xsl:value-of select="nejistota/nejist_var1/@h_nejist" />)</xsl:if></b>
             </xsl:if>
             <xsl:if test="@priznak_kvant[.='V']">
               <b>&#xA0;&#xA0;&#xA0;<xsl:value-of select="hodnota" /><xsl:if test="nejistota/nejist_var1/@h_nejist[.!='']"> &#xA0;&#xA0;( +- &#xA0;<xsl:value-of select="nejistota/nejist_var1/@h_nejist" />)</xsl:if></b>
             </xsl:if>
           </xsl:otherwise> 
         </xsl:choose>
       </xsl:otherwise>
     </xsl:choose>
   </td>
   <td><b><xsl:value-of select="prepocet/@jednotka_lclp" /><xsl:value-of select="../polozka_nclp/@njedn" /></b></td>
   <td align="center"><xsl:if test="skala/s5[.!='']">(<xsl:value-of select="skala/s4" />-<xsl:value-of select="skala/s5" />)</xsl:if></td>
  </tr>

  <xsl:choose>
    <xsl:when test="../autor">
      <tr><td class="pozn" colspan="3">&#xA0;autorizoval(a): &#xA0;<xsl:value-of select="../autor" /></td>
            <td class="pozn" colspan="4">&#xA0;odeslal(a): &#xA0; <xsl:value-of select="../odeslal" /></td>
      </tr>
    </xsl:when>
   <xsl:otherwise>
       <xsl:if test="../odeslal[.!='']">
         <tr><td class="pozn" colspan="3">&#xA0;autorizoval(a): &#xA0;<xsl:value-of select="../autor" /></td>
               <td class="pozn" colspan="4">&#xA0;odeslal(a): &#xA0; <xsl:value-of select="../odeslal" /></td>
         </tr>
       </xsl:if>
   </xsl:otherwise>
 </xsl:choose>

    <xsl:if test="../sci/@id_sci_is[.!='']">
        <tr><td class="ft" colspan="7"><b>Hodnota přísluší k funkčnímu testu</b></td></tr>
        <tr><td class="ft" colspan="2">&#xA0;<b>název:</b>&#xA0;<xsl:value-of select="../sci/polozka_nclp/@nkomp" /></td>
               <td class="ft" colspan="2">&#xA0;<b>identifikace testu:</b>&#xA0;<xsl:value-of select="../sci/@id_sci_is"/></td>
               <td class="ft" colspan="1">&#xA0;<b>krok :</b>&#xA0;<xsl:value-of select="../sci/@krok" /></td>
              <td class="ft" colspan="2">&#xA0;<b>průběh :</b>&#xA0;
                                <xsl:if test="../sci/@prubeh[.='Z']">začátek</xsl:if>
		<xsl:if test="../sci/@prubeh[.='P']">průběh</xsl:if>		
		<xsl:if test="../sci/@prubeh[.='K']">konec</xsl:if>	
              </td>
          </tr>
    </xsl:if>


</xsl:template>


<xsl:template match="v/vr/vrf">

  <tr>
   <td class="text"><b><xsl:value-of select="../@klic_nclp" /></b></td>
   <td class="text"><b><xsl:value-of select="../nazev_lclp" /></b></td>
   <td class="text"><b><xsl:value-of select="hodnota_kod" /></b></td>
   <td class="text"><b><xsl:value-of select="../polozka_nclp/@njedn" /></b></td>
   <td class="text"></td>
  </tr>
  <tr><td class="pozn" colspan="1"><xsl:value-of select="../@id_lis" /></td>
      <td class="pozn" colspan="2"><xsl:if test="../dat_du[.!='']">
                                            <xsl:value-of select="substring(../dat_du,9,2)" />.
                                            <xsl:value-of select="substring(../dat_du,6,2)" />.
                                            <xsl:value-of select="substring(../dat_du,1,4)" />&#xA0;
                                            <xsl:value-of select="substring(../dat_du,12,2)" />:
                                            <xsl:value-of select="substring(../dat_du,15,2)" />:
                                            <xsl:value-of select="substring(../dat_du,18,2)" />
                                          </xsl:if></td>
           
      <td class="pozn" colspan="2">&#xA0;<xsl:if test="../dat_pl[.!='']">
                                            <xsl:value-of select="substring(../dat_pl,9,2)" />.
                                            <xsl:value-of select="substring(../dat_pl,6,2)" />.
                                            <xsl:value-of select="substring(../dat_pl,1,4)" />&#xA0;
                                            <xsl:value-of select="substring(../dat_pl,12,2)" />:
                                            <xsl:value-of select="substring(../dat_pl,15,2)" />
                                          </xsl:if></td>
      <td class="pozn" colspan="2">&#xA0;<xsl:if test="../dat_vv[.!='']">
                                            <xsl:value-of select="substring(../dat_vv,9,2)" />.
                                            <xsl:value-of select="substring(../dat_vv,6,2)" />.
                                            <xsl:value-of select="substring(../dat_vv,1,4)" />&#xA0;
                                            <xsl:value-of select="substring(../dat_vv,12,2)" />:
                                            <xsl:value-of select="substring(../dat_vv,15,2)" />
                                          </xsl:if></td>
  </tr>	
</xsl:template>

<xsl:template match="v/vr/vrb">
 <xsl:choose>
 <xsl:when test="../@klic_nclp = '20044'">
 </xsl:when>
 <xsl:otherwise>
    
<tr> 
  <td class="pozn"><xsl:if test="../dat_du[.!='']">
   <xsl:value-of select="substring(../dat_du,9,2)" />.<xsl:value-of select="substring(../dat_du,6,2)" />.<xsl:value-of select="substring(../dat_du,1,4)" />
	 &#xA0;
   <xsl:value-of select="substring(../dat_du,12,2)" />:<xsl:value-of select="substring(../dat_du,15,2)" />:<xsl:value-of select="substring(../dat_du,18,2)" />
  </xsl:if>
 </td>
	 <xsl:variable name="klic" select="../@klic_nclp"/>
 <td class="text" colspan="1" ><b>&#xA0;<xsl:value-of select="../@klic_nclp" /></b></td>
 <td><b>{NCLPPOL-<xsl:value-of select="../@klic_nclp"/>}</b></td>
 <td class="text" colspan="4" ><b>&#xA0;<xsl:value-of select="../nazev_lclp" /></b></td>
</tr>
 </xsl:otherwise>
 </xsl:choose>
  <tr><td class="text" colspan="7"><pre><xsl:value-of select="text/ptext" disable-output-escaping="yes" /></pre></td></tr>
</xsl:template>


<xsl:template match="dgz">
 <xsl:variable name="diag">
  <xsl:value-of select="normalize-space(diag)"/>
 </xsl:variable>
 <xsl:if test="string-length($diag)= 4"> 
  <strong><xsl:value-of select="substring($diag,1,3)" />.<xsl:value-of select="substring($diag,4,1)" /></strong>
 </xsl:if>
 <xsl:if test="string-length($diag)= 3"> 
  <xsl:value-of select="$diag" />
 </xsl:if>
  {MKN10_5-<xsl:value-of select="$diag" />}
</xsl:template>

<xsl:template match="v/vr/vrx">
	 <xsl:variable name="klic" select="../@klic_nclp"/>
    
  <tr>
   <td class="text" colspan="1" ><b>&#xA0;<xsl:value-of select="../@klic_nclp" /></b></td>
   <td><b>{NCLPPOL-<xsl:value-of select="../@klic_nclp"/>}</b></td>
<!--11.11.2011-->   <td class="text" colspan="1" ><b><xsl:value-of select="../nazev_lclp" /></b></td>
   <td class="text" colspan="3" >
         <xsl:choose>
           <xsl:when test="../@urg_info[.='E']">
             <font class="flag_e"><b><xsl:value-of select="hodnota_nt" /></b></font>
           </xsl:when>
           <xsl:when test="../@urg_info[.='R']">
             <font class="flag_r"><b><xsl:value-of select="hodnota_nt" /></b></font>
           </xsl:when>
           <xsl:when test="../@urg_info[.='Z']">
             <font class="flag_e"><b><xsl:value-of select="hodnota_nt" /></b></font>
           </xsl:when>
           <xsl:when test="../@urg_info[.='P']">
             <font class="flag_e"><b><xsl:value-of select="hodnota_nt" /></b></font>
           </xsl:when>
           <xsl:otherwise>
             <b>1:<xsl:value-of select="hodnota_nt" /></b>
           </xsl:otherwise>
         </xsl:choose>
   </td>
  </tr>
  <tr><td class="pozn" colspan="1">&#xA0;<xsl:value-of select="../@id_lis" /></td>
      <td class="pozn" colspan="2">&#xA0;<xsl:if test="../dat_du[.!='']">
                                            <xsl:value-of select="substring(../dat_du,9,2)" />.
                                            <xsl:value-of select="substring(../dat_du,6,2)" />.
                                            <xsl:value-of select="substring(../dat_du,1,4)" />&#xA0;&#xA0;
                                            <xsl:value-of select="substring(../dat_du,12,2)" />:
                                            <xsl:value-of select="substring(../dat_du,15,2)" />:
                                            <xsl:value-of select="substring(../dat_du,18,2)" />
                                          </xsl:if></td>
           
      <td class="pozn" colspan="2">&#xA0;<xsl:if test="../dat_pl[.!='']">
                                            <xsl:value-of select="substring(../dat_pl,9,2)" />.
                                            <xsl:value-of select="substring(../dat_pl,6,2)" />.
                                            <xsl:value-of select="substring(../dat_pl,1,4)" />&#xA0;&#xA0;
                                            <xsl:value-of select="substring(../dat_pl,12,2)" />:
                                            <xsl:value-of select="substring(../dat_pl,15,2)" />
                                          </xsl:if></td>
      <td class="pozn" colspan="2">&#xA0;<xsl:if test="../dat_vv[.!='']">
                                            <xsl:value-of select="substring(../dat_vv,9,2)" />.
                                            <xsl:value-of select="substring(../dat_vv,6,2)" />.
                                            <xsl:value-of select="substring(../dat_vv,1,4)" />&#xA0;&#xA0;
                                            <xsl:value-of select="substring(../dat_vv,12,2)" />:
                                            <xsl:value-of select="substring(../dat_vv,15,2)" />
                                          </xsl:if></td>
  </tr>	
</xsl:template>
</xsl:stylesheet> 

