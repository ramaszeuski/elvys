insert into "role" values ('root', 'true', 'Správce systému');
insert into "role" values ('customers_manager', 'true', 'Správce zákazníků');
insert into "role" values ('dealers_manager', 'true', 'Manažer obchodníků');
insert into "role" values ('dealer', 'true', 'Obchodník');
insert into "role" values ('supervisor', 'true', 'Supervizor');


insert into "user" values ('andrej', 'andrej666');
insert into "user_role" values ('andrej', 'root');
