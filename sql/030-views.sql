CREATE LANGUAGE plpgsql;
create or replace function "cz_off" (text) returns text as
$$
 begin
	return lower(translate ($1, 'áčďéěíĺňóřšťúůýžÁČĎÉĚÍĹŇÓŘŠŤÚŮÝŽ', 'acdeeilnorstuuyzacdeeilnorstuuyz'));
 end;
$$
language 'plpgsql';

begin;
drop view if exists "login_view";
create view "login_view" as
select
    "username",
    "password"
    from "user" where "active"
union
select
    "login" as "username",
    "password"
    from "subject"
    where "active" and "deleted" is null
;
end;

begin;
drop view if exists "login_role_view";
create view "login_role_view" as
select
    "user_username" as "login",
    "role_role" as "role"
    from "user_role"
union
select
    "login",
    "role"
    from "subject"
    where "active" and "deleted" is null
;
end;

create view "ftp_user_view" as

select
    "id" as "userid",
    "password" as "passwd",
    '/var/lib/ecdata/inbox/' || "id" as "homedir"
from "lab"
    where "active"
    and "deleted" is null
;


create view "ftp_group_view" as
select
    1001::integer as "gid",
    'ecdata'::text as "name",
    ''::text as "members"
;

begin;
drop view if exists "result_view";
create view "result_view" as
select
    "result"."id",
    "result"."sampling",
    "result"."notified",
    "result"."xml_id",
    "result"."value",
    "xml"."generated",
    "xml"."lab_id",
    "lab"."icz" as "lab_icz",
    "lab"."icp" as "lab_icp",
    "lab"."name" as "lab_name",
    "xml"."customer_id",
    "customer"."icz" as "customer_icz",
    "customer"."icp" as "customer_icp",
    "customer"."name" as "customer_name",
    "customer"."email" as "customer_email",
    "customer"."email_notify" as "email_notify",
    "customer"."dealer_account_login" as "dealer_account_login",
    "result"."patient_id",
    "patient"."rc",
    "patient"."firstname",
    "patient"."lastname",
    cz_off("patient"."firstname" || ' ' || "patient"."lastname") as "name_cz_off"
    from "result"
    join "xml" on ("result"."xml_id" = "xml"."id")
    join "lab" on ("xml"."lab_id" = "lab"."id")
    join "customer" on ("xml"."customer_id" = "customer"."id")
    join "patient" on ("result"."patient_id" = "patient"."id")
;
end;

begin;
drop view if exists "xml_view";
create view "xml_view" as
select
    "xml".*,
    "lab"."icz" as "lab_icz",
    "lab"."icp" as "lab_icp",
    "lab"."name" as "lab_name",
    "customer"."icz" as "customer_icz",
    "customer"."icp" as "customer_icp",
    "customer"."name" as "customer_name"
    from "xml"
    join "lab" on ("xml"."lab_id" = "lab"."id")
    join "customer" on ("xml"."customer_id" = "customer"."id")
;
end;

begin;
drop view if exists "log_view";
create view "log_view" as
select
    "log".*,
    coalesce ("subject"."role", 'system') as "user_role",
    "subject"."name" as "user_name",
    coalesce ("subject"."firstname", "user"."firstname") as "user_firstname",
    coalesce ("subject"."lastname", "user"."lastname") as "user_lastname"
    from "log"
    left join "subject" on ("log"."user_username" = cast("subject"."id" as text))
    left join "user" on ("log"."user_username" = "user"."username")
;
end;

begin;
drop view if exists "dealer_view";
create view "dealer_view" as
select
    "user".*,
    "user"."firstname" || ' ' || "user"."lastname" as "name"
from "user"
join "user_role"
on (
    "user"."username" = "user_role"."user_username"
    and
    "user_role"."role_role" = 'dealer'
)
;
end;

end;

begin;
drop view if exists "result_archive_view";
create view "result_archive_view" as
select
    "result_archive"."id",
    "result_archive"."sampling",
    "result_archive"."notified",
    "result_archive"."xml_id",
    "result_archive"."value",
    "xml_archive"."generated",
    "xml_archive"."lab_id",
    "lab"."icz" as "lab_icz",
    "lab"."icp" as "lab_icp",
    "lab"."name" as "lab_name",
    "xml_archive"."customer_id",
    "customer"."icz" as "customer_icz",
    "customer"."icp" as "customer_icp",
    "customer"."name" as "customer_name",
    "customer"."email" as "customer_email",
    "customer"."email_notify" as "email_notify",
    "customer"."dealer_account_login" as "dealer_account_login",
    "result_archive"."patient_id",
    "patient"."rc",
    "patient"."firstname",
    "patient"."lastname",
    cz_off("patient"."firstname" || ' ' || "patient"."lastname") as "name_cz_off"
    from "result_archive"
    join "xml_archive" on ("result_archive"."xml_id" = "xml_archive"."id")
    join "lab" on ("xml_archive"."lab_id" = "lab"."id")
    join "customer" on ("xml_archive"."customer_id" = "customer"."id")
    join "patient" on ("result_archive"."patient_id" = "patient"."id")
;
end;

begin;
drop view if exists "result_all_view";
create view "result_all_view" as
    select * from "result_view"
    union
    select * from "result_archive_view"
;
end;

create view if exists "patient_view" as
select
    *,
    cz_off("lastname") as "lastname_cz_off"
from "patient"
;

