create table "region" (
    "id" varchar(8) not null,
    "name" text,
    primary key ("id")
);

create table "subject" (
    "id"        integer not null default nextval('uid_seq'),
    "role"      varchar(10) not null, --customer, pacient, lab, clinic
    "active"    bool not null default true,
    "created"   timestamp(0) not null default now(),
    "deleted"   timestamp(0),
    "login"     varchar(32),
    "password"  varchar(32),
--jmeno, prijmeni / nazev
    "name"      text,
    "firstname" text,
    "lastname"  text,
--kontakty
    "email"     text,
    "messenger" text,
    "phone"     text,
    "cellular"  text,
    "fax"       text,
--adresa
    "country"   text,
    "region"    text,
    "city"      text,
    "address"   text,
    "zip"       text,
    "profile"   text,
    "notes"     text,
    "tfa"       smallint not null default 0
);

create table "lab" (
    "ico" varchar(8),
    "icz" varchar(8),
    "icp" varchar(8),
    "icl" varchar(8),
    "pcz" varchar(3),
    primary key ("id")
) inherits ("subject");
create index "lab_ico_idx" on "lab" ("ico");
create index "lab_icz_idx" on "lab" ("ico");
create index "lab_icp_idx" on "lab" ("ico");
create index "lab_icp_idx" on "lab" ("ico");

create table "customer" (
    "ico" varchar(8),
    "icz" varchar(8),
    "icp" varchar(8),
    "icl" varchar(8),
    "pcz" varchar(3),
    "dealer_account_login" varchar(32),
    "dealer_contract" bool,
    "region_id"  varchar(8),
    "specialization" text,
    "share" text,
    "primary_lab_id" integer,
    "clinic_id" integer,
    "specialization_code" varchar(3),
    "bonusprogramm_percents" smallint not null default 5,
    "direct_result" bool,
    primary key ("id"),
    unique("ico", "icz", "icp", "icl", "pcz")
) inherits ("subject");

create table "clinic" (
    primary key ("id")
) inherits ("subject");

create view "customer_view" as
select
    "customer".*,
    "user"."firstname" || ' ' ||"user"."lastname" as "dealer_name",
    "region"."name" as "region_name",
    "lab"."name" as "primary_lab_name",
    "clinic"."name" as "clinic_name"
from "customer"
left join "user" on ("customer"."dealer_account_login" = "user"."username")
left join "region" on ("customer"."region_id" = "region"."id")
left join "lab" on ("customer"."primary_lab_id" = "lab"."id")
left join "clinic" on ("customer"."clinic_id" = "clinic"."id")
;


create table "patient" (
    "rc" varchar(10),
    "birthdate" date, --is.ip.dat_dn
    "sex" varchar(8), --nektere laboratore u zvirat ignoruji DASTA
    "insurance_id" integer,
    primary key ("id")
) inherits ("subject");
create index "patient_rc_idx" on "patient" ("rc");

create table "creator" ( --zdroj_is - laboratorni system
    "id"     integer not null default nextval('uid_seq'),
    "name"   text, --lokalni pojmenovani
    "kod_firmy" text,
    "kod_prog" text,
    "verze_prog" text,
    "liccis_prog" text,
    primary key ("id"),
    unique("kod_firmy", "kod_prog", "verze_prog")
);

create table "xml"  (
    "id" varchar(40) not null,
--timestampy
    "generated" timestamp not null, -- dasta.dat.vb
    "processed" timestamp not null default now(),
    "downloaded" timestamp,
    "deleted" timestamp,
--vazby na subjecty
    "lab_id" bigint not null, --laborator
    "customer_id" bigint not null, --zakaznik
    "creator_id" bigint not null, --laboratorni system
--dasta
    "dasta_id_soubor" text,  -- jednoznačná vnitřní identifikace  souboru  v rámci firmy a jejího programu nebo informačního systému
    "dasta_verze_ds" text,   -- verze datové struktury
    "dasta_verze_nclp" text, -- verze používaného NČLP
    "dasta_bin_priloha" char(1), -- binární datové bloky
    "dasta_ur" char(1), -- určení; typ přenášených dat (v případě pacientských dat též urgentnost sdělení nebo zpracování souboru)
    "dasta_typ_odesm" text, -- typ odesílajícího místa
    "dasta_ozn_soub" text, -- doplňující označení odesílaného souboru
    "dasta_potvrzeni" char(1), -- požadavek na potvrzení přijetí souboru
    primary key ("id"),
    foreign key ("lab_id") references "lab" ("id") on update cascade on delete restrict,
    foreign key ("customer_id") references "customer" ("id") on update cascade on delete restrict,
    foreign key ("creator_id") references "creator" ("id") on update cascade on delete restrict
);

create table "result" ( --is
    "id"     integer not null default nextval('uid_seq'),
--timestampy
    "sampling"   timestamp,
    "viewed"     timestamp,
    "notified"    timestamp,
--vazby na subjecty
    "xml_id"     varchar(40) not null,
    "patient_id" bigint not null,
    "value"     text,
    primary key ("id"),
    foreign key ("xml_id") references "xml" ("id") on update cascade on delete cascade,
    foreign key ("patient_id") references "patient" ("id") on update cascade on delete restrict
);

create table "dasta_list" (
    "list" varchar(8) not null,
    "id"   varchar(16) not null,
    "content" text,
    primary key ("list", "id")
);

create table "take_customer_request" (
    "id"                    integer not null default nextval('uid_seq'),
    "state"                 smallint, --0 nova, 1 schvalena, 2 zamitnuta
    "created"               timestamp(0) not null default now(),
    "decided"               timestamp(0),
    "customer_id"           bigint not null, --zakaznik
    "dealer_account_login"  varchar(32),
    "manager_account_login" varchar(32),
    "request"               text,
    "response"              text,
    primary key ("id"),
    foreign key ("customer_id") references "customer" ("id") on update cascade on delete restrict,
    foreign key ("dealer_account_login") references "user" ("username") on update cascade on delete cascade,
    foreign key ("manager_account_login") references "user" ("username") on update cascade on delete cascade
);

begin;
drop view if exists "take_customer_request_view";
create view "take_customer_request_view" as
select
    "take_customer_request".*,
    "customer"."name"   as "customer_name",
    "pretender"."firstname" || ' ' || "pretender"."lastname"  as "pretender_name",
    "dealer"."firstname" || ' ' || "dealer"."lastname"     as "dealer_name",
    "manager"."firstname" || ' ' || "manager"."lastname"    as "manager_name"
--    concat_ws(' ', "pretender"."firstname", "pretender"."lastname")  as "pretender_name",
--    concat_ws(' ', "dealer"."firstname", "dealer"."lastname")     as "dealer_name",
--    concat_ws(' ', "manager"."firstname", "manager"."lastname")    as "manager_name"
from "take_customer_request"

join "customer" on (
    "take_customer_request"."customer_id" = "customer"."id"
)
join "user" "pretender" on (
    "take_customer_request"."dealer_account_login" = "pretender"."username"
)
left join "user" "dealer" on (
    "customer"."dealer_account_login" = "dealer"."username"
)
left join "user" "manager" on (
    "take_customer_request"."manager_account_login" = "manager"."username"
);
end;

create table "two_factor_codes" (
    "username" text,
    "code" text,
    "generated" timestamp not null default now(),
    primary key ("username")
);
