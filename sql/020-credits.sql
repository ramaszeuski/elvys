/* ZABIT V DATABAZI !!!
create table "credit_batch" (
    "id"     integer not null default nextval('uid_seq'),
    "lab_id" bigint not null,  --laborator
    "number" bigint not null, 
    "month"  smallint,
    "year"   smallint,
    primary key ("id")
);
*/

create table "credit_source" (
    "source"    varchar(128) not null, 
    "date"      date not null,
    "processed" timestamp(0) not null default now(),
    primary key ("source")
);


create table "credit" (
    "id"                  integer not null default nextval('uid_seq'),
    "lab_id"              bigint not null,  --laborator
    "customer_id"         bigint not null,  --zakaznik 
    "specialization_code" char(3),          --kod specializace
    "insurance_code"      integer,          --kod pojistovny
    "date"                date,
    "credits"             integer,
    primary key ("id"),
    foreign key ("lab_id") references "lab" ("id") on update cascade on delete restrict,
    foreign key ("customer_id") references "customer" ("id") on update cascade on delete restrict
);
create index "credit_lab_id_idx" on "credit" ("lab_id");
create index "credit_customer_id_idx" on "credit" ("customer_id");
create index "credit_specialization_code_idx" on "credit" ("specialization_code");
create index "credit_insurance_code_idx" on "credit" ("insurance_code");

begin;
drop view if exists "credit_view";
create view "credit_view" as
 SELECT 
    "credit".*, 
    date_part('year'::text, "credit"."date") AS "year", 
    date_part('month'::text, "credit"."date") AS "month", 
    date_part('quarter'::text, "credit"."date") AS "quarter", 
    "customer"."icp" as "customer_icp",
    "customer"."name" as "customer_name",
    "customer"."dealer_account_login",
    "customer"."dealer_contract",
    "customer"."bonusprogramm_percents",
    "credit"."date" >= (date_trunc('month'::text, "customer"."created" - '15 days'::interval) + '1 mon'::interval) 
        AS allow_commissions,
    ("customer"."primary_lab_id" = "lab_id") AS "lab_is_primary"
 from "credit"
 join "customer" on "credit"."customer_id" = "customer"."id"
;
end;

/*
 left join "dasta_list" "dasta_list_zdrpoj"  
    "dasta_list"."content" as "insurance"
    on ("credit"."insurance_code" = "dasta_list_zdrpoj"."id" 
    and "dasta_list_zdrpoj"."list" = 'ZDRPOJ')
*/    

create table "commission_period" (
    "id"                 integer not null default nextval('uid_seq'),
    "year"               integer,
    "quarter"            integer,
    "coefficient"        numeric,
    "consultation_price" integer,
    "calculated"         timestamp(0),
    primary key ("id"),
    unique ("year", "quarter")
);

create table "commission" (
    "id"            integer not null default nextval('uid_seq'),
    "period_id"     integer not null,
    "customer_id"   integer not null,
    "transfered"    numeric(12,2),
    "calculated"    numeric(12,2),
    "consultations" integer,
    "commissions"   integer,
    "transfer"      numeric(12,2),
    "invoiced"      integer,
    primary key ("id"),
    unique ("customer_id", "period_id"),
    foreign key ("customer_id") references "customer" ("id") on update cascade on delete cascade,
    foreign key ("period_id") references "commission_period" ("id") on update cascade on delete cascade
);
