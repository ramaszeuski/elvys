create table "meeting" (
--klassifikacni polozky
    "id"            integer not null default nextval('uid_seq'),
    "user_username" varchar(32) not null,
    "customer_id"   integer not null,

    "created"       timestamp(0) not null default now(),
    "deleted"       timestamp(0),
-- jednani
    "state"         smallint not null, -- 0 planovana, 1 uskutecnena, -2 zrusena
    "date"          date not null,
    "location"      text,
    "reason"        text,
    "program"       text,
    "record"        text,
    "notes"         text,
    foreign key ("user_username")
        references "user" ("username") on delete restrict on update cascade,
    foreign key ("customer_id")
        references "customer" ("id") on delete restrict on update cascade
);

begin;

drop view if exists "meeting_view";
create view "meeting_view" as
select
"meeting".*,
"user"."firstname" as "dealer_firstname",
"user"."lastname" as "dealer_lastname",
"customer"."name" as "customer_name"
from "meeting"
join "user" on ("meeting"."user_username" = "user"."username")
join "customer" on ("meeting"."customer_id" = "customer"."id")
;

end;
