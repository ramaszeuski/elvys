create sequence "uid_seq" start 10000;

create table "role" (
    "role"   varchar(32) not null,
    "active" boolean not null default 't',
    "name"   text not null,
    primary key ("role")
);

create table "user" (
    "username"  varchar(32) not null,
    "password"  varchar(32) not null,
    "active"    boolean not null default 't',
    "firstname" text,
    "lastname"  text,
    "email"     text,
    "tfa"       smallint not null default 0,
  primary key   ("username")
);

create table "user_role" (
    "user_username" varchar(32) not null,
    "role_role" varchar(32) not null,
    primary key ("user_username", "role_role"),
    foreign key ("user_username") references "user" ("username") on update cascade on delete cascade,
    foreign key ("role_role") references "role" ("role") on update cascade on delete cascade
);

create table "log" (
    "id"            integer not null default nextval('uid_seq'),
    "timestamp"     timestamp(0) not null default current_timestamp,
    "event"         varchar(32) not null,
    "address"       varchar(15),
    "user_username" varchar(32),
    "data"          text,
    primary key ("id")
);
