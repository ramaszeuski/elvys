--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.1
-- Dumped by pg_dump version 9.6.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- Name: cz_off(text); Type: FUNCTION; Schema: public; Owner: ecdata
--

CREATE FUNCTION cz_off(text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
 begin
    return lower(translate ($1, 'áčďéěíĺňóřšťúůýžÁČĎÉĚÍĹŇÓŘŠŤÚŮÝŽ', 'acdeeilnorstuuyzacdeeilnorstuuyz'));
 end;
$_$;


ALTER FUNCTION public.cz_off(text) OWNER TO ecdata;

--
-- Name: uid_seq; Type: SEQUENCE; Schema: public; Owner: ecdata
--

CREATE SEQUENCE uid_seq
    START WITH 10000
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE uid_seq OWNER TO ecdata;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: subject; Type: TABLE; Schema: public; Owner: ecdata
--

CREATE TABLE subject (
    id integer DEFAULT nextval('uid_seq'::regclass) NOT NULL,
    role character varying(10) NOT NULL,
    active boolean DEFAULT true NOT NULL,
    created timestamp(0) without time zone DEFAULT now() NOT NULL,
    deleted timestamp(0) without time zone,
    login character varying(32),
    password character varying(32),
    name text,
    firstname text,
    lastname text,
    email text,
    messenger text,
    phone text,
    cellular text,
    fax text,
    country text,
    region text,
    city text,
    address text,
    zip text,
    profile text,
    notes text,
    tfa smallint DEFAULT 0 NOT NULL
);


ALTER TABLE subject OWNER TO ecdata;

--
-- Name: clinic; Type: TABLE; Schema: public; Owner: ecdata
--

CREATE TABLE clinic (
)
INHERITS (subject);


ALTER TABLE clinic OWNER TO ecdata;

--
-- Name: commission; Type: TABLE; Schema: public; Owner: ecdata
--

CREATE TABLE commission (
    id integer DEFAULT nextval('uid_seq'::regclass) NOT NULL,
    period_id integer NOT NULL,
    customer_id integer NOT NULL,
    transfered numeric(12,2),
    calculated numeric(12,2),
    consultations integer,
    commissions integer,
    transfer numeric(12,2),
    invoiced integer
);


ALTER TABLE commission OWNER TO ecdata;

--
-- Name: commission_period; Type: TABLE; Schema: public; Owner: ecdata
--

CREATE TABLE commission_period (
    id integer DEFAULT nextval('uid_seq'::regclass) NOT NULL,
    year integer,
    quarter integer,
    coefficient numeric,
    consultation_price integer,
    calculated timestamp(0) without time zone
);


ALTER TABLE commission_period OWNER TO ecdata;

--
-- Name: creator; Type: TABLE; Schema: public; Owner: ecdata
--

CREATE TABLE creator (
    id integer DEFAULT nextval('uid_seq'::regclass) NOT NULL,
    name text,
    kod_firmy text,
    kod_prog text,
    verze_prog text,
    liccis_prog text
);


ALTER TABLE creator OWNER TO ecdata;

--
-- Name: credit; Type: TABLE; Schema: public; Owner: ecdata
--

CREATE TABLE credit (
    id integer DEFAULT nextval('uid_seq'::regclass) NOT NULL,
    lab_id bigint NOT NULL,
    customer_id bigint NOT NULL,
    specialization_code character(3),
    insurance_code integer,
    date date,
    credits integer
);


ALTER TABLE credit OWNER TO ecdata;

--
-- Name: credit_201709_bad; Type: TABLE; Schema: public; Owner: ecdata
--

CREATE TABLE credit_201709_bad (
    id integer,
    lab_id bigint,
    customer_id bigint,
    specialization_code character(3),
    insurance_code integer,
    date date,
    credits integer
);


ALTER TABLE credit_201709_bad OWNER TO ecdata;

--
-- Name: credit_bak; Type: TABLE; Schema: public; Owner: ecdata
--

CREATE TABLE credit_bak (
    id integer,
    lab_id bigint,
    customer_id bigint,
    specialization_code integer,
    insurance_code integer,
    date date,
    credits integer
);


ALTER TABLE credit_bak OWNER TO ecdata;

--
-- Name: credit_batch; Type: TABLE; Schema: public; Owner: ecdata
--

CREATE TABLE credit_batch (
    id integer DEFAULT nextval('uid_seq'::regclass) NOT NULL,
    lab_id bigint NOT NULL,
    number bigint NOT NULL,
    month smallint,
    year smallint
);


ALTER TABLE credit_batch OWNER TO ecdata;

--
-- Name: credit_source; Type: TABLE; Schema: public; Owner: ecdata
--

CREATE TABLE credit_source (
    source character varying(128) NOT NULL,
    date date NOT NULL,
    processed timestamp(0) without time zone DEFAULT now() NOT NULL
);


ALTER TABLE credit_source OWNER TO ecdata;

--
-- Name: credit_source_bak; Type: TABLE; Schema: public; Owner: ecdata
--

CREATE TABLE credit_source_bak (
    source character varying(128),
    date date,
    processed timestamp(0) without time zone
);


ALTER TABLE credit_source_bak OWNER TO ecdata;

--
-- Name: customer; Type: TABLE; Schema: public; Owner: ecdata
--

CREATE TABLE customer (
    ico character varying(8),
    icz character varying(8),
    icp character varying(8),
    icl character varying(8),
    pcz character varying(3),
    dealer_account_login character varying(32),
    region_id character varying(8),
    specialization text,
    share text,
    primary_lab_id integer,
    dealer_contract boolean,
    clinic_id integer,
    specialization_code character varying(3),
    bonusprogramm_percents smallint DEFAULT 5 NOT NULL,
    email_notify boolean DEFAULT false NOT NULL,
    changed timestamp without time zone,
    last_login timestamp without time zone
)
INHERITS (subject);


ALTER TABLE customer OWNER TO ecdata;

--
-- Name: credit_view; Type: VIEW; Schema: public; Owner: ecdata
--

CREATE VIEW credit_view AS
 SELECT credit.id,
    credit.lab_id,
    credit.customer_id,
    credit.specialization_code,
    credit.insurance_code,
    credit.date,
    credit.credits,
    date_part('year'::text, credit.date) AS year,
    date_part('month'::text, credit.date) AS month,
    date_part('quarter'::text, credit.date) AS quarter,
    customer.icp AS customer_icp,
    customer.name AS customer_name,
    customer.dealer_account_login,
    customer.dealer_contract,
    customer.bonusprogramm_percents,
    (credit.date >= (date_trunc('month'::text, (customer.created - '15 days'::interval)) + '1 mon'::interval)) AS allow_commissions,
    (customer.primary_lab_id = credit.lab_id) AS lab_is_primary
   FROM (credit
     JOIN customer ON ((credit.customer_id = customer.id)));


ALTER TABLE credit_view OWNER TO ecdata;

--
-- Name: lab; Type: TABLE; Schema: public; Owner: ecdata
--

CREATE TABLE lab (
    ico character varying(8),
    icz character varying(8),
    icp character varying(8),
    icl character varying(8),
    pcz character varying(3)
)
INHERITS (subject);


ALTER TABLE lab OWNER TO ecdata;

--
-- Name: region; Type: TABLE; Schema: public; Owner: ecdata
--

CREATE TABLE region (
    id character varying(8) NOT NULL,
    name text
);


ALTER TABLE region OWNER TO ecdata;

--
-- Name: user; Type: TABLE; Schema: public; Owner: ecdata
--

CREATE TABLE "user" (
    username character varying(32) NOT NULL,
    password character varying(32) NOT NULL,
    active boolean DEFAULT true NOT NULL,
    firstname text,
    lastname text,
    email text,
    tfa smallint DEFAULT 0 NOT NULL
);


ALTER TABLE "user" OWNER TO ecdata;

--
-- Name: customer_view; Type: VIEW; Schema: public; Owner: ecdata
--

CREATE VIEW customer_view AS
 SELECT customer.id,
    customer.role,
    customer.active,
    customer.created,
    customer.deleted,
    customer.login,
    customer.password,
    customer.name,
    customer.firstname,
    customer.lastname,
    customer.email,
    customer.messenger,
    customer.phone,
    customer.cellular,
    customer.fax,
    customer.country,
    customer.region,
    customer.city,
    customer.address,
    customer.zip,
    customer.profile,
    customer.notes,
    customer.ico,
    customer.icz,
    customer.icp,
    customer.icl,
    customer.pcz,
    customer.dealer_account_login,
    customer.region_id,
    customer.specialization,
    customer.share,
    customer.primary_lab_id,
    customer.dealer_contract,
    customer.clinic_id,
    customer.specialization_code,
    customer.bonusprogramm_percents,
    customer.email_notify,
    customer.changed,
    customer.last_login,
    customer.tfa,
    (("user".firstname || ' '::text) || "user".lastname) AS dealer_name,
    region.name AS region_name,
    lab.name AS primary_lab_name,
    clinic.name AS clinic_name
   FROM ((((customer
     LEFT JOIN "user" ON (((customer.dealer_account_login)::text = ("user".username)::text)))
     LEFT JOIN region ON (((customer.region_id)::text = (region.id)::text)))
     LEFT JOIN lab ON ((customer.primary_lab_id = lab.id)))
     LEFT JOIN clinic ON ((customer.clinic_id = clinic.id)));


ALTER TABLE customer_view OWNER TO ecdata;

--
-- Name: dasta_list; Type: TABLE; Schema: public; Owner: ecdata
--

CREATE TABLE dasta_list (
    list character varying(8) NOT NULL,
    id character varying(16) NOT NULL,
    content text
);


ALTER TABLE dasta_list OWNER TO ecdata;

--
-- Name: user_role; Type: TABLE; Schema: public; Owner: ecdata
--

CREATE TABLE user_role (
    user_username character varying(32) NOT NULL,
    role_role character varying(32) NOT NULL
);


ALTER TABLE user_role OWNER TO ecdata;

--
-- Name: dealer_view; Type: VIEW; Schema: public; Owner: ecdata
--

CREATE VIEW dealer_view AS
 SELECT "user".username,
    "user".password,
    "user".active,
    "user".firstname,
    "user".lastname,
    "user".email,
    "user".tfa,
    (("user".firstname || ' '::text) || "user".lastname) AS name
   FROM ("user"
     JOIN user_role ON (((("user".username)::text = (user_role.user_username)::text) AND ((user_role.role_role)::text = 'dealer'::text))));


ALTER TABLE dealer_view OWNER TO ecdata;

--
-- Name: ftp_group_view; Type: VIEW; Schema: public; Owner: ecdata
--

CREATE VIEW ftp_group_view AS
 SELECT 1001 AS gid,
    'ecdata'::text AS name,
    ''::text AS members;


ALTER TABLE ftp_group_view OWNER TO ecdata;

--
-- Name: ftp_user_view; Type: VIEW; Schema: public; Owner: ecdata
--

CREATE VIEW ftp_user_view AS
 SELECT lab.id AS userid,
    lab.password AS passwd,
    ('/var/lib/ecdata/inbox/'::text || lab.id) AS homedir
   FROM lab
  WHERE (lab.active AND (lab.deleted IS NULL));


ALTER TABLE ftp_user_view OWNER TO ecdata;

--
-- Name: log; Type: TABLE; Schema: public; Owner: ecdata
--

CREATE TABLE log (
    id integer DEFAULT nextval('uid_seq'::regclass) NOT NULL,
    "timestamp" timestamp(0) without time zone DEFAULT now() NOT NULL,
    event character varying(32) NOT NULL,
    address character varying(15),
    user_username character varying(32),
    data text
);


ALTER TABLE log OWNER TO ecdata;

--
-- Name: log_2012_2015; Type: TABLE; Schema: public; Owner: ecdata
--

CREATE TABLE log_2012_2015 (
    id integer,
    "timestamp" timestamp(0) without time zone,
    event character varying(32),
    address character varying(15),
    user_username character varying(32),
    data text
);


ALTER TABLE log_2012_2015 OWNER TO ecdata;

--
-- Name: log_view; Type: VIEW; Schema: public; Owner: ecdata
--

CREATE VIEW log_view AS
 SELECT log.id,
    log."timestamp",
    log.event,
    log.address,
    log.user_username,
    log.data,
    COALESCE(subject.role, 'system'::character varying) AS user_role,
    subject.name AS user_name,
    COALESCE(subject.firstname, "user".firstname) AS user_firstname,
    COALESCE(subject.lastname, "user".lastname) AS user_lastname
   FROM ((log
     LEFT JOIN subject ON (((log.user_username)::text = (subject.id)::text)))
     LEFT JOIN "user" ON (((log.user_username)::text = ("user".username)::text)));


ALTER TABLE log_view OWNER TO ecdata;

--
-- Name: login_role_view; Type: VIEW; Schema: public; Owner: ecdata
--

CREATE VIEW login_role_view AS
 SELECT user_role.user_username AS login,
    user_role.role_role AS role
   FROM user_role
UNION
 SELECT subject.login,
    subject.role
   FROM subject
  WHERE (subject.active AND (subject.deleted IS NULL));


ALTER TABLE login_role_view OWNER TO ecdata;

--
-- Name: login_view; Type: VIEW; Schema: public; Owner: ecdata
--

CREATE VIEW login_view AS
 SELECT "user".username,
    "user".password
   FROM "user"
  WHERE "user".active
UNION
 SELECT subject.login AS username,
    subject.password
   FROM subject
  WHERE (subject.active AND (subject.deleted IS NULL));


ALTER TABLE login_view OWNER TO ecdata;

--
-- Name: meeting; Type: TABLE; Schema: public; Owner: ecdata
--

CREATE TABLE meeting (
    id integer DEFAULT nextval('uid_seq'::regclass) NOT NULL,
    user_username character varying(32) NOT NULL,
    customer_id integer NOT NULL,
    created timestamp(0) without time zone DEFAULT now() NOT NULL,
    deleted timestamp(0) without time zone,
    state smallint NOT NULL,
    date date NOT NULL,
    location text,
    reason text,
    program text,
    record text,
    notes text
);


ALTER TABLE meeting OWNER TO ecdata;

--
-- Name: meeting_view; Type: VIEW; Schema: public; Owner: ecdata
--

CREATE VIEW meeting_view AS
 SELECT meeting.id,
    meeting.user_username,
    meeting.customer_id,
    meeting.created,
    meeting.deleted,
    meeting.state,
    meeting.date,
    meeting.location,
    meeting.reason,
    meeting.program,
    meeting.record,
    meeting.notes,
    "user".firstname AS dealer_firstname,
    "user".lastname AS dealer_lastname,
    customer.name AS customer_name
   FROM ((meeting
     JOIN "user" ON (((meeting.user_username)::text = ("user".username)::text)))
     JOIN customer ON ((meeting.customer_id = customer.id)));


ALTER TABLE meeting_view OWNER TO ecdata;

--
-- Name: patient; Type: TABLE; Schema: public; Owner: ecdata
--

CREATE TABLE patient (
    rc character varying(10),
    birthdate date,
    sex character varying(8),
    insurance_id integer
)
INHERITS (subject);


ALTER TABLE patient OWNER TO ecdata;

--
-- Name: patient_view; Type: VIEW; Schema: public; Owner: ecdata
--

CREATE VIEW patient_view AS
 SELECT patient.id,
    patient.role,
    patient.active,
    patient.created,
    patient.deleted,
    patient.login,
    patient.password,
    patient.name,
    patient.firstname,
    patient.lastname,
    patient.email,
    patient.messenger,
    patient.phone,
    patient.cellular,
    patient.fax,
    patient.country,
    patient.region,
    patient.city,
    patient.address,
    patient.zip,
    patient.profile,
    patient.notes,
    patient.rc,
    patient.birthdate,
    patient.sex,
    patient.insurance_id,
    cz_off(patient.lastname) AS lastname_cz_off
   FROM patient;


ALTER TABLE patient_view OWNER TO ecdata;

--
-- Name: result; Type: TABLE; Schema: public; Owner: ecdata
--

CREATE TABLE result (
    id integer DEFAULT nextval('uid_seq'::regclass) NOT NULL,
    sampling timestamp without time zone,
    viewed timestamp without time zone,
    xml_id character varying(40) NOT NULL,
    patient_id bigint NOT NULL,
    notified timestamp without time zone
);


ALTER TABLE result OWNER TO ecdata;

--
-- Name: result_archive; Type: TABLE; Schema: public; Owner: ecdata
--

CREATE TABLE result_archive (
    id integer DEFAULT nextval('uid_seq'::regclass) NOT NULL,
    sampling timestamp without time zone,
    viewed timestamp without time zone,
    xml_id character varying(40),
    patient_id bigint,
    notified timestamp without time zone
);


ALTER TABLE result_archive OWNER TO ecdata;

--
-- Name: xml_archive; Type: TABLE; Schema: public; Owner: ecdata
--

CREATE TABLE xml_archive (
    id character varying(40),
    generated timestamp without time zone,
    processed timestamp without time zone,
    downloaded timestamp without time zone,
    deleted timestamp without time zone,
    lab_id bigint,
    customer_id bigint,
    creator_id bigint,
    dasta_id_soubor text,
    dasta_verze_ds text,
    dasta_verze_nclp text,
    dasta_bin_priloha character(1),
    dasta_ur character(1),
    dasta_typ_odesm text,
    dasta_ozn_soub text,
    dasta_potvrzeni character(1),
    has_result boolean
);


ALTER TABLE xml_archive OWNER TO ecdata;

--
-- Name: result_archive_view; Type: VIEW; Schema: public; Owner: ecdata
--

CREATE VIEW result_archive_view AS
 SELECT result_archive.id,
    result_archive.sampling,
    result_archive.notified,
    result_archive.xml_id,
    xml_archive.generated,
    xml_archive.lab_id,
    lab.icz AS lab_icz,
    lab.icp AS lab_icp,
    lab.name AS lab_name,
    xml_archive.customer_id,
    customer.icz AS customer_icz,
    customer.icp AS customer_icp,
    customer.name AS customer_name,
    customer.email AS customer_email,
    customer.email_notify,
    customer.dealer_account_login,
    result_archive.patient_id,
    patient.rc,
    patient.firstname,
    patient.lastname,
    cz_off(((patient.firstname || ' '::text) || patient.lastname)) AS name_cz_off
   FROM ((((result_archive
     JOIN xml_archive ON (((result_archive.xml_id)::text = (xml_archive.id)::text)))
     JOIN lab ON ((xml_archive.lab_id = lab.id)))
     JOIN customer ON ((xml_archive.customer_id = customer.id)))
     JOIN patient ON ((result_archive.patient_id = patient.id)));


ALTER TABLE result_archive_view OWNER TO ecdata;

--
-- Name: xml; Type: TABLE; Schema: public; Owner: ecdata
--

CREATE TABLE xml (
    id character varying(40) NOT NULL,
    generated timestamp without time zone NOT NULL,
    processed timestamp without time zone DEFAULT now() NOT NULL,
    downloaded timestamp without time zone,
    deleted timestamp without time zone,
    lab_id bigint NOT NULL,
    customer_id bigint NOT NULL,
    creator_id bigint NOT NULL,
    dasta_id_soubor text,
    dasta_verze_ds text,
    dasta_verze_nclp text,
    dasta_bin_priloha character(1),
    dasta_ur character(1),
    dasta_typ_odesm text,
    dasta_ozn_soub text,
    dasta_potvrzeni character(1)
);


ALTER TABLE xml OWNER TO ecdata;

--
-- Name: result_view; Type: VIEW; Schema: public; Owner: ecdata
--

CREATE VIEW result_view AS
 SELECT result.id,
    result.sampling,
    result.notified,
    result.xml_id,
    xml.generated,
    xml.lab_id,
    lab.icz AS lab_icz,
    lab.icp AS lab_icp,
    lab.name AS lab_name,
    xml.customer_id,
    customer.icz AS customer_icz,
    customer.icp AS customer_icp,
    customer.name AS customer_name,
    customer.email AS customer_email,
    customer.email_notify,
    customer.dealer_account_login,
    result.patient_id,
    patient.rc,
    patient.firstname,
    patient.lastname,
    cz_off(((patient.firstname || ' '::text) || patient.lastname)) AS name_cz_off
   FROM ((((result
     JOIN xml ON (((result.xml_id)::text = (xml.id)::text)))
     JOIN lab ON ((xml.lab_id = lab.id)))
     JOIN customer ON ((xml.customer_id = customer.id)))
     JOIN patient ON ((result.patient_id = patient.id)));


ALTER TABLE result_view OWNER TO ecdata;

--
-- Name: result_all_view; Type: VIEW; Schema: public; Owner: ecdata
--

CREATE VIEW result_all_view AS
 SELECT result_view.id,
    result_view.sampling,
    result_view.notified,
    result_view.xml_id,
    result_view.generated,
    result_view.lab_id,
    result_view.lab_icz,
    result_view.lab_icp,
    result_view.lab_name,
    result_view.customer_id,
    result_view.customer_icz,
    result_view.customer_icp,
    result_view.customer_name,
    result_view.customer_email,
    result_view.email_notify,
    result_view.dealer_account_login,
    result_view.patient_id,
    result_view.rc,
    result_view.firstname,
    result_view.lastname,
    result_view.name_cz_off
   FROM result_view
UNION
 SELECT result_archive_view.id,
    result_archive_view.sampling,
    result_archive_view.notified,
    result_archive_view.xml_id,
    result_archive_view.generated,
    result_archive_view.lab_id,
    result_archive_view.lab_icz,
    result_archive_view.lab_icp,
    result_archive_view.lab_name,
    result_archive_view.customer_id,
    result_archive_view.customer_icz,
    result_archive_view.customer_icp,
    result_archive_view.customer_name,
    result_archive_view.customer_email,
    result_archive_view.email_notify,
    result_archive_view.dealer_account_login,
    result_archive_view.patient_id,
    result_archive_view.rc,
    result_archive_view.firstname,
    result_archive_view.lastname,
    result_archive_view.name_cz_off
   FROM result_archive_view;


ALTER TABLE result_all_view OWNER TO ecdata;

--
-- Name: role; Type: TABLE; Schema: public; Owner: ecdata
--

CREATE TABLE role (
    role character varying(32) NOT NULL,
    active boolean DEFAULT true NOT NULL,
    name text NOT NULL
);


ALTER TABLE role OWNER TO ecdata;

--
-- Name: take_customer_request; Type: TABLE; Schema: public; Owner: ecdata
--

CREATE TABLE take_customer_request (
    id integer DEFAULT nextval('uid_seq'::regclass) NOT NULL,
    state smallint,
    created timestamp(0) without time zone DEFAULT now() NOT NULL,
    decided timestamp(0) without time zone,
    customer_id bigint NOT NULL,
    dealer_account_login character varying(32),
    manager_account_login character varying(32),
    request text,
    response text
);


ALTER TABLE take_customer_request OWNER TO ecdata;

--
-- Name: take_customer_request_view; Type: VIEW; Schema: public; Owner: ecdata
--

CREATE VIEW take_customer_request_view AS
 SELECT take_customer_request.id,
    take_customer_request.state,
    take_customer_request.created,
    take_customer_request.decided,
    take_customer_request.customer_id,
    take_customer_request.dealer_account_login,
    take_customer_request.manager_account_login,
    take_customer_request.request,
    take_customer_request.response,
    customer.name AS customer_name,
    ((pretender.firstname || ' '::text) || pretender.lastname) AS pretender_name,
    ((dealer.firstname || ' '::text) || dealer.lastname) AS dealer_name,
    ((manager.firstname || ' '::text) || manager.lastname) AS manager_name
   FROM ((((take_customer_request
     JOIN customer ON ((take_customer_request.customer_id = customer.id)))
     JOIN "user" pretender ON (((take_customer_request.dealer_account_login)::text = (pretender.username)::text)))
     LEFT JOIN "user" dealer ON (((customer.dealer_account_login)::text = (dealer.username)::text)))
     LEFT JOIN "user" manager ON (((take_customer_request.manager_account_login)::text = (manager.username)::text)));


ALTER TABLE take_customer_request_view OWNER TO ecdata;

--
-- Name: two_factor_codes; Type: TABLE; Schema: public; Owner: ecdata
--

CREATE TABLE two_factor_codes (
    username text NOT NULL,
    code text,
    generated timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE two_factor_codes OWNER TO ecdata;

--
-- Name: xml_view; Type: VIEW; Schema: public; Owner: ecdata
--

CREATE VIEW xml_view AS
 SELECT xml.id,
    xml.generated,
    xml.processed,
    xml.downloaded,
    xml.deleted,
    xml.lab_id,
    xml.customer_id,
    xml.creator_id,
    xml.dasta_id_soubor,
    xml.dasta_verze_ds,
    xml.dasta_verze_nclp,
    xml.dasta_bin_priloha,
    xml.dasta_ur,
    xml.dasta_typ_odesm,
    xml.dasta_ozn_soub,
    xml.dasta_potvrzeni,
    lab.icz AS lab_icz,
    lab.icp AS lab_icp,
    lab.name AS lab_name,
    customer.icz AS customer_icz,
    customer.icp AS customer_icp,
    customer.name AS customer_name
   FROM ((xml
     JOIN lab ON ((xml.lab_id = lab.id)))
     JOIN customer ON ((xml.customer_id = customer.id)));


ALTER TABLE xml_view OWNER TO ecdata;

--
-- Name: clinic id; Type: DEFAULT; Schema: public; Owner: ecdata
--

ALTER TABLE ONLY clinic ALTER COLUMN id SET DEFAULT nextval('uid_seq'::regclass);


--
-- Name: clinic active; Type: DEFAULT; Schema: public; Owner: ecdata
--

ALTER TABLE ONLY clinic ALTER COLUMN active SET DEFAULT true;


--
-- Name: clinic created; Type: DEFAULT; Schema: public; Owner: ecdata
--

ALTER TABLE ONLY clinic ALTER COLUMN created SET DEFAULT now();


--
-- Name: clinic tfa; Type: DEFAULT; Schema: public; Owner: ecdata
--

ALTER TABLE ONLY clinic ALTER COLUMN tfa SET DEFAULT 0;


--
-- Name: customer id; Type: DEFAULT; Schema: public; Owner: ecdata
--

ALTER TABLE ONLY customer ALTER COLUMN id SET DEFAULT nextval('uid_seq'::regclass);


--
-- Name: customer active; Type: DEFAULT; Schema: public; Owner: ecdata
--

ALTER TABLE ONLY customer ALTER COLUMN active SET DEFAULT true;


--
-- Name: customer created; Type: DEFAULT; Schema: public; Owner: ecdata
--

ALTER TABLE ONLY customer ALTER COLUMN created SET DEFAULT now();


--
-- Name: customer tfa; Type: DEFAULT; Schema: public; Owner: ecdata
--

ALTER TABLE ONLY customer ALTER COLUMN tfa SET DEFAULT 0;


--
-- Name: lab id; Type: DEFAULT; Schema: public; Owner: ecdata
--

ALTER TABLE ONLY lab ALTER COLUMN id SET DEFAULT nextval('uid_seq'::regclass);


--
-- Name: lab active; Type: DEFAULT; Schema: public; Owner: ecdata
--

ALTER TABLE ONLY lab ALTER COLUMN active SET DEFAULT true;


--
-- Name: lab created; Type: DEFAULT; Schema: public; Owner: ecdata
--

ALTER TABLE ONLY lab ALTER COLUMN created SET DEFAULT now();


--
-- Name: lab tfa; Type: DEFAULT; Schema: public; Owner: ecdata
--

ALTER TABLE ONLY lab ALTER COLUMN tfa SET DEFAULT 0;


--
-- Name: patient id; Type: DEFAULT; Schema: public; Owner: ecdata
--

ALTER TABLE ONLY patient ALTER COLUMN id SET DEFAULT nextval('uid_seq'::regclass);


--
-- Name: patient active; Type: DEFAULT; Schema: public; Owner: ecdata
--

ALTER TABLE ONLY patient ALTER COLUMN active SET DEFAULT true;


--
-- Name: patient created; Type: DEFAULT; Schema: public; Owner: ecdata
--

ALTER TABLE ONLY patient ALTER COLUMN created SET DEFAULT now();


--
-- Name: patient tfa; Type: DEFAULT; Schema: public; Owner: ecdata
--

ALTER TABLE ONLY patient ALTER COLUMN tfa SET DEFAULT 0;


--
-- Name: clinic clinic_pkey; Type: CONSTRAINT; Schema: public; Owner: ecdata
--

ALTER TABLE ONLY clinic
    ADD CONSTRAINT clinic_pkey PRIMARY KEY (id);


--
-- Name: commission commission_customer_id_key; Type: CONSTRAINT; Schema: public; Owner: ecdata
--

ALTER TABLE ONLY commission
    ADD CONSTRAINT commission_customer_id_key UNIQUE (customer_id, period_id);


--
-- Name: commission_period commission_period_pkey; Type: CONSTRAINT; Schema: public; Owner: ecdata
--

ALTER TABLE ONLY commission_period
    ADD CONSTRAINT commission_period_pkey PRIMARY KEY (id);


--
-- Name: commission_period commission_period_year_key; Type: CONSTRAINT; Schema: public; Owner: ecdata
--

ALTER TABLE ONLY commission_period
    ADD CONSTRAINT commission_period_year_key UNIQUE (year, quarter);


--
-- Name: commission commission_pkey; Type: CONSTRAINT; Schema: public; Owner: ecdata
--

ALTER TABLE ONLY commission
    ADD CONSTRAINT commission_pkey PRIMARY KEY (id);


--
-- Name: creator creator_kod_firmy_key; Type: CONSTRAINT; Schema: public; Owner: ecdata
--

ALTER TABLE ONLY creator
    ADD CONSTRAINT creator_kod_firmy_key UNIQUE (kod_firmy, kod_prog, verze_prog);


--
-- Name: creator creator_pkey; Type: CONSTRAINT; Schema: public; Owner: ecdata
--

ALTER TABLE ONLY creator
    ADD CONSTRAINT creator_pkey PRIMARY KEY (id);


--
-- Name: credit_batch credit_batch_pkey; Type: CONSTRAINT; Schema: public; Owner: ecdata
--

ALTER TABLE ONLY credit_batch
    ADD CONSTRAINT credit_batch_pkey PRIMARY KEY (id);


--
-- Name: credit credit_pkey; Type: CONSTRAINT; Schema: public; Owner: ecdata
--

ALTER TABLE ONLY credit
    ADD CONSTRAINT credit_pkey PRIMARY KEY (id);


--
-- Name: credit_source credit_source_pkey; Type: CONSTRAINT; Schema: public; Owner: ecdata
--

ALTER TABLE ONLY credit_source
    ADD CONSTRAINT credit_source_pkey PRIMARY KEY (source);


--
-- Name: customer customer_ico_key; Type: CONSTRAINT; Schema: public; Owner: ecdata
--

ALTER TABLE ONLY customer
    ADD CONSTRAINT customer_ico_key UNIQUE (ico, icz, icp, icl, pcz);


--
-- Name: customer customer_pkey; Type: CONSTRAINT; Schema: public; Owner: ecdata
--

ALTER TABLE ONLY customer
    ADD CONSTRAINT customer_pkey PRIMARY KEY (id);


--
-- Name: dasta_list dasta_list_pkey; Type: CONSTRAINT; Schema: public; Owner: ecdata
--

ALTER TABLE ONLY dasta_list
    ADD CONSTRAINT dasta_list_pkey PRIMARY KEY (list, id);


--
-- Name: lab lab_pkey; Type: CONSTRAINT; Schema: public; Owner: ecdata
--

ALTER TABLE ONLY lab
    ADD CONSTRAINT lab_pkey PRIMARY KEY (id);


--
-- Name: log log_pkey; Type: CONSTRAINT; Schema: public; Owner: ecdata
--

ALTER TABLE ONLY log
    ADD CONSTRAINT log_pkey PRIMARY KEY (id);


--
-- Name: patient patient_pkey; Type: CONSTRAINT; Schema: public; Owner: ecdata
--

ALTER TABLE ONLY patient
    ADD CONSTRAINT patient_pkey PRIMARY KEY (id);


--
-- Name: region region_pkey; Type: CONSTRAINT; Schema: public; Owner: ecdata
--

ALTER TABLE ONLY region
    ADD CONSTRAINT region_pkey PRIMARY KEY (id);


--
-- Name: result result_pkey; Type: CONSTRAINT; Schema: public; Owner: ecdata
--

ALTER TABLE ONLY result
    ADD CONSTRAINT result_pkey PRIMARY KEY (id);


--
-- Name: role role_pkey; Type: CONSTRAINT; Schema: public; Owner: ecdata
--

ALTER TABLE ONLY role
    ADD CONSTRAINT role_pkey PRIMARY KEY (role);


--
-- Name: take_customer_request take_customer_request_pkey; Type: CONSTRAINT; Schema: public; Owner: ecdata
--

ALTER TABLE ONLY take_customer_request
    ADD CONSTRAINT take_customer_request_pkey PRIMARY KEY (id);


--
-- Name: two_factor_codes two_factor_codes_pkey; Type: CONSTRAINT; Schema: public; Owner: ecdata
--

ALTER TABLE ONLY two_factor_codes
    ADD CONSTRAINT two_factor_codes_pkey PRIMARY KEY (username);


--
-- Name: user user_pkey; Type: CONSTRAINT; Schema: public; Owner: ecdata
--

ALTER TABLE ONLY "user"
    ADD CONSTRAINT user_pkey PRIMARY KEY (username);


--
-- Name: user_role user_role_pkey; Type: CONSTRAINT; Schema: public; Owner: ecdata
--

ALTER TABLE ONLY user_role
    ADD CONSTRAINT user_role_pkey PRIMARY KEY (user_username, role_role);


--
-- Name: xml xml_pkey; Type: CONSTRAINT; Schema: public; Owner: ecdata
--

ALTER TABLE ONLY xml
    ADD CONSTRAINT xml_pkey PRIMARY KEY (id);


--
-- Name: credit_customer_id_idx; Type: INDEX; Schema: public; Owner: ecdata
--

CREATE INDEX credit_customer_id_idx ON credit USING btree (customer_id);


--
-- Name: credit_insurance_code_idx; Type: INDEX; Schema: public; Owner: ecdata
--

CREATE INDEX credit_insurance_code_idx ON credit USING btree (insurance_code);


--
-- Name: credit_lab_id_idx; Type: INDEX; Schema: public; Owner: ecdata
--

CREATE INDEX credit_lab_id_idx ON credit USING btree (lab_id);


--
-- Name: credit_specialization_code_idx; Type: INDEX; Schema: public; Owner: ecdata
--

CREATE INDEX credit_specialization_code_idx ON credit USING btree (specialization_code);


--
-- Name: lab_ico_idx; Type: INDEX; Schema: public; Owner: ecdata
--

CREATE INDEX lab_ico_idx ON lab USING btree (ico);


--
-- Name: lab_icp_idx; Type: INDEX; Schema: public; Owner: ecdata
--

CREATE INDEX lab_icp_idx ON lab USING btree (ico);


--
-- Name: lab_icz_idx; Type: INDEX; Schema: public; Owner: ecdata
--

CREATE INDEX lab_icz_idx ON lab USING btree (ico);


--
-- Name: patient_rc_idx; Type: INDEX; Schema: public; Owner: ecdata
--

CREATE INDEX patient_rc_idx ON patient USING btree (rc);


--
-- Name: result_archive_xml_id_idx; Type: INDEX; Schema: public; Owner: ecdata
--

CREATE INDEX result_archive_xml_id_idx ON result_archive USING btree (xml_id);


--
-- Name: xml_archive_customer_id_idx; Type: INDEX; Schema: public; Owner: ecdata
--

CREATE INDEX xml_archive_customer_id_idx ON xml_archive USING btree (customer_id);


--
-- Name: xml_archive_generated; Type: INDEX; Schema: public; Owner: ecdata
--

CREATE INDEX xml_archive_generated ON xml_archive USING btree (generated);


--
-- Name: xml_archive_id_idx; Type: INDEX; Schema: public; Owner: ecdata
--

CREATE INDEX xml_archive_id_idx ON xml_archive USING btree (id);


--
-- Name: xml_archive_lab_id_idx; Type: INDEX; Schema: public; Owner: ecdata
--

CREATE INDEX xml_archive_lab_id_idx ON xml_archive USING btree (lab_id);


--
-- Name: commission commission_customer_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: ecdata
--

ALTER TABLE ONLY commission
    ADD CONSTRAINT commission_customer_id_fkey FOREIGN KEY (customer_id) REFERENCES customer(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: commission commission_period_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: ecdata
--

ALTER TABLE ONLY commission
    ADD CONSTRAINT commission_period_id_fkey FOREIGN KEY (period_id) REFERENCES commission_period(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: credit credit_customer_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: ecdata
--

ALTER TABLE ONLY credit
    ADD CONSTRAINT credit_customer_id_fkey FOREIGN KEY (customer_id) REFERENCES customer(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: credit credit_lab_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: ecdata
--

ALTER TABLE ONLY credit
    ADD CONSTRAINT credit_lab_id_fkey FOREIGN KEY (lab_id) REFERENCES lab(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: meeting meeting_customer_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: ecdata
--

ALTER TABLE ONLY meeting
    ADD CONSTRAINT meeting_customer_id_fkey FOREIGN KEY (customer_id) REFERENCES customer(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: meeting meeting_user_username_fkey; Type: FK CONSTRAINT; Schema: public; Owner: ecdata
--

ALTER TABLE ONLY meeting
    ADD CONSTRAINT meeting_user_username_fkey FOREIGN KEY (user_username) REFERENCES "user"(username) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: result result_patient_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: ecdata
--

ALTER TABLE ONLY result
    ADD CONSTRAINT result_patient_id_fkey FOREIGN KEY (patient_id) REFERENCES patient(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: take_customer_request take_customer_request_customer_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: ecdata
--

ALTER TABLE ONLY take_customer_request
    ADD CONSTRAINT take_customer_request_customer_id_fkey FOREIGN KEY (customer_id) REFERENCES customer(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: take_customer_request take_customer_request_dealer_account_login_fkey; Type: FK CONSTRAINT; Schema: public; Owner: ecdata
--

ALTER TABLE ONLY take_customer_request
    ADD CONSTRAINT take_customer_request_dealer_account_login_fkey FOREIGN KEY (dealer_account_login) REFERENCES "user"(username) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: take_customer_request take_customer_request_manager_account_login_fkey; Type: FK CONSTRAINT; Schema: public; Owner: ecdata
--

ALTER TABLE ONLY take_customer_request
    ADD CONSTRAINT take_customer_request_manager_account_login_fkey FOREIGN KEY (manager_account_login) REFERENCES "user"(username) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: user_role user_role_role_role_fkey; Type: FK CONSTRAINT; Schema: public; Owner: ecdata
--

ALTER TABLE ONLY user_role
    ADD CONSTRAINT user_role_role_role_fkey FOREIGN KEY (role_role) REFERENCES role(role) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: user_role user_role_user_username_fkey; Type: FK CONSTRAINT; Schema: public; Owner: ecdata
--

ALTER TABLE ONLY user_role
    ADD CONSTRAINT user_role_user_username_fkey FOREIGN KEY (user_username) REFERENCES "user"(username) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: xml xml_creator_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: ecdata
--

ALTER TABLE ONLY xml
    ADD CONSTRAINT xml_creator_id_fkey FOREIGN KEY (creator_id) REFERENCES creator(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: xml xml_customer_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: ecdata
--

ALTER TABLE ONLY xml
    ADD CONSTRAINT xml_customer_id_fkey FOREIGN KEY (customer_id) REFERENCES customer(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: xml xml_lab_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: ecdata
--

ALTER TABLE ONLY xml
    ADD CONSTRAINT xml_lab_id_fkey FOREIGN KEY (lab_id) REFERENCES lab(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- PostgreSQL database dump complete
--

