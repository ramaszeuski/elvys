FROM debian:buster-slim

RUN apt-get update && apt-get install -y \
 build-essential \
 cpanminus \
 gettext-base \
 libcrypt-openssl-rsa-perl \
 libcrypt-openssl-x509-perl \
 libdata-password-perl \
 libdate-manip-perl \
 libdbd-pg-perl \
 libdbix-class-perl \
 libdigest-sha-perl \
 libfile-path-perl \
 libfile-slurp-perl \
 libhtml-formfu-model-dbic-perl \
 libjson-perl \
 liblogger-syslog-perl \
 libmime-base64-perl \
 libproc-processtable-perl \
 libstring-mkpasswd-perl \
 libtemplate-plugin-latex-perl \
 libxml-simple-perl \
 libyaml-perl \
 texlive-base \
 texlive-binaries \
 texlive-fonts-recommended \
 texlive-generic-recommended \
 texlive-latex-base \
 texlive-latex-extra \
 texlive-latex-recommended \
 texlive-lang-czechslovak \
 texlive-pictures \
 zip

COPY cpanfile /
RUN cpanm --force --installdeps . -M https://cpan.metacpan.org

RUN groupadd -r ecdata && \
    useradd --no-log-init -u 5000 -r -g ecdata ecdata && \
    mkdir /var/opt/ecdata && \
    chown ecdata /var/opt/ecdata

ADD . /opt/ecdata

USER ecdata
EXPOSE 3000
WORKDIR /opt/ecdata
ENV PERL5LIB=/opt/ecdata/lib CATALYST_CONFIG=/var/opt/ecdata/config.yaml CATALYST_HOME=/opt/ecdata CATALYST_DEBUG=1
ENTRYPOINT /opt/ecdata/entrypoint.sh
