use strict;
use warnings;
use Test::More;


use Catalyst::Test 'ECData';
use ECData::Controller::Commissions;

ok( request('/commissions')->is_success, 'Request should succeed' );
done_testing();
