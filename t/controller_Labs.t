use strict;
use warnings;
use Test::More;


use Catalyst::Test 'ECData';
use ECData::Controller::Labs;

ok( request('/labs')->is_success, 'Request should succeed' );
done_testing();
