use strict;
use warnings;
use Test::More;


use Catalyst::Test 'ECData';
use ECData::Controller::Meetings;

ok( request('/meeting')->is_success, 'Request should succeed' );
done_testing();
