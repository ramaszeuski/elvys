use strict;
use warnings;
use Test::More;

BEGIN { use_ok 'Catalyst::Test', 'ECData' }
BEGIN { use_ok 'ECData::Controller::Users' }

ok( request('/users')->is_success, 'Request should succeed' );
done_testing();
