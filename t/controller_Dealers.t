use strict;
use warnings;
use Test::More;


use Catalyst::Test 'ECData';
use ECData::Controller::Dealers;

ok( request('/dealers')->is_success, 'Request should succeed' );
done_testing();
