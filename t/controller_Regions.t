use strict;
use warnings;
use Test::More;

BEGIN { use_ok 'Catalyst::Test', 'ECData' }
BEGIN { use_ok 'ECData::Controller::Regions' }

ok( request('/regions')->is_success, 'Request should succeed' );
done_testing();
