use strict;
use warnings;
use Test::More;


use Catalyst::Test 'ECData';
use ECData::Controller::XML;

ok( request('/xml')->is_success, 'Request should succeed' );
done_testing();
