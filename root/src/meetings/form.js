$(document).ready(function() {
    $( ".Button" ).button();

    $( "#date" ).datepicker({
        dateFormat: 'dd.mm.yy',
        showOn: 'button', 
        defaultDate: null
    });

    $('#Meeting').ajaxForm({
        beforeSubmit: validate,
        success:    function() { 
            window.location.href = "/customers/[% meeting.customer_id or customer_id %]/";
        } 
    });

    function validate(formData, jqForm, options) { 
        var form = jqForm[0];
 
        if ( !form.location.value ) { 
            alert('Není zadané místo jednání'); 
            return false; 
        } 
        if ( !form.reason.value ) { 
            alert('Není zadaný předmět jednání'); 
            return false; 
        } 
    } 
});

