#!/usr/bin/perl
use strict;
use warnings;
use utf8;

use FindBin qw($Bin);
use lib "$Bin/../lib";

use YAML;
use Getopt::Long::Descriptive;
use ECData::Schema;
use ECData::Storage;

use vars qw(
    $opt
    $usage
    $cfg
    $dbic
    $storage
);

use constant NAME    	 => 'update_customer_names';
use constant LOG_ID  	 => 'ecdata.' . NAME;
use constant DEFAULT_CFG => '/var/lib/ecdata/ecdata.yaml'; 

use constant OPTIONS  => [
  [ 'config|c=s', "konfigurační soubor", { default => DEFAULT_CFG } ],
  [ 'help|h', "tato napověda" ],
];

# parametry prikazove radky
( $opt, $usage ) = describe_options( '%c %o', @{ (OPTIONS) });

$usage->die() if $opt->help;

$cfg = YAML::LoadFile( $opt->config );

# spojeni z databazi
$dbic = ECData::Schema->connect(
    $cfg->{'Model::DB'}{connect_info}{dsn},
    $cfg->{'Model::DB'}{connect_info}{user},
    $cfg->{'Model::DB'}{connect_info}{password},
    {	
        on_connect_do  => "set timezone to 'Europe/Prague'",
        AutoCommit     => 1,       
        quote_char     => q("),    
        name_sep       => q(.),     
        pg_enable_utf8 => 1, 	
    }		
);

# uloziste dokumentu
$storage = ECData::Storage->new( $cfg->{workdir} );

my $customers = $dbic->resultset('Customer')->search(
    {
        name => undef
    },        
);

while ( my $customer = $customers->next() ) {
    my $last_xml = $customer->xmls(
        {},
        {
            order_by => { -desc => 'id'},
            rows     => 1,
        }
    )->first;
    
    my $xml = $storage->get_xml( $last_xml->id );


    if ($xml->{pm} && $xml->{pm}{a} && $xml->{pm}{a}{jmeno}) {
        $customer->update( 
            {
                name => $xml->{pm}{a}{jmeno}{content}, 
            }
        );
    }
    
}
